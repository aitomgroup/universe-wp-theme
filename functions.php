<?php

/**
 * functions.php
 *
 * - Theme setup
 * - Front-end and admin common functions
 * - Plugin installation and activation for theme
 * - Register widgets and shortcodes
 */

    // Prevent direct script access
    if ( !defined( 'ABSPATH' ) ) exit;

    // Define constants

    defined( 'LWD_NAME' )
        || define( 'LWD_NAME', 'Aitom Universe' );

    defined( 'LWD_VERSION' )
        || define( 'LWD_VERSION', '0.1' );

    defined( 'LWD_SLUG' )
        || define( 'LWD_SLUG', get_template() );

    defined( 'LWD_DIR' )
        || define( 'LWD_DIR', get_template_directory() );

    defined( 'LWD_URL' )
        || define( 'LWD_URL', get_template_directory_uri() );

    defined( 'LWD_FRAMEWORK_URL' )
        || define( 'LWD_FRAMEWORK_URL', LWD_URL . '/frontend' );

    defined( 'LWD_INCLUDES_DIR' )
        || define( 'LWD_INCLUDES_DIR', LWD_DIR . '/frontend/inc' );

    defined( 'LWD_ADMIN_DIR' )
        || define( 'LWD_ADMIN_DIR', LWD_DIR . '/admin' );

    defined( 'LWD_ADMIN_INCLUDES_DIR' )
        || define( 'LWD_ADMIN_INCLUDES_DIR', LWD_ADMIN_DIR . '/inc' );

    defined( 'LWD_ADMIN_URL' )
        || define( 'LWD_ADMIN_URL', LWD_URL . '/admin' );

    defined( 'LWD_OPTIONS_NAME' )
        || define( 'LWD_OPTIONS_NAME', LWD_SLUG . '_options' );

    defined( 'LWD_TEXT_DOMAIN' )
        || define( 'LWD_TEXT_DOMAIN', LWD_SLUG );

    defined( 'LWD_ADMIN_TEXT_DOMAIN' )
        || define( 'LWD_ADMIN_TEXT_DOMAIN', LWD_SLUG . '_admin' );

    defined( 'LWD_PREFIX' )
        || define ( 'LWD_PREFIX', 'lwd' );

    /*----------------------------------------------------------------------------*
     * THEME SETUP
     *----------------------------------------------------------------------------*/

    add_action( 'after_setup_theme', function() {

        // Add thumbnail support
        add_theme_support( 'post-thumbnails' );
        
        //set_post_thumbnail_size( '280', '210', true ); 
        
        add_image_size( 'thumb-default', '280', '210', true );
        //add_image_size( 'thumb-loop', '400', '266', true );
        add_image_size( 'thumb-single', '760', '506', array( 'center', 'top' ) );
        add_image_size( 'hero', '1920', '320', array( 'center', 'top' ) );
        add_image_size( 'hero-full', '1920', '1080', true );
        add_image_size( 'hero-half', '767', '767', array( 'center', 'top' ) );

        // Add menu support
        register_nav_menus( array(
            'main' => _x( "Main menu in header", "Menu name", LWD_TEXT_DOMAIN ),
        ) );
        
        // Add post formats
        add_theme_support( 'post-formats', 
            array(  
                'gallery',
                'link',
                'video',
            ) 
        );
        
        add_post_type_support( 'post', 'post-formats' );
        
        // Custom login screen
        add_theme_support( 'ai-login-screen' );
        
    } );

    // Set up translations
    load_theme_textdomain( LWD_TEXT_DOMAIN, LWD_DIR . '/lwd-i18n' );

    add_action( 'after_switch_theme', 'flush_rewrite_rules' );

    /*----------------------------------------------------------------------------*
     * REGISTER CUSTOM POST TYPE AND TERMS
     *----------------------------------------------------------------------------*/

    add_action( 'init', function() {
        
        $testimonies_base = get_option( LWD_OPTIONS_NAME . '_testimony', 'testimonies' ) . '/';
        $services_base = get_option( LWD_OPTIONS_NAME . '_service', 'services' ) . '/';

        register_post_type( 'testimony', array(  
            'labels' =>  array(  
                'name' => __( 'Testimonies', LWD_TEXT_DOMAIN ),
                'singular_name' => __( 'Testimony', LWD_TEXT_DOMAIN ),
                'add_new' => __( 'New testimony', LWD_TEXT_DOMAIN ),
                'add_new_item' => __( 'Add new testimony', LWD_TEXT_DOMAIN ),
                'edit_item' => __( 'Edit testimony', LWD_TEXT_DOMAIN ),
                'new_item' => __( 'New testimony', LWD_TEXT_DOMAIN ),
                'all_items' => __( 'All testimonies', LWD_TEXT_DOMAIN )
            ),  
            'public' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,  
            'show_in_nav_menus' => true,
            'show_ui' => true,  
            'query_var' => true,  
            'rewrite' => array( 'slug' => $testimonies_base . _x( 'detail', 'Single post type rewrite slug', LWD_TEXT_DOMAIN ), 'with_front' => false ), 
            'has_archive' => false,
            'capability_type' => 'post',  
            'hierarchical' => false,  
            'menu_position' => null,
            'menu_icon' => 'dashicons-format-quote', 
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'taxonomies' => array( 'testimony_cat' ),
        ) );

        register_taxonomy( 'testimony_cat', array( 'testimony' ), array(
            'labels'                     => array(
                'name'                       => _x( 'Testimony category', 'Taxonomy General Name', LWD_TEXT_DOMAIN ),
            ),
            'hierarchical'               => true,
            'public'                     => false,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => false,
            'show_tagcloud'              => false,
            'rewrite'                    => array( 'slug' => $testimonies_base . _x( 'category', 'Cat rewrite slug',  LWD_TEXT_DOMAIN), 'with_front' => false )
        ) );

        register_post_type( 'service', array(  
            'labels' =>  array(  
                'name' => __( 'Services', LWD_TEXT_DOMAIN ),
                'singular_name' => __( 'Service', LWD_TEXT_DOMAIN ),
                'add_new' => __( 'New service', LWD_TEXT_DOMAIN ),
                'add_new_item' => __( 'Add new service', LWD_TEXT_DOMAIN ),
                'edit_item' => __( 'Edit service', LWD_TEXT_DOMAIN ),
                'new_item' => __( 'New service', LWD_TEXT_DOMAIN ),
                'all_items' => __( 'All services', LWD_TEXT_DOMAIN )
            ),  
            'public' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,  
            'show_in_nav_menus' => true,
            'show_ui' => true,  
            'query_var' => true,  
            'rewrite' => array( 'slug' => $services_base . _x( 'detail', 'Single post type rewrite slug', LWD_TEXT_DOMAIN ), 'with_front' => false ), 
            'has_archive' => false,
            'capability_type' => 'post',  
            'hierarchical' => false,  
            'menu_position' => null,
            'menu_icon' => 'dashicons-businessman', 
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'taxonomies' => array( 'service_cat' ),
        ) );

        register_taxonomy( 'service_cat', array( 'service' ), array(
            'labels'                     => array(
                'name'                       => _x( 'Service category', 'Taxonomy General Name', LWD_TEXT_DOMAIN ),
            ),
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => false,
            'rewrite'                    => array( 'slug' => $services_base . _x( 'category', 'Cat rewrite slug',  LWD_TEXT_DOMAIN), 'with_front' => false )
          ) );
        
    }, 10, 0 );

    /*----------------------------------------------------------------------------*
     * THEME FUNCTIONS
     *----------------------------------------------------------------------------*/

    // EXCERPT FUNCTION
    if ( ! function_exists( 'lwd_perex' ) ) {
        function lwd_perex( $text, $length = 5, $ending = " ..." ) {
            if ( strlen( $text  ) <= $length ) return $text;
            
            $text = preg_replace( '#\<p>[{\w},\s\d"]+\</p>#', "", $text );
            
            $text  = mb_substr( $text, 0, $length );
            $pos   = mb_strrpos( $text, " " );
            $text  = mb_substr( $text, 0, $pos );
            $text  = strip_shortcodes( $text );
            $text .= $ending;
            return $text;
        }
    }

    // CREATE ID
    if ( ! function_exists( 'lwd_create_id' ) ) {
        function lwd_create_id( $text ) {
            return sanitize_title( $text );
        }
    }

    //CHECK URL
    if ( ! function_exists( 'lwd_check_url' ) ) {
        function lwd_check_url( $url ) {
            if ( preg_match( '#http(s)?://#', $url ) ) {
                return $url;
        } else {
                return site_url( $url );
            }
        }
    }

    // GET SLUG FUNCTION
    if ( ! function_exists( 'lwd_the_slug' ) ) {
        function lwd_the_slug( $echo = true, $id = '' ){
            $slug = basename( get_permalink( $id ) );
            do_action( 'before_slug', $slug );
            $slug = apply_filters( 'slug_filter', $slug );
            if ( $echo ) echo $slug;
            do_action( 'after_slug', $slug );
            return $slug;
        }
    }

    // GET PAGE BY SLUG FUNCTION
    if ( ! function_exists( 'lwd_get_page_by_slug' ) ) {
        function lwd_get_page_by_slug( $page_slug, $output = OBJECT, $post_type = 'page' ) { 		
            global $wpdb;
            $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $page_slug, $post_type ) );
            if ( $page ) return get_page( $page, $output ); return null;
        }
    }

    // GET POST METAS
    if ( ! function_exists( 'lwd_get_post_metas' ) ) {
        function lwd_get_post_metas( $post_id ) { 		
            global $wpdb;
            $metas = $wpdb->get_results( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = '$post_id'", ARRAY_A );
            $return = array();
            foreach ( $metas as $meta ) {
                $return[$meta['meta_key']] = $meta['meta_value'];
            }
            return $return;
        }
    }

    //FORMAT PHONE NUMBER
    if ( ! function_exists( 'lwd_format_phone' ) ) {
        function lwd_format_phone( $phone_number, $spaces = true, $prefix = true ) {
            
            if ( strpos( $phone_number, '+' ) !== false ) {
                $phone_prefix = substr( $phone_number, 0, 4 );
            } else {
                $phone_prefix = _x( '+44', 'Default phone prefix', AI_TEXT_DOMAIN );
            }
            
            $phone_number = strtr( $phone_number, [
                $phone_prefix => '',
                ' ' => ''
            ] );

            if ( $spaces ) {
                if ( is_numeric( $phone_number ) && strlen( $phone_number ) == 9 && preg_match( '/^(\d{3})(\d{3})(\d{3})$/', $phone_number,  $matches ) ) {
                    $phone_number = ( $prefix ? $phone_prefix : '' ) . ' ' . $matches[1] . ' ' .$matches[2] . ' ' . $matches[3];
                }
            } else {
                $phone_number = ( $prefix ? $phone_prefix : '' ) . $phone_number;
            }

            return $phone_number;
        }
    }

    if ( ! function_exists( 'lwd_get_link' ) ) {
        function lwd_get_link( $post_ID = '', $meta_key = '', $metas = array() ) {
            
            if ( $post_ID == '' || $meta_key == '' ) return '';
            
            $link_type = isset( $metas[$meta_key] ) && $metas[$meta_key] != '' ? $metas[$meta_key] : get_post_meta( $post_ID, $meta_key, true );
            
            switch ( $link_type ) {
                case 'internal' :
                    $internal = isset( $metas[$meta_key . '-internal'] ) && $metas[$meta_key . '-internal'] != '' ? $metas[$meta_key . '-internal'] : get_post_meta( $post_ID, $meta_key . '-internal', true );
                    $link = [ 'url' => do_shortcode( $internal ), 'text' => ( strpos( $internal, '_url') !== false ? get_the_title( preg_replace( "/[^0-9]/", "", $internal ) ) : preg_replace('#^https?://#', '', rtrim( $internal, '/') ) ) ];
                    break;
                case 'external' :
                    $external = isset( $metas[$meta_key . '-external'] ) && $metas[$meta_key . '-external'] != '' ? $metas[$meta_key . '-external'] : get_post_meta( $post_ID, $meta_key . '-external', true );
                    $link = [ 'url' => $external, 'text' => preg_replace('#^https?://#', '', rtrim( $external, '/') ) ];
                    break;
                case 'phone' :
                    $phone = isset( $metas[$meta_key . '-phone'] ) && $metas[$meta_key . '-phone'] != '' ? $metas[$meta_key . '-phone'] : get_post_meta( $post_ID, $meta_key . '-phone', true );
                    $link = [ 'url' => 'tel:' . lwd_format_phone( $phone, false ), 'text' => lwd_format_phone( $phone ) ];
                    break;
                case 'e-mail' :
                    $email = isset( $metas[$meta_key . '-e-mail'] ) && $metas[$meta_key . '-e-mail'] != '' ? $metas[$meta_key . '-e-mail'] : get_post_meta( $post_ID, $meta_key . '-e-mail', true );
                    $link = [ 'url' => 'mailto:' . $email, 'text' => $email ];
                    break;
                case 'none' :
                    $link = [ 'url' => home_url( '/404/' ), 'text' => __( 'No link', LWD_TEXT_DOMAIN ) ];
                    break;
                case '' : default :
                    $link = [ 'url' => get_permalink( $post_ID ), 'text' => __( 'Detail', LWD_TEXT_DOMAIN ) ];
                    break;
            }
            
            return $link;
        }
    }

    /*----------------------------------------------------------------------------*
     * INITIALIZE THEME OPTIONS
     *----------------------------------------------------------------------------*/

    require_once LWD_ADMIN_DIR . '/theme-customizer.php';

    /*----------------------------------------------------------------------------*
     * LOAD OPTION TREE
     *----------------------------------------------------------------------------*/

    // Filters the Theme Options ID 
    add_filter( 'ot_options_id', 'lwd_options_id' );

    function lwd_options_id() {
        return 'lwd_option_tree';
    }

    //Filters the Settings ID
    add_filter( 'ot_settings_id', 'lwd_settings_id' );

    function lwd_settings_id() {
        return 'lwd_option_tree_settings';
    }

    // Filters the Theme Option header list.
    add_action( 'ot_header_list', 'lwd_header_list' );

    function lwd_header_list() {
       echo '<li id="theme-version"><span>' . LWD_NAME . ' ' . LWD_VERSION . '</span></li>';
    }

    // Theme Mode
    add_filter( 'ot_theme_mode', '__return_true' );

    // Show Settings Pages
    add_filter( 'ot_show_pages', '__return_false' );


    // Custom Theme Options page
    add_filter( 'ot_use_theme_options', '__return_false' );

    // Loads the meta boxes for post formats
    add_filter( 'ot_post_formats', '__return_true' );

    // Required: set 'ot_override_forced_textarea_simple' filter to true.
    add_filter( 'ot_override_forced_textarea_simple', '__return_true' );

    // OptionTree in Theme Mode
    require( LWD_ADMIN_INCLUDES_DIR. '/option-tree-master/ot-loader.php' );

    /*----------------------------------------------------------------------------*
 	 * LOAD ELEMENTOR
 	 *----------------------------------------------------------------------------*/

    if ( class_exists( 'Elementor\Plugin' ) ) require_once LWD_DIR . '/elementor/init.php';

    // Force disable Elementor updates
    add_filter( 'site_transient_update_plugins', function( $updates ) {
        if ( isset( $updates->response[ 'elementor/elementor.php' ] ) ) unset( $updates->response[ 'elementor/elementor.php' ] );
        
         return $updates;
    } );

    /*----------------------------------------------------------------------------*
     * LOAD ADMIN AND FRONT SCRIPTS
     *----------------------------------------------------------------------------*/

    if ( is_admin() ) {
        require_once LWD_ADMIN_DIR . '/theme-admin.php';
        
        // Load post duplicator
        require_once LWD_ADMIN_DIR . '/theme-duplicate-post.php';
        
        // Load Video support for gallery shortcode
        require_once LWD_ADMIN_DIR . '/theme-video-gallery.php';
    } else {
        require_once LWD_DIR . '/frontend/theme.php';
    }

    /*----------------------------------------------------------------------------*
 	 * LOAD THEME WIDGETS
 	 *----------------------------------------------------------------------------*/
     
    require_once LWD_INCLUDES_DIR . '/widgets.php';

    /*----------------------------------------------------------------------------*
 	 * REGISTER SHORTCODES
 	 *----------------------------------------------------------------------------*/	
 	
    if ( !is_admin() ) {
        require_once LWD_INCLUDES_DIR . '/shortcodes.php';
        require_once LWD_INCLUDES_DIR . '/shortcodes/gallery.php';    
    }

    /*----------------------------------------------------------------------------*
 	 * LOAD MEMBER ZONE
 	 *----------------------------------------------------------------------------*/
     
    require_once LWD_DIR . '/member-zone/init.php';
