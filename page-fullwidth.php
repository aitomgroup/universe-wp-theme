<?php
/**
 * The template for displaying Elementor content
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

get_header(); 

    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();?>

            <article id="<?php the_ID(); ?>" <?php echo post_class(); ?>>
                
                <?php

                    get_template_part( 'partials/hero' );
                
                    do_action( 'lwd_before_content' );

                    the_content();
                
                    do_action( 'lwd_after_content' );

                ?>

                
            </article>

        <?php endwhile;
    endif;

get_footer(); ?>