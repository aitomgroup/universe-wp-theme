<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 */

get_header();

    get_template_part( 'partials/hero' ); ?>

    <section class="section">
        
        <?php
            
            global $wp_query, $archive_style, $archive_cols, $archive_align;
        
            /*switch ( $post->post_type ) {
                case 'service' :
                    $post_style = 'g-g4';
                    break;
                case 'testimony' :
                    $post_style = 'g-g2';
                    break;
                case 'post' : default :
                    $post_style = 'g-g2';
                    break;
            }

            if ( $post_style == 'g-g3' || $post_style == 'g-g1' ) $post_style = 'g-g2';*/

            $archive_style = apply_filters( 'lwd_filter_archive_style', 'g-g2' );
            $archive_cols = apply_filters( 'lwd_filter_archive_cols', 3 );
            $archive_align = apply_filters( 'lwd_filter_archive_align', 'left' );
            $archive_cats_filter = is_home() || is_category() || is_tag() || is_tax() ? apply_filters( 'lwd_filter_archive_cats_filter', false, $wp_query->queried_object ) : false;
        
            if ( $archive_cats_filter ) {
                
                if ( is_home() || is_category() || is_tag() ) {
                    $archive_link = get_post_type_archive_link( 'post' );
                } else {
                    global $post;
                    if ( !empty( $post ) && isset( $post->post_type ) ) $archive_link = get_post_type_archive_link( $post->post_type );
                }
                
                $tax = 'category';
                
                if ( is_home() ) {
                    $cats = get_categories( [ 'hide_empty' => true ] );
                } else {
                    if ( isset( $wp_query->queried_object->taxonomy ) ) {
                        $tax = $wp_query->queried_object->taxonomy;
                        $cats = get_terms( [ 'taxonomy' => $tax, 'hide_empty' => true ] );
                    }
                }
        
                if ( !empty( $cats ) ) { ?>
                    <div class="filter content">
                        <?php
                                        
                            if ( isset( $archive_link ) && $archive_link != '' ) {
                                echo '<a href="' . $archive_link . '" class="filter__btn' . ( !isset( $wp_query->queried_object->taxonomy ) || $wp_query->queried_object->taxonomy == '' ? ' filter__all--checked' : '' ) . ' js_loadFade">' . __( 'All', LWD_TEXT_DOMAIN ) . '</a>';
                            }
                                        
                            foreach ( $cats as $cat ) {
                                echo '<a href="' . get_term_link( $cat, $tax ) . '" class="filter__btn' . ( isset( $wp_query->queried_object->term_id ) && $wp_query->queried_object->term_id == $cat->term_id ? ' filter__all--checked' : '' ) .' js_loadFade">' . $cat->name . '</a>';
                            }
                        ?>
                    </div>
                <?php }
            
            }
        
        ?>
        
        <div class="content">
            
            <div class="<?php echo $archive_style != '' ? $archive_style . ( $archive_cols != '' ? ' ' . $archive_style . '--' . $archive_cols : '' ) . ( $archive_align != '' ? ' ' . $archive_style . '--' . $archive_align : '' ) : ''; ?> pure-g">

            <?php

                /* Run the loop to output the post.
                 * If you want to overload this 
                 * in a child theme then include a file
                 * called loop-single.php and that
                 * will be used instead.
                 */

                get_template_part( 'loop' );

            ?>
                
            </div>
            
            <?php if ( function_exists( 'ai_paginator' ) ) {
                ai_paginator( $wp_query->max_num_pages );
            } ?>

        </div>
    </section>
 				
<?php get_footer(); ?>