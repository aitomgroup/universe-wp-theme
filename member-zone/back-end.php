<?php

/**
 * back-end.php
 *
 * - Display restrict state in admin all list
 * - Fix to allow restricted post types in admin all list
 * - Add new post status to existing status options in quick edit
 * - Add new post status to existing status options in submit box
 * - Update new post status on save post
 * - Enable restricted post types to be added to menu
 * - Register new menu for restricted pages
 * - Hide meta boxes if is content restricted
 */

    // Prevent direct script access
    if ( !defined( 'ABSPATH' ) ) exit;

    /*----------------------------------------------------------------------------*
 	 * DISPLAY RESTRICT STATE IN ADMIN ALL LIST
 	 *----------------------------------------------------------------------------*/

    add_filter( 'display_post_states', function( $states ) {
        global $post, $post_type_to_restrict;
        
         if ( !isset( $_GET['post_status'] ) || $_GET['post_status'] != 'restrict' ) {
              if ( isset( $post->post_status ) && $post->post_status == 'restrict' && isset( $post->post_type ) && !empty( $post_type_to_restrict ) && in_array( $post->post_type, $post_type_to_restrict ) ) {
                   $states[] = __( 'Only for logged-in users', LWD_MZ_DOMAIN );
              }
         }
        
        return $states;
    }, 10, 1 );

    /*----------------------------------------------------------------------------*
 	 * FIX TO ALLOW RESTRICTED POST TYPES IN ADMIN ALL LIST
 	 *----------------------------------------------------------------------------*/

    add_action( 'pre_get_posts', function( $query ) {
        global $pagenow;

        if ( !is_admin() || !isset( $query->query['post_type'] ) || $query->query['post_type'] != 'restrict' || empty( $pagenow ) || $pagenow != 'edit.php' )
            return;

        if ( !isset( $_GET['post_status'] ) || empty( $_GET['post_status'] ) || ( isset( $_GET['post_status'] ) && $_GET['post_status'] == 'all') )
            $query->set( 'post_status', [ 'publish', 'draft', 'restrict' ] );
        
    } );

    /*----------------------------------------------------------------------------*
 	 * ADD NEW POST STATUS TO EXISTING STATUS OPTIONS IN QUICK EDIT
 	 *----------------------------------------------------------------------------*/

    add_action( 'admin_enqueue_scripts', function( $hook ) {
        global $post_type_to_restrict;
        
        if ( 'edit.php' === $hook && !empty( $post_type_to_restrict ) && ( !isset( $_GET['post_type'] ) || in_array( $_GET['post_type'], $post_type_to_restrict ) ) ) {
            
            $restrict_status = get_post_status_object( 'restrict' );
            
            if ( empty( $restrict_status ) ) return;

            if ( !wp_script_is( 'jquery', 'done' ) ) wp_enqueue_script( 'jquery' );

            wp_add_inline_script( 'jquery-migrate', 'jQuery(document).ready(function($) {
                var $postStatus = $(".inline-edit-status select");
                
                if ( $postStatus.find("option[value=\"' . $restrict_status->name . '\"]").length == 0 ) {
                    $postStatus.append("<option value=\"' . $restrict_status->name . '\">' . $restrict_status->label . '</option>");
                }
                
            });' );
        
        }
        
    } );

    /*----------------------------------------------------------------------------*
 	 * ADD NEW POST STATUS TO EXISTING STATUS OPTIONS IN SUBMIT BOX
 	 *----------------------------------------------------------------------------*/

    add_action( 'post_submitbox_misc_actions', function( $post ) {
        global $post_type_to_restrict;
        
        $restrict_status = get_post_status_object( 'restrict' );
    
        if ( !empty( $restrict_status ) && isset( $restrict_status->name ) && isset( $restrict_status->label ) && isset( $post->post_type ) && !empty( $post_type_to_restrict ) && in_array( $post->post_type, $post_type_to_restrict ) ) {

            $post_status = !empty( $post ) && isset( $post->post_status ) ? $post->post_status : 'draft' ?>

            <style>
                #misc-publishing-actions.restrict .misc-pub-section.yoast,
                #misc-publishing-actions.restrict .misc-pub-post-status .force-hide-edit + .edit-post-status {
                    display: none !important
                }
            </style>

            <script>
                jQuery(document).ready(function($) {
                    
                    var post_status = '<?php echo $post_status; ?>',
                        custom_status = '<?php echo $restrict_status->name; ?>',
                        $postStatus = $('#post_status'),
                        $postVisibilitySelect = $('#post-visibility-select');
                    
                    // Add new visibility option
                    $('#post-visibility-select #password-span').after('<span class="<?php echo $restrict_status->name; ?>-span"><input type="radio" name="visibility" id="visibility-radio-<?php echo $restrict_status->name; ?>" value="<?php echo $restrict_status->name; ?>"<?php echo $restrict_status->name == $post_status ? ' checked="checked"' : ''; ?>> <label for="visibility-radio-<?php echo $restrict_status->name; ?>" class="selectit"><?php echo $restrict_status->label; ?></label></span><br>');
                    
                        // TO-DO: Add checkbox to select allowed roles
                    
                    // Init on load new option
                    if ( post_status == custom_status ) updateCustomVisibility();

                    // Set the selected visibility as current.
                    $postVisibilitySelect.find('.save-post-visibility').click( function( event ) {
                        if ( $('#visibility-radio-<?php echo $restrict_status->name; ?>').is(':checked') ) {
                            updateCustomVisibility();
                        } else {
                            $('#misc-publishing-actions').removeClass( 'restrict' );
                            $('#post-status-display').removeClass( 'force-hide-edit' );
                        }
                    });
                    
                    function updateCustomVisibility() {
                        
                        $('#misc-publishing-actions').addClass( 'restrict' );
        
                        $('#post-visibility-display').html('<?php echo $restrict_status->label; ?>');
                        $('#post-status-display').html( postL10n.published ).addClass( 'force-hide-edit' );
                        
                        $('#hidden-post-visibility').val( custom_status );
                        
                        if ( $('option[value="publish"]', $postStatus).length > 0 ) {
                            $('option[value="publish"]', $postStatus).html( postL10n.published );
                        } else {
                            $postStatus.append('<option value="publish">' + postL10n.published + '</option>');
                        }
                        
                        $('option[value="publish"]', $postStatus).prop('selected', true);
                        
                        $('#misc-publishing-actions .edit-post-status, #save-action #save-post').hide();
                    
                        $('#publish').val( postL10n.update );
                        
                    }
                
                    // Fix focus on selected visibility option
                    $('#visibility .edit-visibility').click( function( e ) {
                        e.preventDefault();
                        
                        if ( $postVisibilitySelect.is(':hidden') ) {
                            setTimeout(function(){
                                $postVisibilitySelect.find( 'input[type="radio"]:checked' ).focus();
                            }, 300);
                        }
                    });
                });
            </script>

        <?php }
        
    } );

    /*----------------------------------------------------------------------------*
 	 * UPDATE NEW POST STATUS ON SAVE POST
 	 *----------------------------------------------------------------------------*/

    add_filter( 'wp_insert_post_data', function( $data, $postarr ) {
        global $post_type_to_restrict;
        
        if ( isset( $postarr['visibility'] ) && $postarr['visibility'] == 'restrict' && isset( $postarr['post_type'] ) && !empty( $post_type_to_restrict ) && in_array( $postarr['post_type'], $post_type_to_restrict ) ) {
            $data['post_status'] = 'restrict';
            $data['post_password'] = '';
        }

        return $data;
    }, 10, 2 );

    /*----------------------------------------------------------------------------*
 	 * ENABLE RESTRICTED POST TYPES TO BE ADDED TO MENU
 	 *----------------------------------------------------------------------------*/

    add_filter( 'nav_menu_meta_box_object', function( $object = null ) {
        if ( isset( $object->name ) ) {
            
            global $post_type_to_restrict;

            if ( !empty( $post_type_to_restrict ) && in_array( $object->name, $post_type_to_restrict ) ) {
                if ( !isset( $object->_default_query['post_status'] ) ) {
                    $object->_default_query['post_status'] = [ 'publish', 'restrict' ];
                } elseif ( !is_array( $object->_default_query['post_status'] ) ) {
                    $object->_default_query['post_status'] = [ $object->_default_query['post_status'], 'restrict' ];
                } elseif ( is_array( $object->_default_query['post_status'] ) && !in_array( 'restrict', $object->_default_query['post_status'] ) ) {
                    $object->_default_query['post_status'][] = 'restrict';
                }
            }
            
        }

        return $object;
    }, 10, 1 );

    /*----------------------------------------------------------------------------*
 	 * REGISTER NEW MENU FOR RESTRICTED PAGES
 	 *----------------------------------------------------------------------------*/

    add_action( 'after_setup_theme', function() {

        // Add menu support
        register_nav_menus( [
            'restrict-menu' => _x( "Menu for member zone", "Menu name", LWD_MZ_DOMAIN ),
        ] );
        
    } );

    /*----------------------------------------------------------------------------*
 	 * HIDE META BOXES IF IS CONTENT RESTRICTED
 	 *----------------------------------------------------------------------------*/

    add_action( 'do_meta_boxes', function() {
        global $post, $post_type_to_restrict;
        
        if ( !empty( $post ) && isset( $post->post_status ) && $post->post_status == 'restrict' && isset( $post->post_type ) && !empty( $post_type_to_restrict ) && in_array( $post->post_type, $post_type_to_restrict ) ) remove_meta_box( 'wpseo_meta' , $post->post_type , 'normal' ); 
    } );