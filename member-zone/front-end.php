<?php

/**
 * front-end.php
 *
 * - Restrict page content for non logged users
 * - Set page template for virtual pages
 * - Reset SQL request for virtual pages
 * - Set content for virtual pages according query var
 * - Disable auto p content for member zone core pages
 * - Filter restrict pages in menu for non-logged users
 * - Prepend member zone menu to restricted pages
 * - Hide admin role for specific roles
 */

    // Prevent direct script access
    if ( !defined( 'ABSPATH' ) ) exit;

    /*----------------------------------------------------------------------------*
     * RESTRICT PAGE CONTENT FOR NON LOGGED USERS
     *----------------------------------------------------------------------------*/

    add_action( 'pre_get_posts', function( $query ) {
        
        if ( is_admin() || !$query->is_main_query() ) return;

        // Check if is restricted page
        if ( is_user_logged_in() ) {
            $query->set( 'post_status', [ 'restrict', 'publish' ] );
        }
        
        if ( is_singular() && isset( $query->queried_object->post_status ) && $query->queried_object->post_status == 'restrict' ) {
            //$query->query_vars[ 'post_status' ] = 'restrict, publish';
            
            // If is NOT user logged in than redirect to login page
            if ( is_user_logged_in() ) {
                
                // Set restricted content to publish
                add_filter( 'posts_results', function( $posts ) {
                    global $post_type_to_restrict;
                    
                    foreach ( $posts as $id => $post ) {
                        if ( isset( $post->post_status ) && $post->post_status == 'restrict' && isset( $post->post_type ) && !empty( $post_type_to_restrict ) && in_array( $post->post_type, $post_type_to_restrict ) ) {
                            $posts[$id]->post_status = 'publish';
                            
                            set_query_var( 'restrict', true );
                        }
                    }

                    return $posts;
                }, 10, 1 );
                
            } else {
                
                wp_redirect( LWD_MZ_HOME_URL . 'login' ); // Redirect to login page
                exit();
                
                return $query;
                
            }
            
        }
        
        if ( get_query_var( 'mz', false ) !== false  && get_query_var( 'mz' ) != 'welcome' && is_user_logged_in() ) {
            
            wp_redirect( LWD_MZ_HOME_URL ); // Redirect to welcome page
            exit();
                
            return $query;
                
        }
        
        // Create virtual pages according query vars
        if ( get_query_var( 'mz', false ) !== false ) {
        
            $query->is_page = true;
            //$query->is_singular = true;
            $query->is_home = false;
            $query->is_archive = false;
            $query->is_category = false;
            
            unset( $query->query["error"] );
            $query->query_vars["error"] = "";
            
            $query->is_404 = false;
             
        }
        
    } );

    /*----------------------------------------------------------------------------*
     * SET PAGE TEMPLATE FOR VIRTUAL PAGES
     *----------------------------------------------------------------------------*/

    add_action( 'template_include', function( $template ) {
        
        if ( get_query_var( 'mz', false ) !== false ) {
 
            $newTemplate = locate_template( [ 'page-fullwidth.php', 'page.php' ] );
            
            if ( '' != $newTemplate )
                return $newTemplate;

        }
        
        return $template;
    } );

    /*----------------------------------------------------------------------------*
     * RESET SQL REQUEST FOR VIRTUAL PAGES
     *----------------------------------------------------------------------------*/

    add_filter( 'posts_request', function( $request, $wp_query ) {
        
        if ( is_admin() || !is_main_query() ) return $request;
        
        if ( get_query_var( 'mz', false ) !== false ) $request = '';
        
        return $request;
    }, 10, 2 );

    /*----------------------------------------------------------------------------*
     * SET CONTENT FOR VIRTUAL PAGES ACCORDING QUERY VAR
     *----------------------------------------------------------------------------*/

    add_filter( 'posts_results', function( $posts, $wp_query ) {
        
        if ( is_admin() || !is_main_query() ) return $posts;

        if ( get_query_var( 'mz', false ) !== false ) {
            
            $mz_title = __( 'Member zone', LWD_MZ_DOMAIN );
            
            ob_start();
            get_template_part( 'member-zone/templates/mz', 'login' );
            $mz_content = ob_get_contents();
            ob_end_clean();
            
            if ( get_query_var( 'mz' ) == 'welcome' && is_user_logged_in() ) {
                ob_start();
                get_template_part( 'member-zone/templates/mz', 'welcome' );
                $mz_content = ob_get_contents();
                ob_end_clean();
                
                // Prepend Member zone menu
                $mz_content = lwd_create_mz_menu( wp_nav_menu( [
                    'container'       => 'div',
                    'container_class' => 'submenu',
                    'echo'            => false,
                    'menu_class'      => 'submenu__list',
                    'theme_location'  => 'restrict-menu',
                    'fallback_cb'     => false,
                    'depth'           => 1,
                    'items_wrap'      => '<ul id="%1$s" class="%2$s"><li class="submenu__i js_loadFadeDelayed"><a href="' . LWD_MZ_HOME_URL . '" class="submenu__i__link submenu__i__link--active">' . __( 'User settings', LWD_MZ_DOMAIN ) . '</a></li>%3$s</ul>'
                ] ) ) . $mz_content;
                
            } elseif ( get_query_var( 'mz' ) == 'register' ) {
                ob_start();
                get_template_part( 'member-zone/templates/mz', 'register' );
                $mz_content = ob_get_contents();
                ob_end_clean();
            } elseif ( get_query_var( 'mz' ) == 'login' && isset( $_GET['action'] ) && $_GET['action'] == 'lostpassword' ) {
                ob_start();
                get_template_part( 'member-zone/templates/mz', 'lostpassword' );
                $mz_content = ob_get_contents();
                ob_end_clean();
            }

            //create a fake post
            $mz_page = new stdClass;
            $mz_page->post_author = 1;
            $mz_page->post_title = $mz_title;
            $mz_page->post_name = sanitize_title( $mz_page->post_title );
            $mz_page->guid = LWD_MZ_HOME_URL;
            $mz_page->post_content = $mz_content;
            $mz_page->post_excerpt = '';
            $mz_page->ID = 0;
            $mz_page->post_type = 'page';
            $mz_page->post_status = 'publish';
            $mz_page->post_parent = 0;
            $mz_page->comment_status = 'closed';
            $mz_page->ping_status = 'closed';
            $mz_page->comment_count = 0;
            //dates may need to be overwritten if you have a "recent posts" widget or similar - set to whatever you want
            $mz_page->post_date = current_time( 'mysql' );
            $mz_page->post_date_gmt = current_time( 'mysql', 1 );
            $mz_page->ancestors = [];

            $posts = NULL;
            $posts[] = $mz_page;

            $wp_query->queried_object = $mz_page;
        }

        return $posts;
    }, 10, 2 );

    /*----------------------------------------------------------------------------*
     * DISABLE AUTO P CONTENT FOR MEMBER ZONE CORE PAGES
     *----------------------------------------------------------------------------*/

    add_filter( 'the_content', function( $content ) {
        get_query_var( 'mz', false ) !== false && remove_filter( 'the_content', 'wpautop' );
        
        return $content;
    }, 0 );

    /*----------------------------------------------------------------------------*
 	 * FILTER RESTRICT PAGES IN MENU FOR NON-LOGGED USERS
 	 *----------------------------------------------------------------------------*/

    add_filter( 'wp_get_nav_menu_items', function( $items, $menu, $args ) {
        if ( is_user_logged_in() || !apply_filters( 'lwd_filter_mz_nav_items', true ) )  return $items;
        
        foreach ( $items as $key => $item ) {
            if ( !isset( $item->object ) || $item->object == 'custom' || !isset( $item->object_id ) || $item->object_id == '' ) continue;
            
            if ( get_post_status( $item->object_id ) == 'restrict' ) unset( $items[$key] );
        }

        return $items;
    }, null, 3 );

    /*----------------------------------------------------------------------------*
     * PREPEND MEMBER ZONE MENU TO RESTRICTED PAGES
     *----------------------------------------------------------------------------*/

    add_filter( 'the_content', function( $content ) {        
        if ( get_query_var( 'restrict', false ) == true ) {
             $content = lwd_create_mz_menu( wp_nav_menu( [
                'container'       => 'div',
                'container_class' => 'submenu',
                'echo'            => false,
                'menu_class'      => 'submenu__list',
                'theme_location'  => 'restrict-menu',
                'fallback_cb'     => false,
                'depth'           => 1,
                'items_wrap'      => '<ul id="%1$s" class="%2$s"><li class="submenu__i js_loadFadeDelayed"><a href="' . LWD_MZ_HOME_URL . '" class="submenu__i__link">' . __( 'User settings', LWD_MZ_DOMAIN ) . '</a></li>%3$s</ul>'
            ] ) ) . $content;
        }
        
        return $content;
    }, 19 );

    function lwd_create_mz_menu( $menu_string ) {
        
        $menu_string = preg_replace_callback( '/<li(.*)class="menu-item (.*)"><a href="(.*)">(.*)<\/a><\/li>/iU', function( $m ){
            return '<li' . $m[1] . 'class="submenu__i js_loadFadeDelayed menu-item ' . $m[2] . '"><a href="' . $m[3] . '" class="submenu__i__link' . ( strpos( $m[2], 'current-menu-item' ) !== false || strpos( $m[2], 'current_page_item' ) !== false ? ' submenu__i__link--active' : '' ) . '">' . $m[4] . '</a></li>';
        }, $menu_string );
        
        return $menu_string;
    }

    /*----------------------------------------------------------------------------*
     * HIDE ADMIN ROLE FOR SPECIFIC ROLES
     *----------------------------------------------------------------------------*/

    add_filter( 'show_admin_bar' , function() {
        $user = wp_get_current_user();
        $allowed_roles = [ 'administrator', 'editor', 'author' ];
        return array_intersect( $allowed_roles, $user->roles ) ? true : false;
    } );