<?php

/**
 * init.php
 *
 * - Setup member zone
 * - Force allow user registration
 * - Extend registration form of first and last name
 * - Front-end and admin common functions
 */

    // Prevent direct script access
    if ( !defined( 'ABSPATH' ) ) exit;

    // Define constants

    defined( 'LWD_MEMBER_ZONE' )
        || define ( 'LWD_MEMBER_ZONE', true );

    defined( 'LWD_MEMBER_ZONE_DIR' )
        || define ( 'LWD_MEMBER_ZONE_DIR', LWD_DIR . '/member-zone' );

    defined( 'LWD_MZ_DOMAIN' )
        || define( 'LWD_MZ_DOMAIN', LWD_SLUG . '-mz' );

    if ( defined( 'LWD_MEMBER_ZONE' ) && LWD_MEMBER_ZONE ) {
        $post_type_to_restrict = [];

        // Set up member zone rewrite base
        $mz_base = _x( 'member-zone', 'Member zone rewrite  base', LWD_MZ_DOMAIN );
        $mz_base = apply_filters( 'lwd_filter_mz_rewrite_base', $mz_base ); 
        $mz_base = sanitize_title( $mz_base );

        // Set up member zone welcome url
        defined( 'LWD_MZ_HOME_URL' )
            || define( 'LWD_MZ_HOME_URL', home_url( '/' . $mz_base . '/' ) );

        // Set transient to flush rewrite role
        add_action( 'after_switch_theme', function() {
            set_transient( 'lwd_mz_rewrite_flush', 1, 60 );
        } );

        // Set up translations
        load_theme_textdomain( LWD_MZ_DOMAIN, LWD_MEMBER_ZONE_DIR . '/lwd-i18n' );

        add_action( 'init', function() {

            global $post_type_to_restrict;

            // Set global option of allowed post types to restrict
            $post_type_to_restrict = get_post_types( [
                'public' => true,
                'exclude_from_search' => false
            ] );

            // Register new post status
            register_post_status( 'restrict', [
                'label'                     => _x( 'Only for logged-in users', 'Restrict post status', LWD_MZ_DOMAIN ),
                'public'                    => false,
                'exclude_from_search'       => true,
                'show_in_admin_all_list'    => true, // Set false to hide item with this status from All list
                'protected'                 => true,
                'show_in_admin_status_list' => true,
                'label_count'               => _n_noop( 'Restricted <span class="count">(%s)</span>', 'Restricted <span class="count">(%s)</span>', LWD_MZ_DOMAIN )
            ] );

            // Add rewrite rule for virtual member pages
            global $mz_base;

            add_rewrite_rule( '^' . $mz_base . '$', 'index.php?mz=welcome', 'top' );
            add_rewrite_rule( '^' . $mz_base . '/([^/]+)/?$', 'index.php?mz=$matches[1]', 'top' );

            if ( get_transient( 'lwd_mz_rewrite_flush' ) ) {
                delete_transient( 'lwd_mz_rewrite_flush' );
                flush_rewrite_rules();
            }

            // Re-order post statuses - push 'trash' on the end
            if ( !empty( $post_type_to_restrict ) && is_admin() ) {

                foreach ( $post_type_to_restrict as $post_type ) {

                    add_filter( 'views_edit-' . $post_type, function( $views ) {

                        if ( isset( $views['restrict'] ) && isset( $views['trash'] ) ) {
                            $add_trash_after = $views['trash'];

                            unset( $views['trash'] );

                            $views['trash'] = $add_trash_after;
                        }

                        return $views;
                    }, 10,  1 );

                }

            }

        } );

        // Add new query vars for rewriting virtual pages
        add_action( 'query_vars', function( $vars ) {
            if ( !in_array( 'mz', $vars ) ) $vars[] = 'mz';
            if ( !in_array( 'restrict', $vars ) ) $vars[] = 'restrict';

            return $vars;
        } );

        /*----------------------------------------------------------------------------*
         * FORCE ALLOW USER REGISTRATION
         *----------------------------------------------------------------------------*/

        add_filter( 'option_users_can_register', '__return_true' );

        add_filter( 'option_default_role', function() {
            return 'subscriber';
        } );

        /*----------------------------------------------------------------------------*
         * EXTEND REGISTRATION FORM OF FIRST AND LAST NAME
         *----------------------------------------------------------------------------*/

        //1. Add a new form element...
        add_action( 'register_form', function() {
            echo '<p><label for="first_name">'.__( 'Name', LWD_MZ_DOMAIN ).'<br><input type="text" name="first_name" id="first_name" class="input" value="' . ( ! empty( $_POST['first_name'] ) ? trim( $_POST['first_name'] ) : '' ) . '" size="25"></label></p>';
            echo '<p><label for="last_name">'.__( 'Surname', LWD_MZ_DOMAIN ).'<br><input type="text" name="last_name" id="last_name" class="input" value="' . ( ! empty( $_POST['last_name'] ) ? trim( $_POST['last_name'] ) : '' ) . '" size="25"></label></p>';
        } );

        //2. Add validation. In this case, we make sure first_name is required
        /*add_filter( 'registration_errors', function( $errors, $sanitized_user_login, $user_email ) {

            if ( empty( $_POST['first_name'] ) || ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) {
                $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'mydomain' ) );
            }

            return $errors;
        }, 10, 3 );*/

        //3. Finally, save our extra registration user meta
        add_action( 'user_register', function( $user_id ) {
            if ( ! empty( $_POST['first_name'] ) ) update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
            if ( ! empty( $_POST['last_name'] ) ) update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
        } );

        /*----------------------------------------------------------------------------*
         * LOAD ADMIN AND FRONT FUNCTIONS
         *----------------------------------------------------------------------------*/

        if ( is_admin() ) {
            require_once LWD_MEMBER_ZONE_DIR . '/back-end.php';
        } else {
            require_once LWD_MEMBER_ZONE_DIR . '/front-end.php';  
        }
    }