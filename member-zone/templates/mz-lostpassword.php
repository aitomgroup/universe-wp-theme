<?php
/**
 * The template for display password recovery screen of Member zone
 */

// Add subpage name to wp title
remove_filter( 'wp_title', 'lwd_mz_filte_default_wp_title' );

add_filter( 'wp_title', function( $title, $sep, $seplocation ) {
    return __( 'Forgotten password', LWD_MZ_DOMAIN ) . ' ' . $title;
}, 10, 3 );

?>

<section class="section">
	<div class="content">
        <div class="title-perex">
            <h2 class="title-perex__title"><?php _e( 'Forgotten password', LWD_MZ_DOMAIN ); ?></h2>
			<p class="title-perex__perex"><?php _e( 'Forgot your password? Let us send you a new one to your email.', LWD_MZ_DOMAIN ); ?></p>
		</div>
        
        <form action="<?php echo wp_lostpassword_url(); ?>" method="post" id="lostpasswordform" class="form1 client-zone-form">
            <div class="client-zone-form__row">
                <label for="user_login" class="form1__label"><?php _e( 'Enter your email', LWD_MZ_DOMAIN ); ?>:</label>
			</div>
			<div class="client-zone-form__row">
                <input type="text" name="user_login" id="user_login" class="form1__input" />
			</div>
            <div class="client-zone-form__row">
                <input type="submit" name="submit" value="<?php _e( 'Send a new password', LWD_MZ_DOMAIN ); ?>" class="form1__btn client-zone-form__submit" />
			</div>
            <div class="client-zone-form__row">
                <p class="txt-center"><a href="<?php echo LWD_MZ_HOME_URL . 'login'; ?>"><?php _e( 'Back to login', LWD_MZ_DOMAIN ); ?></a></p>
            </div>
            <input type="hidden" name="redirect_to" value="<?php echo LWD_MZ_HOME_URL . 'login?notice=success_reset'; ?>" />
        </form>
        
	</div>
</section>