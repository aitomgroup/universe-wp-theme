<?php
/**
 * The template for display welcome screen of Member zone
 */

// Add subpage name to wp title
remove_filter( 'wp_title', 'lwd_mz_filte_default_wp_title' );

add_filter( 'wp_title', function( $title, $sep, $seplocation ) {
    return __( 'User settings', LWD_MZ_DOMAIN ) . ' ' . $title;
}, 10, 3 );

$current_user = wp_get_current_user();

$email = isset( $_POST['user_email'] ) && $_POST['user_email'] != '' ? trim( $_POST['user_email'] ) : ( isset( $current_user->user_email ) && $current_user->user_email != '' ? $current_user->user_email : '' );

$first_name = isset( $_POST['first_name'] ) && $_POST['first_name'] != '' ? trim( $_POST['first_name'] ) : ( isset( $current_user->user_firstname ) && $current_user->user_firstname != '' ? $current_user->user_firstname : '' );

$last_name = isset( $_POST['last_name'] ) && $_POST['last_name'] != '' ? trim( $_POST['last_name'] ) : ( isset( $current_user->user_lastname ) && $current_user->user_lastname != '' ? $current_user->user_lastname : '' );

$meta_error = $pass_error = [];

// Verify and save user data
if ( isset( $_POST['_submit_edit'] ) ) {
    
    if ( ! isset( $_POST['_lwd_mz_user_wpnonce'] ) || ! wp_verify_nonce( $_POST['_lwd_mz_user_wpnonce'], 'edit_user_meta_' . $current_user->ID ) ) {

       $meta_error[] = 'Sorry, your nonce did not verify.';

    } else {
        
        /* Update user information. */
        if ( !empty( $_POST['user_email'] ) ) {
            
            if ( !is_email( esc_attr( $_POST['user_email'] ) ) ) {
                
            	$meta_error[] = __( 'The email you entered is not valid, please review it.', LWD_MZ_DOMAIN );
                
            } else if ( email_exists( esc_attr( $_POST['user_email'] ) ) && email_exists( esc_attr( $_POST['user_email'] ) ) != $current_user->ID ) {
                
            	$meta_error[] = __( 'This email is already registered by another user, please try another one.', LWD_MZ_DOMAIN );
                
            } else {
                
            	wp_update_user( [
                    'ID' => $current_user->ID,
                    'user_email' => esc_attr( $_POST['user_email'] )
                ] );
                
        	}
            
    	}
    		
    	if ( !empty( $_POST['first_name'] ) ) {
        	update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first_name'] ) );
        }
        
    	if ( !empty( $_POST['last_name'] ) ) {
        	update_user_meta( $current_user->ID, 'last_name', esc_attr( $_POST['last_name'] ) );
        }		
        
        //action hook for plugins and extra fields saving
        /*if ( count( $error ) == 0 ) {
        	do_action( 'edit_user_profile_update', $current_user->ID );
    	}*/
        
    }
    
}

// Verify and save new user password
if ( isset( $_POST['_submit_pass'] ) ) {
    
    if ( ! isset( $_POST['_lwd_mz_pass_wpnonce'] ) || ! wp_verify_nonce( $_POST['_lwd_mz_pass_wpnonce'], 'edit_user_pass_' . $current_user->ID ) ) {

       $pass_error[] = 'Sorry, your nonce did not verify.';

    } else {
        
        if ( isset( $_POST['pass1'] ) && $current_user && wp_check_password( $_POST['pass1'], $current_user->user_pass, $current_user->ID) ) {
            
            /* Update user password. */
            if ( !empty( $_POST['pass2'] ) && !empty( $_POST['pass3'] ) ) {
                if ( $_POST['pass2'] == $_POST['pass3'] ) {
                    
                    wp_update_user( [
                        'ID' => $current_user->ID,
                        'user_pass' => esc_attr( $_POST['pass2'] )
                    ] );
                    
                } else {
                    $pass_error[] = __( 'Your passwords do not match so the new password has not been saved.', LWD_MZ_DOMAIN );
                }
            }
            
            
        } else {
            $pass_error[] = __( 'The entered password does not match the current one, so the new password has not been saved.', LWD_MZ_DOMAIN ); 
        }

    }
    
} ?>

<section class="section">
	<div class="content">
        
        <div class="title-perex">
	   		<h2 class="title-perex__title"><?php _e( 'Change your personal information', LWD_MZ_DOMAIN ); ?></h2>	
        </div>
        
        <form action="<?php echo LWD_MZ_HOME_URL; ?>#edit-profile" method="post" class="form1 client-zone-form" data-anchcor="edit-profile">
            
            <?php
        
                if ( isset( $_POST['_submit_edit'] ) ) {
                    echo '<div class="notice' . ( count( $meta_error ) == 0 ? ' notice--success' : '' ) . '">';

                    if ( count( $meta_error ) == 0 ) {
                        echo '<h3 class="notice__text">' . __( 'Your profile has been saved.', LWD_MZ_DOMAIN ) . '</h3>';
                    } else {
                        foreach ( $meta_error as $message ) {
                            echo '<h4 class="notice__text">' . $message . '</h4>';
                        }
                    }

                    echo '</div>';
                }
            
                wp_nonce_field( 'edit_user_meta_' . $current_user->ID, '_lwd_mz_user_wpnonce' );
            
            ?>
            
            <div class="client-zone-form__row client-zone-form__text">
               <label for="user_email" class="form1__label"><?php _e( 'Your e-mail', LWD_MZ_DOMAIN ); ?></label>
            </div>
            <div class="client-zone-form__row">
                <input type="email" name="user_email" id="user_email" value="<?php echo $email; ?>" class="form1__input" />
            </div>
            <div class="client-zone-form__row client-zone-form__text">
               <label for="first_name" class="form1__label"><?php _e( 'Your name', LWD_MZ_DOMAIN ); ?></label>
            </div>
            <div class="client-zone-form__row">
                <input type="text" name="first_name" id="first_name" value="<?php echo $first_name; ?>" class="form1__input" />
            </div>
            <div class="client-zone-form__row client-zone-form__text">
               <label for="last_name" class="form1__label"><?php _e( 'Your last name', LWD_MZ_DOMAIN ); ?></label>
            </div>
            <div class="client-zone-form__row">
                <input type="text" name="last_name" id="last_name" value="<?php echo $last_name; ?>" class="form1__input" />
            </div>
            <div class="client-zone-form__row">
                <input type="submit" name="_submit_edit" value="<?php _e( 'Save settings', LWD_MZ_DOMAIN ); ?>" class="form1__btn client-zone-form__submit" />
            </div>
        </form>
        
        <div class="title-perex">
	   		<h2 class="title-perex__title"><?php _e( 'Changing the password', LWD_MZ_DOMAIN ); ?></h2>	
        </div>
        
        <form action="<?php echo LWD_MZ_HOME_URL; ?>#edit-pass" method="post" class="form1 client-zone-form" data-anchor="edit-pass">
            
            <?php
        
                if ( isset( $_POST['_submit_pass'] ) ) {
                    echo '<div class="notice' . ( count( $pass_error ) == 0 ? ' notice--success' : '' ) . '">';

                    if ( count( $pass_error ) == 0 ) {
                        echo '<h3 class="notice__text">' . __( 'Your password has been changed.', LWD_MZ_DOMAIN ) . '</h3>';
                    } else {
                        foreach ( $pass_error as $message ) {
                            echo '<h4 class="notice__text">' . $message . '</h4>';
                        }
                    }

                    echo '</div>';
                }

                wp_nonce_field( 'edit_user_pass_' . $current_user->ID, '_lwd_mz_pass_wpnonce' );
            
            ?>
            
            <div class="client-zone-form__row client-zone-form__text">
                <label for="pass1" class="form1__label"><?php _e( 'Current password', LWD_MZ_DOMAIN ); ?></label>
            </div>
            <div class="client-zone-form__row">
                <input type="password" name="pass1" autocomplete="off" id="pass1" class="form1__input" />
            </div>
            <div class="client-zone-form__row client-zone-form__text">
                <label for="pass2" class="form1__label"><?php _e( 'New password', LWD_MZ_DOMAIN ); ?></label>
            </div>
            <div class="client-zone-form__row">
                <input type="password" name="pass2" autocomplete="off" id="pass2" class="form1__input" />
            </div>
            <div class="client-zone-form__row client-zone-form__text">
                <label for="pass3" class="form1__label"><?php _e( 'Re-type new password', LWD_MZ_DOMAIN ); ?></label>
            </div>
            <div class="client-zone-form__row">
                <input type="password" name="pass3" autocomplete="off" id="pass3" class="form1__input" />
            </div>
            <div class="client-zone-form__row">
                <input type="submit" name="_submit_pass" value="<?php _e( 'Change Password', LWD_MZ_DOMAIN ); ?>" class="form1__btn client-zone-form__submit" />
            </div>
        </form>

	</div>
</section>