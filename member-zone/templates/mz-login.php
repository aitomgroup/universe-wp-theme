<?php
/**
 * The template for display login screen of Member zone
 */

// Add subpage name to wp title
add_filter( 'wp_title', 'lwd_mz_filte_default_wp_title', 10, 3 );

function lwd_mz_filte_default_wp_title( $title, $sep, $seplocation ) {
    return __( 'Login', LWD_MZ_DOMAIN ) . ' ' . $title;
}

?>

<section class="section">
	<div class="content">
        <div class="title-perex">
            <h2 class="title-perex__title"><?php _e( 'Login', LWD_MZ_DOMAIN ); ?></h2>
			<p class="title-perex__perex"><?php _e( 'This page requires logging into the system.', LWD_MZ_DOMAIN ); ?></p>
		</div>
        
        <form action="<?php echo esc_url( wp_login_url() ); ?>" method="post" id="loginform" name="loginform" class="form1 client-zone-form">
            
            <?php
            
                if ( isset( $_GET['notice'] ) && in_array( $_GET['notice'], [ 'success_reset', 'success_register' ] ) ) {
                    echo '<div class="notice notice--success">';
                    
                    switch ( $_GET['notice'] ) {
                        case 'success_reset' :
                            echo '<h4 class="notice__text">' . __( 'Instructions to change your password have been sent to your e-mail address.', LWD_MZ_DOMAIN ) . '</h4>';
                            break;
                        case 'success_register' :
                            echo '<h4 class="notice__text">' . __( 'Registration complete. The login instructions were sent to the entered email address.', LWD_MZ_DOMAIN ) . '</h4>';
                            break;
                    }
                    
                    echo '</div>';
                }
            
            ?>
            
            <div class="client-zone-form__row">
                <label for="user_login" class="form1__label"><?php _e( 'Username or email', LWD_MZ_DOMAIN ); ?>:</label>
                <input type="text" name="log" id="user_login" class="form1__input" size="20" />
            </div>
            <div class="client-zone-form__row ">
                <label for="user_pass" class="form1__label"><?php _e( 'Password', LWD_MZ_DOMAIN ); ?>:</label>
                <input type="password" name="pwd" id="user_pass" class="form1__input" size="20" />
            </div>
            <div class="client-zone-form__row">
                <div class="form1__checkboxWrap checkbox-svg">
				    <label class="form1__label" for="rememberme">
                        <input class="form1__input" type="checkbox" name="rememberme" id="rememberme" />
                        <svg class="checkbox" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 200 200"><path class="checkbox__frame checkbox__path" d="M175 0h-150c-13.75 0-25 11.25-25 25v150c0 13.75 11.25 25 25 25h150c13.75 0 25-11.25 25-25v-150c0-13.75-11.25-25-25-25zM175 175h-150v-150h150v150z"></path><path class="checkbox__check checkbox__path" d="M87.5 155.178l-46.339-46.339 17.678-17.677 28.661 28.661 59.911-59.911 17.677 17.678-77.589 77.589z"></path></svg> <?php _e( 'Remember me', LWD_MZ_DOMAIN ); ?>
                    </label>
                </div>
            </div>
            <div class="client-zone-form__row">
                <input type="submit" name="wp-submit" id="wp-submit" value="<?php _e( 'Log in', LWD_MZ_DOMAIN ); ?>" class="form1__btn client-zone-form__submit" />
            </div>
            <div class="client-zone-form__row">
                <a href="<?php echo LWD_MZ_HOME_URL . 'register'; ?>"><?php _e( 'Registration', LWD_MZ_DOMAIN ); ?></a><br>	
                <a href="<?php echo LWD_MZ_HOME_URL . 'login?action=lostpassword'; ?>"><?php _e( 'Forgot your password?', LWD_MZ_DOMAIN ); ?></a>
            </div>
            <input type="hidden" name="redirect_to" value="<?php echo ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
		</form>

	</div>
</section>