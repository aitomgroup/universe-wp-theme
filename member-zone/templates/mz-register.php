<?php
/**
 * The template for display register screen of Member zone
 */

// Add subpage name to wp title
remove_filter( 'wp_title', 'lwd_mz_filte_default_wp_title' );

add_filter( 'wp_title', function( $title, $sep, $seplocation ) {
    return __( 'Registration', LWD_MZ_DOMAIN ) . ' ' . $title;
}, 10, 3 );

?>

<section class="section">
	<div class="content">
        <div class="title-perex">
            <h2 class="title-perex__title"><?php _e( 'Registration', LWD_MZ_DOMAIN ); ?></h2>
			<p class="title-perex__perex"><?php _e( 'If you want to access the client section, please register.', LWD_MZ_DOMAIN ); ?></p>
		</div>
        
        <form action="<?php echo wp_registration_url(); ?>" method="post" id="signupform" class="form1 client-zone-form" onsubmit="return mzRegisterForm()">
			<div class="client-zone-form__text client-zone-form__row">
				<label for="user_email" class="form1__label client-zone-form__label"><?php _e( 'Your e-mail', LWD_MZ_DOMAIN ); ?> <em class="form1__required">*</em></label>
			</div>
            <div class="client-zone-form__row">
				<input type="text" name="user_email" id="user_email" class="form1__input client-zone-form__input" />
			</div>
            <div class="client-zone-form__text client-zone-form__row">
				<label for="first_name" class="form1__label client-zone-form__label"><?php _e( 'Your name', LWD_MZ_DOMAIN ); ?> <em class="form1__required">*</em></label>
			</div>
            <div class="client-zone-form__row">
				<input type="text" name="first_name" id="first_name" class="form1__input client-zone-form__input" />
			</div>
            <div class="client-zone-form__text client-zone-form__row">
				<label for="last_name" class="form1__label client-zone-form__label"><?php _e( 'Your last name', LWD_MZ_DOMAIN ); ?> <em class="form1__required">*</em></label>
			</div>
            <div class="client-zone-form__row">
				<input type="text" name="last_name" id="last_name" class="form1__input client-zone-form__input" />
			</div>
            <div class="client-zone-form__row">
                <?php _e( 'Note: The registration confirmation will be emailed to your address, including instructions for creating your login password.', LWD_MZ_DOMAIN ); ?>
            </div>
            <div class="client-zone-form__row">
                <input type="submit" name="submit" value="<?php _e( 'Register', LWD_MZ_DOMAIN ); ?>" class="form1__btn client-zone-form__submit" />
			</div>
            <div class="client-zone-form__row">
                <p class="txt-center"><a href="<?php echo LWD_MZ_HOME_URL . 'login'; ?>"><?php _e( 'Back to login', LWD_MZ_DOMAIN ); ?></a></p>
            </div>
            <input type="hidden" name="user_login" id="user_login" value="" />
            <input type="hidden" name="redirect_to" value="<?php echo LWD_MZ_HOME_URL . 'login?notice=success_register'; ?>" />
		</form>
        
        <script>
            function bezdiak( value ) {
                var sdiak = "áäčďéěíĺľňóôőöŕšťúůűüýřžÁÄČĎÉĚÍĹĽŇÓÔŐÖŔŠŤÚŮŰÜÝŘŽ";
                var bdiak = "aacdeeillnoooorstuuuuyrzAACDEEILLNOOOORSTUUUUYRZ";

                new_value = "";

                for ( p = 0; p < value.length; p++ ) { 
                    if ( sdiak.indexOf( value.charAt( p ) ) !=-1 ) {
                        new_value += bdiak.charAt( sdiak.indexOf( value.charAt( p ) ) );
                    } else {
                        new_value += value.charAt( p );
                    }
                }
                
                return new_value;
            }
            
            function mzRegisterForm() {
                var firstName = document.getElementById('first_name').value,
                    lastName = document.getElementById('last_name').value;
                
                document.getElementById('user_login').value = ( firstName.length > 0 ? bezdiak( firstName.toLowerCase() ) + ( lastName.length > 0 ? '.' + bezdiak( lastName.toLowerCase() ) : '' ) : '' );
            }
        </script>

	</div>
</section>