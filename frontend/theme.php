<?php

/**
 * theme.php
 *
 * Front-end only functions
 * - Load front-end scripts
 * - WP Setup
 * - Theme functions
 * - Header menu walker
 * - Lazyload for attachments
 * - Redirect detail on custom link
 * - Add page template for elementor pages
 * - Add video support for gallery shortcode
 * - Styling comments
 * - WPCF7 customizing
 */

    // Load framework plugins
    require_once LWD_INCLUDES_DIR . '/plugins.php';

    /*----------------------------------------------------------------------------*
     * LOAD FRONT STYLES AND SCRIPTS
     *----------------------------------------------------------------------------*/

    add_action( 'wp_enqueue_scripts', 'lwd_enqueue_styles' );

    function lwd_enqueue_styles() {
        
        $theme_url = get_stylesheet_directory_uri();
        
        // Disable default WordPress fonts
        wp_deregister_style( 'open-sans' );
        wp_register_style( 'open-sans', false );
        
        wp_enqueue_style( LWD_SLUG, $theme_url . '/assets/css' . ( is_child_theme() ? '/crunch/' : '/' ) . 'style.css', false, LWD_VERSION );
        
        // Disable wordpress embed
        wp_deregister_script( 'wp-embed' );
        wp_register_script( 'wp-embed', false );

        wp_enqueue_script( LWD_SLUG, $theme_url . '/assets/js/min/universe.main.js', [ 'jquery' ], LWD_VERSION, true );
        
        wp_localize_script( LWD_SLUG, 'php', [
            'googleMapsApiKey' => get_theme_mod( 'lwd_general_gapi', '' )
        ] );

    }

    /*----------------------------------------------------------------------------*
     * WP SETUP
     *----------------------------------------------------------------------------*/

    // Using AITOM MU Plugin
    add_action( 'after_setup_theme', function() {
        add_theme_support( 'ai-clean-up' );
        add_theme_support( 'ai-disable-asset-versioning' );
        add_theme_support( 'ai-html-compression' );
        add_theme_support( 'ai-hide-wp' );
        add_theme_support( 'ai-nice-search' );
    } );

    add_filter( 'widget_archives_args', function( $args ) {
        $args['limit'] = 5;
        return $args;
    } );

    add_filter( 'the_content', 'lwd_lightbox_replace' );
    add_filter( 'wp_get_attachment_link', 'lwd_lightbox_replace' );

    function lwd_lightbox_replace( $content ) {
        $pattern = '/<a(.*?)href=(["\'])(.*?)\\2(.*?)><img/i';
        $content = preg_replace_callback( $pattern, function( $m ) {
            if ( isset( $m[3] ) && in_array( pathinfo( $m[3], PATHINFO_EXTENSION ), [ 'bmp', 'gif', 'jpeg', 'jpg', 'png' ] ) ) {
                global $post;
                $class_pos = $rel_pos = false;
                
                foreach ( $m as $index => $val ) {
                    if ( $index == 1 || $index == 5 ) {
                        if ( strpos( $m[$index], 'class=' ) !== false ) {
                            $class_pos = $index;
                        }

                        if ( strpos( $m[$index], 'data-rel=' ) !== false ) {
                            $rel_pos = $index;
                        }
                    }
                }

                if ( !$class_pos ) {
                    if( !$rel_pos ) {
                        $m[1] = ' class="js-lightbox"';
                        $class_pos = 1;
                    } else {
                        $m[$rel_pos] = str_replace( 'data-rel="', 'class="js-lightbox" data-rel="', $m[$rel_pos] );
                        $class_pos = $rel_pos;
                    }
                } elseif ( strpos( $m[$class_pos], 'js-lightbox' ) == false ) {
                    $m[$class_pos] = str_replace( 'class="', 'class="js-lightbox ', $m[$class_pos] );
                }

                if ( !$rel_pos && isset( $post->post_name ) ) {
                    $m[$class_pos] = str_replace( 'class="', 'data-rel="magnific-popup:' . $post->post_name . '" class="', $m[$class_pos] );
                }

                return '<a' . $m[1] . ' href="' . $m[3] . '"' . $m[4] . '><img';
            } else {
                return $m[0];
            }
        }, $content );
        return $content;
    }

    function lwd_add_class_gallery_link( $html ) {
        $pattern = '/<a(.*?)href=(["\'])(.*?)\\2(.*?)><img/i';
        $html = preg_replace_callback( $pattern, function( $m ) {
            if ( isset( $m[1] ) && isset( $m[3] ) && isset( $m[4] ) ) {
                $class_pos = false;

                foreach ( $m as $index => $val ) {
                    if ( $index == 1 || $index == 5 ) {
                        if ( strpos( $m[$index], 'class=' ) !== false ) {
                            $class_pos = $index;
                        }
                    }
                }

                if ( !$class_pos ) {
                    $m[1] = str_replace( '<a', '<a class="g-photos__item"', $m[$class_pos] );
                    $class_pos = 1;
                } elseif ( strpos( $m[$class_pos], 'g-photos__item' ) == false ) {
                    $m[$class_pos] = str_replace( 'class="', 'class="g-photos__item ', $m[$class_pos] );
                }

                return '<a' . $m[1] . ' href="' . $m[3] . '"' . $m[4] . '><img';
            } else {
                return $m[0];
            }
        }, $html );

        return $html;
    }

    /*----------------------------------------------------------------------------*
     * THEME FUNCTIONS
     *----------------------------------------------------------------------------*/

    /**
     * Get the post meta value with the given key or the default
     * if the value is empty

     * @param int $post_id
     * @param string $key
     * @param mixed $default
     * @return mixed
     */

    function lwd_get_post_meta( $post_id, $key, $default = '' ){
        $value = get_post_meta( $post_id, '_lwd_' . $key, true );

        if ($value === '' || $value === false) {
            return $default;
        }

        return $value;
    }

    //ANALYTICS CODE
    add_action( 'wp_head', function() {
       /* $code = ot_get_option('analytics') */
        if ( $code = get_theme_mod( 'lwd_tracking_code' ) ) {
            echo '<!-- Tracking code -->' . "\n";
            echo htmlspecialchars_decode( $code, ENT_QUOTES ) . "\n";
        }
    } );

    //GET LOGO
    function lwd_logo( $class = '', $id = '' ){
        
        $custom_logo = get_theme_mod( 'lwd_customizer_logo' );
        $theme_logo = LWD_URL . '/assets/images/logotype.svg';
        $theme_logo_retina = '';

        if ( $custom_logo != '' ) {
            $theme_logo = lwd_check_url( $custom_logo );
            if ( $custom_logo_retina = get_theme_mod( 'lwd_customizer_retina_logo' ) ) {
                $theme_logo_retina = $custom_logo_retina;
            }
        }

        if ( !is_ssl() ) {
            $theme_logo = str_replace( 'https://', 'http://', $theme_logo );
        } 
        
        $sitename = get_bloginfo('name');

        $logo = '<a href="' . home_url('/') . '"' . ( $class != '' ? ' class="' . $class . '"' : '' ) . ( $id != '' ? ' id="' . $id . '"' : '' ) . ' title="' . $sitename . '"><img src="' . $theme_logo . '"' . ( $class != '' ? ' class="' . $class . '__img"' : '' ) . ' srcset="' . $theme_logo . ' 1x' . ( $theme_logo_retina != '' ? ', ' . $theme_logo_retina . ' 2x' : '' ) . '" alt="' . $sitename . ' logo" /></a>';

        return $logo;
    }

    // GET YOUTUBE ID FROM URL
    function get_youtube_id_from_url( $url ) {
        if ( strpos( $url, 'http' ) !== false && strpos( $url, 'youtu') !== false ) {
            if ( stristr( $url, 'youtu.be/' ) ) {
                preg_match( '/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID );
                return $final_ID[4];
            } else {
                @preg_match( '/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD ); return $IDD[5];
            }
        } else {
            return false;
        }
    }

    //GET VIMEO ID FROM URL
    function get_vimeo_id_from_url( $url ){
        if ( strpos($url, 'http' ) !== false && strpos( $url, 'vimeo') !== false ) {
            return (int) substr( parse_url( $url, PHP_URL_PATH ), 1 );
        } else {
            return false;
        }
    }

    //GET FORMATED NUMBER
    function n_is_dec( $val ) {
        return is_numeric( $val ) && floor( $val ) != $val;
    }

    function get_nf( $number, $decimals = '' ){
        if( !is_numeric( $number ) ) 
            return $number;

        $number = str_replace( ',', '.', $number );

        if ( $decimals == '' ) {
            $decimals = n_is_dec( $number ) ? 1 : 0;
        }

        return number_format ( $number , $decimals, ',', ' ' );
    }

    // Async load
    add_filter( 'clean_url', function ($url ) {
        if ( strpos( $url, '#asyncload') === false )
            return $url;
        else if ( is_admin() )
            return str_replace( '#asyncload', '', $url );
        else
            return str_replace( '#asyncload', '', $url ). "' async='async"; 
    }, 11, 1 );

    /*----------------------------------------------------------------------------*
     * HEADER MENU WALKER
     *----------------------------------------------------------------------------*/

    class lwd_main_walker extends Walker_Nav_Menu {
        function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
            if ( ! $element ) return;
            
            $id_field = $this->db_fields['id'];
            
            if ( is_object( $args[0] ) ) {
                $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
                $args[0]->total_children = 0;
            
                if ( ! empty( $children_elements[$element->$id_field] ) ) {
                    $args[0]->total_children = count( $children_elements[$element->$id_field] );
                }
            }
            
            return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
        }

        function start_lvl( &$output, $depth = 0, $args = [] ) {
            $indent = str_repeat( "\t", $depth );
            $output .= "\n$indent<ul class=\"nav__list" . ( $depth + 1 )  . " level-" . $depth . "\">\n";
        }

        function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
            $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
            $class_names = '';

            if ( isset( $args->has_children ) && $args->has_children ) {
                $item->classes[] = 'nav__i' . $depth . '--submenu';
            }

            /*if ( is_category() ) {
                if ( $item->url == get_post_type_archive_link( 'post' ) ) $item->current = true;
            } else if ( is_tax( 'event_type' ) ) {
                if ( $item->url == get_post_type_archive_link( 'event' ) ) $item->current = true;
            } else if ( is_singular() ) {
                global $post;
                if ( $item->url == get_post_type_archive_link( $post->post_type ) ) $item->current = true;
            }*/

            $classes = empty( $item->classes ) ? [] : (array) $item->classes;
            $classes = preg_replace( '/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes );
            $classes = preg_replace( '/^((menu|page)[-_\w+]+)+/', '', $classes );
            $classes[] = 'nav__i' . $depth;

            $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
            $class_names = ' class="' . esc_attr( $class_names ) . '"';

            $output .= $indent . '<li' . $class_names . ( isset( $args->has_children ) && $args->has_children && $depth == 0 && $args->total_children ? '  data-cols="' . ( $args->total_children >= 6 ? 6 : $args->total_children ) . '"' : '' ) . '>';

            $attributes  = ! empty( $item->title ) ? ' title="'  . apply_filters( 'the_title', $item->title, $item->ID ) .'"' : '';
            $attributes .= ' class="nav__link' . $depth . ( $item->target == '_blank' ? ' nav__external' : '' ) . ( isset( $item->classes ) && ( ! empty( $item->current ) || ! empty( $item->current_item_ancestor ) || ! empty( $item->current_item_parent ) || in_array( 'current-page-ancestor', $item->classes ) || in_array( 'current-page-parent', $item->classes ) ) ? ' nav__i' . $depth . '--active' : '' ) . '"';
            $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
            $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
            $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';

            if ( isset( $args->before ) ) $item_output = $args->before;
            $item_output = '<a'. ( $attributes != '' ? $attributes : '' ) . '>';

            $item_output .= ( isset( $args->link_before ) ? $args->link_before : '' ) . apply_filters( 'the_title', $item->title, $item->ID );

            if ( isset( $args->link_after ) ) $item_output .= $args->link_after;
            $item_output .= '</a>';
            if ( isset( $args->after ) ) $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }
    }

    /*----------------------------------------------------------------------------*
 	 * LAZYLOAD ATTACHMENT IMAGES
 	 *----------------------------------------------------------------------------*/

    add_filter( 'wp_get_attachment_image_attributes', function( $attr, $attachment ) {
        if ( isset( $attr['class'] ) && strpos( $attr['class'], 'lazyload' ) !== false ) {
            if ( isset( $attr['src'] ) ) {
                $attachment_size = getimagesize( $attr['src'] );
                $attr['data-src'] = $attr['src'];
                $attr['src'] = generate_attachment_placeholder( $attachment_size[0], $attachment_size[1] );
                //$attr['src'] = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
            }
            
            if ( isset( $attr['srcset'] ) ) {
                $attr['data-srcset'] = $attr['srcset'];
                unset( $attr['srcset'] );
                if ( isset( $attr['sizes'] ) ) unset( $attr['sizes'] );
            }
        }
        
        // Manually disable srcset
        if ( isset( $attr['disable_srcset'] ) && $attr['disable_srcset'] ) {
            if ( isset( $attr['sizes'] ) ) unset( $attr['sizes'] );
            if ( isset( $attr['srcset'] ) ) unset( $attr['srcset'] );
            if ( isset( $attr['data-srcset'] ) ) unset( $attr['data-srcset'] );
            
            unset( $attr['disable_srcset'] );
        }
        
        return $attr;
    }, 10, 2 );

    /*----------------------------------------------------------------------------*
 	 * GENERATE DYNAMIC IMG PLACEHOLDER
 	 *----------------------------------------------------------------------------*/

    function generate_attachment_placeholder( $width = 1, $height = 1 ) {
        return 'data:image/gif;base64,' . base64_encode( file_get_contents( LWD_FRAMEWORK_URL . '/inc/generate-gif.php?x=' . $width . '&y=' . $height ) );
    }

    /*----------------------------------------------------------------------------*
 	 * REDIRECT DETAIL ON CUSTOM LINK
 	 *----------------------------------------------------------------------------*/

    add_action( 'wp', function() {
        if ( is_singular() ) {
            global $post;
            
            $custom_link = get_post_meta( $post->ID, 'custom-link', true );
            $post_meta = [];
            
             if ( is_singular('post') ) {
                if ( get_post_format() == 'link' ) {
                    $post_meta['custom-link'] = 'internal';
                    $post_meta['custom-link-internal'] = $custom_link;
                } else {
                    $custom_link = '';
                    $post_meta = [];
                }
            }
              
            if ( isset( $custom_link ) && $custom_link != '' ) {
                
                if ( $custom_link == 'none' ) {
                    global $wp_query;
                    $wp_query->set_404();
                    status_header( 404 );
                }
                
                $link = lwd_get_link( $post->ID, 'custom-link', $post_meta );
                    
                if ( isset( $link['url'] ) && $link['url'] != '' ) wp_redirect( $link['url'] ); exit;
            }
        }
    } );

    /*----------------------------------------------------------------------------*
 	 * ADD PAGE TEMPLATE FOR ELEMENTOR PAGES
 	 *----------------------------------------------------------------------------*/

    add_action( 'template_include', function( $template ) {
        
        if ( is_singular() ) {
            global $post;
            
            if ( isset( $post->ID ) && $post->ID == get_option( LWD_OPTIONS_NAME . '_contact' ) ) {
                $contact_page = locate_template( [ 'page-contact.php' ] );
                    
                if ( '' != $contact_page ) {
                    return $contact_page;
                }
            }
            
            $allowed_types = get_option( 'elementor_cpt_support' );
            
            if ( isset( $post->post_type ) && isset( $post->ID ) && is_array( $allowed_types ) && !empty( $allowed_types ) && in_array( $post->post_type, $allowed_types ) ) {
                if ( get_post_meta( $post->ID, '_elementor_edit_mode', true ) == 'builder' ) {
                    $elementor_page = locate_template( [ 'page-fullwidth.php' ] );
                    
                    if ( '' != $elementor_page ) {
                        return $elementor_page;
                    }
                }
            }
        }
        
        return $template;
        
    }, 10, 1 );

    /*----------------------------------------------------------------------------*
     * VIDEO SUPPORT FOR GALLERY SHORTCODE - ADD VIDEO URL FIELD TO ATTACHMENT META
     *----------------------------------------------------------------------------*/

    add_filter( 'wp_get_attachment_metadata', function( $data, $post_id ) {
        if ( $video_url = get_post_meta( $post_id, '_video_url', true ) ) $data['video_url'] = $video_url;
        return $data;
    }, 10, 2 );

    /*----------------------------------------------------------------------------*
 	 * STYLING COMMENTS
 	 *----------------------------------------------------------------------------*/

    function ai_comments_function( $comment, $args, $depth ) {
        
        global $post;
        $author_id = $post->post_author;
        
        $GLOBALS['comment'] = $comment;
        
        if ( !isset( $comment->comment_parent ) || !$comment->comment_parent ) $GLOBALS['comments_max_depth'] = isset( $args['has_children'] ) && $args['has_children'] ? count( $comment->to_array()['children'] ) + 1 : 1;

        if ( !$depth || $depth == 1 )  {
            if ( isset( $GLOBALS['comments_id'][1] ) && $GLOBALS['comments_id'][1] != $comment->comment_ID ) echo '</div>';
            echo '<div class="timeline__items"><span class="timeline__year">' . get_avatar( $comment, isset( $args['avatar_size'] ) ? $args['avatar_size'] : 90 ) . '</span>';
        } else {
            echo '<span class="timeline__point">' . get_avatar( $comment, isset( $args['avatar_size'] ) ? $args['avatar_size'] : 90 ) . '</span>';
        } ?>

        <div id="comment-<?php comment_ID(); ?>" style="top:-200px; position:relative"></div><?php /* Hack header height to move to anchor */ ?>
        <div <?php comment_class('timeline__i js_timeline'); ?> data-anchor="comment-anchor-<?php comment_ID(); ?>">
            
            <div class="timeline__i__content">
                
                <h3 class="timeline__title"><?php comment_author_link(); ?></h3>
                
                <div class="timeline__text">
                    
                    <p class="comment-date">
                        <?php printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
                            esc_url( get_comment_link( $comment->comment_ID ) ),
                            get_comment_time( 'c' ),
                            sprintf( _x( 'Comments sent %1$s @ %2$s', 'comments date', LWD_TEXT_DOMAIN ), get_comment_date(), get_comment_time() )
                        ); ?>
                    </p><!-- .comment-date -->	
                    
                    <?php if ( '0' == $comment->comment_approved ) : ?>
                        <p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is pending approval.', LWD_TEXT_DOMAIN ); ?></p>
                    <?php endif; ?>
                    
                    <?php comment_text(); ?>
                    
                   <?php comment_reply_link( array_merge( $args, [
                        'reply_text' => isset( $args['reply_text'] ) ? $args['reply_text'] : esc_html__( 'Reply', LWD_TEXT_DOMAIN ),
                        'depth'      => $depth,
                        'max_depth'	 => $args['max_depth']
                    ] ) ); ?><!-- .reply -->
                    
                </div>
                
            </div>
            
        </div>

        <?php
        
        if ( isset( $GLOBALS['comments_max_depth'] ) && $depth == $GLOBALS['comments_max_depth'] ) echo '</div>';
        
    }

    function ai_comments_end_function() {
        echo '';
    }

    add_filter( 'get_comment_link', function( $link, $comment ) {
        return ( strpos( $link, 'comment-anchor-') == false && strpos( $link, 'comment-') !== false ? str_replace( 'comment-', 'comment-anchor-', $link ) : $link );
    }, 10, 2 );

    add_filter( 'comment_reply_link', function( $link, $args, $comment ) {
        $link = strpos( $link, '#respond-achor') == false && strpos( $link, '#respond') !== false ? str_replace( '#respond', '#respond-achor', $link ) : $link;
        
        return ( strpos( $link, 'g-btn') == false && strpos( $link, 'class=\'comment-reply-link') !== false ? str_replace( 'class=\'comment-reply-link', 'class=\'g-btn g-btn--border g-btn--text-dark comment-reply-link', $link ) : $link );
    }, 10, 3 );

    add_filter( 'cancel_comment_reply_link', function( $link ) {
        return ( strpos( $link, '#respond-achor') == false && strpos( $link, '#respond') !== false ? str_replace( '#respond', '#respond-achor', $link ) : $link );
    }, 10, 1 );

    add_filter( 'comment_form_fields', function( $fields ) {
        
        if ( isset( $fields['comment'] ) ) {
            $comment_field = $fields['comment'];
            unset( $fields['comment'] );
            $fields['comment'] = $comment_field;
        }
       
        return $fields;
    }, 10, 1 );

    /*----------------------------------------------------------------------------*
 	 * LOAD WPCF7 SCRIPTS AND STYLES ON WHEN NEED IT
 	 *----------------------------------------------------------------------------*/

    if ( function_exists( 'wpcf7_enqueue_scripts' ) && function_exists( 'wpcf7_enqueue_styles' ) ) {
        add_filter( 'wpcf7_load_js', '__return_false' );
        add_filter( 'wpcf7_load_css', '__return_false' );
        
        add_action( 'wp_enqueue_scripts', function() {
            global $post;
            
            if ( !empty( $post ) && is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'contact-form-7' ) ) {
                //wpcf7_enqueue_styles();
                wpcf7_enqueue_scripts();
            }
        } );
    }

    /*----------------------------------------------------------------------------*
 	 * WPCF7 ALLOW PIPES
 	 *----------------------------------------------------------------------------*/

    add_filter( 'wpcf7_form_tag', function( $scanned_tag ) {
        $pipes = new WPCF7_Pipes( $scanned_tag['raw_values'] );
        
        $scanned_tag['values'] = $pipes->collect_afters();
        
        return $scanned_tag;
    }, 10, 1 );
    
    /*----------------------------------------------------------------------------*
 	 * FILTER WPCF7 FORM ELEMENTS - DO SHORTCODES
 	 *----------------------------------------------------------------------------*/

    // Keep function separately to disable or modify this filter in child theme
    add_filter( 'wpcf7_form_elements', 'lwd_filter_wpcf7_form_elements' );

    function lwd_filter_wpcf7_form_elements( $return ) {
        
        $ico_check = locate_template( 'assets/images/icons/checkbox.svg' );
        $ico_check = isset( $ico_check ) && $ico_check != '' ? file_get_contents( $ico_check ) : '';
        $ico_radio = locate_template( 'assets/images/icons/radio.svg' );
        $ico_radio = isset( $ico_radio ) && $ico_radio != '' ? file_get_contents( $ico_radio ) : '';
        
        // Style required
        $return = str_replace( '*', '<em class="form1__required">*</em>', $return );
        
        // Wrap Acceptance Checkbox
        $return = preg_replace( '/<p><span class="wpcf7-form-control-wrap (acceptance-(.*)|accept-this)">(.*)<\/span> (.*)<\/p>/iU', '<div class="form1__item pure-u-1 pure-u-v700-12-24 wpcf7-form-control-wrap accept-this"><div class="form1__checkboxWrap checkbox-svg wpcf7-form-control"><label class="wpcf7-list-item first">$3' . $ico_check . '&nbsp;<span class="wpcf7-list-item-label">$4</span></label></div></div>', $return );
        
        // Style submit
        $return = preg_replace( '/<p><input type="submit" value="(.*)"(.*)><\/p>/iU', '<div class="form1__item pure-u-1 wpcf7-form-submit-wrap"><button type="submit" name="send" class="form1__btn wpcf7-form-submit">$1</button><div class="ajax-loader wpcf7-ajax-loader spinner"><div class="spinner__i spinner__i--1"></div><div class="spinner__i spinner__i--2"></div><div class="spinner__i spinner__i--3"></div></div></div>', $return );
        
        $return = preg_replace_callback( '~<p><label>(.*?)</label></p>~is', function( $return ) use ( $ico_check, $ico_radio ) {
            if ( !isset( $return[1] ) ) return '';
            
            $return = $return[1];
            
            // Set field width
            $fullwidth = $text = $name = $new_row = $field_class = false;
            
            // Get field text
            $text = trim( substr( $return, 0, strpos( $return, '<br' ) ) );
            
            // Get field name
            preg_match( '/name="(.*)"/iU', $return, $match );
            if ( isset( $match[1] ) && $match[1] != '' && $match[1] != 'false' ) $name = $match[1];
            
            // Get field object
            $field =  strstr( $return, '<span class="wpcf7-form-control-wrap' );
            
            // Style checkbox / radio 
            if ( strpos( $field, 'wpcf7-checkbox') !== false || strpos( $field, 'wpcf7-radio') !== false ) {
                $check_type = strpos( $field, 'wpcf7-radio') !== false ? 'radio' : 'checkbox';
                
                $field = str_replace( 'class="wpcf7-form-control', 'class="form1__' . $check_type . 'Wrap ' . $check_type . '-svg wpcf7-form-control wpcf7-list', $field );
                
                $field = preg_replace( '/(<input[^>]+>(?:<\/input>)?)/i', '$1' . ( $check_type == 'radio' ? $ico_radio : $ico_check ) , $field );
                
                $field = preg_replace( '/<span class="wpcf7-list-item-label">(.*)<\/span>/iU', '<u class="wpcf7-list-item-label">$1</u>', $field );
                
                if (  strpos( $field, '</label>') === false ) {
                    $field = preg_replace_callback( '~<span class="wpcf7-list-item(.*?)">(.*?)</span>~is', function( $return ) {
                         if ( !isset( $return[2] ) ) return '';

                        return '<span class="wpcf7-list-item' . ( isset( $return[1] ) ? $return[1] : '' ) . '"><label>' . $return[2] . '</label></span>';
                    }, $field );
                }
            }
            
            // Add css class
            if ( strpos( $field, 'wpcf7-select') !== false ) {
                $field_class = 'select';
            } else if ( strpos( $field, 'wpcf7-textarea') !== false ) {
                $field_class = 'area';
                $fullwidth = true;
            } else if ( strpos( $field, 'wpcf7-checkbox') !== false || strpos( $field, 'wpcf7-radio') !== false ) {
                
            } else {
                $field_class = 'input';
            }
            
            if ( strpos( $field, 'wpcf7-fullwidth') !== false ) $fullwidth = true;
            if ( $field_class != '' ) $field = str_replace( 'class="', 'class="form1__' . $field_class . ' ', $field );
            
            if ( strpos( $field, 'wpcf7-new-row') !== false ) $new_row = '</div><div class="pure-g">';
            
            // Print box template
            $return = $new_row . '<div class="form1__item pure-u-1' . ( !$fullwidth ? ' pure-u-v700-12-24' : '' ) . '"><label for="' . $name . '" class="form1__label">' . $text  . '</label>' . $field . '</div>';
            
            return $return;
        }, $return );
        
        // Style default P tag
        $return = preg_replace( '~<p>(.*?)</p>~is', '<div class="form1__item pure-u-1"><span class="form1__message">$1</span></div>', $return );

        return '<div class="wpcf7-form-inner form1 js_loadFade pure-form form1--small"><div class="pure-g">' . do_shortcode( shortcode_empty_paragraph_fix( $return ) ) . '</div></div>';
    }