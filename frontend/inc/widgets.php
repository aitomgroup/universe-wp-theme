<?php

    // Prevent direct script access
    if ( !defined( 'ABSPATH' ) ) exit;

    /*----------------------------------------------------------------------------*
     * SET UP SIDEBARS
     *----------------------------------------------------------------------------*/

    if ( function_exists( 'register_sidebar' ) ) {

        register_sidebar( [
            'name' => __( 'Footer', LWD_TEXT_DOMAIN),
            'id' => 'footer',
            'before_widget' => '<div class="widget pure-u-1 g-footer__i js_loadFadeDelayed %2$s"><div class="content wysiwyg js_loadFade">',
            'after_widget' => '</div></div>'."\n",
            'before_title' => '<h3>',
            'after_title' => '</h3>'."\n",
        ] );

    }

    /*----------------------------------------------------------------------------*
     * ALLOW SHORTCODES IN WIDGETS
     *----------------------------------------------------------------------------*/

    add_filter( 'widget_text', 'do_shortcode' );

    /*----------------------------------------------------------------------------*
     * ADD CLASSES FOR FOOTER WIDGETS
     *----------------------------------------------------------------------------*/

    add_filter( 'dynamic_sidebar_params', function( $params ) {

        global $widget_i, $post; // Global a counter array
        
        $this_id = $params[0]['id']; // Get the id for the current sidebar we're processing

        $arr_registered_widgets = wp_get_sidebars_widgets(); // Get an array of ALL registered widgets	

        // If the counter array doesn't exist, create it
        if ( !$widget_i ) $widget_i = [];

        // Check if the current sidebar has no widgets
        if ( !isset( $arr_registered_widgets[ $this_id ] ) || !is_array( $arr_registered_widgets[ $this_id ] ) ) return $params;

        if ( isset( $widget_i[ $this_id] ) ) {
            // See if the counter array has an entry for this sidebar
            $widget_i[ $this_id ]++;
        } else { 
            // If not, create it starting with 1
            $widget_i[ $this_id ] = 1;
        }

        $class = 'class="widget widget-' . $widget_i[$this_id];
        // Add a widget number class for additional styling options
        
        if ( $this_id == 'footer' ) {
            if ( count( $arr_registered_widgets[ $this_id ] ) >= 4 )
                $cols = 4;
            else
                $cols = count( $arr_registered_widgets[ $this_id ] );
            
            $class .= ' pure-u-v1000-' . ( 24 / $cols ) . '-24' . ( $cols % 2 == 0 ? ' pure-u-v600-12-24' : '' );
        }

        // Insert our new classes into "before widget"
        $params[0]['before_widget'] = str_replace( 'class="widget', $class, $params[0]['before_widget'] ); 

        return $params;

    } );

    /*----------------------------------------------------------------------------*
     * UNREGISTER DEFAULT WP WIDGETS
     *----------------------------------------------------------------------------*/

    add_action( 'widgets_init', function() {

        unregister_widget( 'WP_Widget_Links' );
        unregister_widget( 'WP_Widget_Meta' );
        unregister_widget( 'WP_Widget_RSS' );

    }, 99 );
