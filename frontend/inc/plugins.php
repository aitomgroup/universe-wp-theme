<?php

    // Prevent direct script access
    if ( !defined( 'ABSPATH' ) ) exit; 


    /*
    Plugin Name: Shortcode empty Paragraph fix
    Plugin URI: http://www.johannheyne.de/wordpress/shortcode-empty-paragraph-fix/
    Description: Fix issues when shortcodes are embedded in a block of content that is filtered by wpautop.
    Author URI: http://www.johannheyne.de
    Version: 0.1
    */

    add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );

    function shortcode_empty_paragraph_fix( $content ){   
        $array = [
            '<p>[' => '[', 
            ']</p>' => ']', 
            ']<br />' => ']',
            '<p><script' => '<script',
            'script></p>' => 'script>'
        ];
        
        if ( class_exists( 'PostLinkShortcodes' ) ) {
            $array['http://['] = '[';
            $array['https://['] = '[';
        }

        $content = strtr( $content, $array );

        return $content;
    }


    /*
    Plugin Name: Image P tag remover
    Description: Plugin to remove p tags from around images in content outputting, after WP autop filter has added them. (oh the irony)
    Version: 1.0
    Author: Fublo Ltd
    Author URI: http://fublo.net/
    */

    // we want it to be run after the autop stuff... 10 is default.
    add_filter( 'the_content', 'filter_ptags_on_images' );

    function filter_ptags_on_images( $content ) {
        // do a regular expression replace...
        // find all p tags that have just
        // <p>maybe some white space<img all stuff up to /> then maybe whitespace </p>
        // replace it with just the image tag...
        return preg_replace( '/<p>(\s*)(<img .* \/>)(\s*)<\/p>/iU', '\2', $content );
    }


    /*
    Plugin Name: Search WordPress by Custom Fields plugin
    Plugin URI: http://adambalee.com/search-wordpress-by-custom-fields-without-a-plugin/
    Description: Unfortunately, it’s not possible to search WordPress by custom fields out of the box. In order to fix that, we need to modify the WordPress search query to include custom fields.
    Author URI: http://adambalee.com
    Version: 1.0
    */

    /**
     * Join posts and postmeta tables
     *
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
     */

    add_filter( 'posts_join', 'cf_search_join' );

    function cf_search_join( $join ) {
        global $wpdb;

        if ( is_search() ) {    
            $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
        }

        return $join;
    }

    /**
     * Modify the search query with posts_where
     *
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
     */

    add_filter( 'posts_where', 'cf_search_where' );

    function cf_search_where( $where ) {
        global $pagenow, $wpdb;

        if ( is_search() ) {
            $where = preg_replace(
                "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );

            // Exclude hidden custom fields    
            //$where .= " AND ((".$wpdb->postmeta.".meta_key != 'featured-image') AND (".$wpdb->postmeta.".meta_key != 'gallery') AND (".$wpdb->postmeta.".meta_key != 'releated') AND (".$wpdb->postmeta.".meta_key != 'run-out-model') AND (".$wpdb->postmeta.".meta_key != '_edit_lock') AND (".$wpdb->postmeta.".meta_key != '_edit_last') AND (".$wpdb->postmeta.".meta_key != '_wp_old_slug'))";
        }

        return $where;
    }


    /**
     * Prevent duplicates
     *
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
     */

    add_filter( 'posts_distinct', 'cf_search_distinct' );

    function cf_search_distinct( $where ) {
        global $wpdb;

        if ( is_search() ) {
            return "DISTINCT";
        }

        return $where;
    }

    /*
    Plugin Name: Pagination plugin
    Plugin URI: http://www.kriesi.at/archives/how-to-build-a-wordpress-post-pagination-without-plugin
    Description: Simple and flexible pagination plugin which provides users with better navigation on your WordPress site.
    Author URI: http://www.kriesi.at
    Version: 1.0
    */

    if ( !function_exists( 'ai_paginator' ) ) {
        function ai_paginator( $pages = '', $range = 2 ) {
             $showitems = ( $range * 2 ) + 1;

             global $paged;
             if ( empty( $paged ) ) $paged = 1;

             if ( $pages == '' ) {
                 global $wp_query;
                 $pages = $wp_query->max_num_pages;
                 
                 if ( !$pages ) {
                     $pages = 1;
                 }
             }

             if ( 1 != $pages ) {
                 
                 echo '<!-- [ PAGENAVI ] -->' . "\n";
                 echo '<div class="paginator js_loadFade">';
                 
                 if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) echo '<a href="' . get_pagenum_link( 1 ) . '" class="paginator__i paginator__i--first" rel="first">&laquo;</a> ';
                 
                 if ( $paged > 1) echo '<a href="' . get_pagenum_link( $paged - 1 ) . '" class="paginator__i paginator__i--prev" rel="prev">&lsaquo;</a> ';

                 for ( $i=1; $i <= $pages; $i++ ) {
                     if ( 1 != $pages && ( !( $i >= $paged + $range + 1 || $i <= $paged-$range - 1 ) || $pages <= $showitems ) ) {
                         echo ( $paged == $i ) ? '<span class="paginator__i paginator__i--active">' . $i . '</span> ' : '<a class="paginator__i" href="' . get_pagenum_link( $i ) . '">' . $i . '</a> ';
                     }
                 }

                 if ( $paged < $pages ) echo '<a href="' . get_pagenum_link( $paged + 1 ) . '" class="paginator__i paginator__i--next" rel="next">&rsaquo;</a> ';
                 
                 if ( $paged < $pages - 1 &&  $paged + $range - 1 < $pages && $showitems < $pages ) echo '<a href="' . get_pagenum_link( $pages ) . '" class="paginator__i paginator__i--last" rel="last">&raquo;</a>';
                 
                 echo '</div>' . "\n";
                 echo '<!-- [ / PAGENAVI ] -->' . "\n";
             }
        }
    }