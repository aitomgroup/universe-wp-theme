<?php

    $imgWidth = intval( $_GET['x'] );
    $imgWidth = $imgWidth > 0 ? $imgWidth : 1;

    $imgHeight = intval( $_GET['y'] );
    $imgHeight = $imgHeight > 0 ? $imgHeight : 1;

    $im = imagecreatetruecolor( $imgWidth, $imgHeight );
    imagealphablending( $im, false );
    imagesavealpha( $im, true );
    $white = imagecolorallocatealpha( $im, 255, 255, 255, 127 ); // fully transparent white.

    imagefill( $im, 0, 0, $white );

    header( 'Content-Type: image/gif' );
    imagegif( $im );
    imagedestroy( $im );

?>