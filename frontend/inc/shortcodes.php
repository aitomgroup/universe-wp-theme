<?php

    // Prevent direct script access
    if ( !defined( 'ABSPATH' ) ) exit;

    /*----------------------------------------------------------------------------*
     * CLEAR CONTENT FROM TAGS AND DO SHORTCODE
     *----------------------------------------------------------------------------*/

    function content_helper( $content, $removep ) {
        $content = str_replace( "\r\n", "", $content );
        $content = preg_replace( '#^<\/p>|<p>$#', "", do_shortcode( $content ) );
        if ( $removep == "1" ) {
            $content = strip_tags( $content, "<a><i><em><strong><span><cite><img><li><ol><ul><h1><h2><h3><h4><h5><h6><table>" );
        }
        return $content;
    }

    /*----------------------------------------------------------------------------*
     * SHORTCODE: GAP
     *----------------------------------------------------------------------------*/

    add_shortcode( 'gap', 'gap_func' );

    function gap_func( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'vyska' => '20',
        ), $atts ) );

        return '<div class="gap-' . $vyska . '"></div>';
    }

    /*----------------------------------------------------------------------------*
     * SHORTCODE: CLEAR FLOATED ELEMENTS
     *----------------------------------------------------------------------------*/

    add_shortcode( 'clear', 'clear_func' );

    function clear_func() {
        return '<div class="clear"></div>';
    }

    /*----------------------------------------------------------------------------*
     * SHORTCODE: BUTTON
     *----------------------------------------------------------------------------*/

    add_shortcode( 'tlacitko', 'button_func' );
    add_shortcode( 'button', 'button_func' );

    function button_func( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'color' => 'btn-default',
            'size' => '',
            'text' => '',
            'link' => '',
            'target' => '_self',
            'el_class' => ''
        ), $atts ) );

        $class = array();
        $class[] = $color;
        $class[] = ( $size != '' ? $size : '' );
        $class[] = ( $el_class != '' ? $el_class : '' );

        $return = '
            <a href="' . $link . '" class="btn' . ( $class != '' ? ' ' . implode( ' ', $class ) : '' ) . '" target="' . $target . '">' . $text . '</a>
        ';
        return $return;
    }

    /*----------------------------------------------------------------------------*
     * SHORTCODE: DATE
     *----------------------------------------------------------------------------*/

    add_shortcode( 'datum', 'date_func' );

    function date_func( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'format' => 'd.M Y',
        ), $atts ) );
        return date( $format, current_time( 'timestamp', 1 ) );
    }

    /*----------------------------------------------------------------------------*
     * SHORTCODE: WP MENU
     *----------------------------------------------------------------------------*/

    add_shortcode('wp-menu', 'wp_menu_func');

    function wp_menu_func( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'jmeno' => '',
            'trida' => ''
        ), $atts ) );
        return wp_nav_menu( array('menu' => $jmeno, 'container' => false, 'echo' => false, 'menu_class' => $trida ) );
    }

    /*----------------------------------------------------------------------------*
     * SHORTCODE: GET URL BY NAME
     *----------------------------------------------------------------------------*/

    add_shortcode( 'ziskat-adresu', 'get_url_func' );

    function get_url_func( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'jmeno' => '',
        ), $atts ) );
        if ( $jmeno != '' ) {
            return get_permalink( lwd_get_page_by_slug($jmeno)->ID );
        } else {
            return home_url('/');
        }
    }

    /*----------------------------------------------------------------------------*
     * SHORTCODE: BLOCKQUOTE
     *----------------------------------------------------------------------------*/

    add_shortcode( 'citace', 'quote_func' );
    add_shortcode( 'blockquote', 'quote_func' );

    function quote_func( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'autor' => '',
            'el_class' => ''
        ), $atts ) );

        $content = str_replace( array( "\n", "\r" ), "</p><p>", $content );

        $return .= '
        <blockquote>
            <div class="color">
                <p>&bdquo;' . $content . '&rdquo;</p>
            </div>
            ' . ( $autor != '' ? '<footer>' . $autor . '</footer>' : '') . '
        </blockquote>';
        return $return;
    }

    /*----------------------------------------------------------------------------*
     * SHORTCODE: RESPONZIVE EMBED
     *----------------------------------------------------------------------------*/

    add_shortcode( 'responzivni-embed', 'responzive_embed_func' );

    function responzive_embed_func( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'format' => '4by3',
        ), $atts ) );
        return '<div class="embed-responsive embed-responsive-' . $format . ' m-bottom-20">' . $content . '</div>';
    }
