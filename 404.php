<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

get_header(); ?>

    <!-- [ ERROR 404 ] -->
    <section class="page-header">
        <div class="page-header__bg lazyload"></div>
        <h1 class="page-header__title js_loadFade"><?php _ex( '404', '404 page title', LWD_TEXT_DOMAIN ); ?></h1>
        <div class="page-header__perex js_loadFade"><?php _e( 'Page not found, select new one in main menu please', LWD_TEXT_DOMAIN ); ?></div>
    </section>
    <!-- [ / ERROR 404 ] -->

<?php get_footer(); ?>