/**
 * Modify link url without http protocol for dynamic shortcode link
 */

( function( $ ) {
    
    if ( typeof wpLink != 'undefined' ) {
        wpLink.getAttrs = function() {
            wpLink.correctURL();

            var href= $( '#wp-link-url' ).val();

            if ( href.indexOf("http://[") == 0 || href.indexOf("https://[") == 0 ) {
                href = href.replace( /^https?\:\/\//i, '' );
            }

            return {
                href:       $.trim( href ),
                target:     $( '#wp-link-target' ).prop( 'checked' ) ? '_blank' : ''
            };
        }
    }
    
} )( jQuery );
    

