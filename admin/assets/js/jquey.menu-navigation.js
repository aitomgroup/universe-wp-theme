/**
 * Modify move navigation of menu item - up & down
 */

( function( $ ) {
    
    $('<style>.menu-item .item-order .menus-move { color: #0073aa } .menu-item .item-order .menus-move:hover, .menu-item .item-order .menus-move:focus { color: #00a0d2 } .nav-menus-php .item-edit:before { margin-bottom: 10px } .nav-menus-php .item-edit.no-focus:before, .nav-menus-php .item-edit.no-focus:focus:before { -webkit-box-shadow: none; -moz-box-shadow: none; box-shadow: none }</style>').appendTo('head');
    
    $('.menu-item').each(function() {
        
        var $up = $(this).find('.menus-move.menus-move-up').clone(),
            $down = $(this).find('.menus-move.menus-move-down').clone();
        
        $up.html('&#8593;');
        $down.html('&#8595;');
        
        $(this).find('.item-order').empty().append( $up, ' | ', $down ).show();
        
        $( [ $up, $down ] ).each(function() {
            $('.menu-item .item-edit').addClass('no-focus');
            
            $(this).on('click', function(){
                setTimeout(function(){
                    $('.menu-item .item-edit').removeClass('no-focus');
                    $('.menu-item .item-edit').blur();
                }, 300);
            });
        });
    });
    
} )( jQuery );
    

