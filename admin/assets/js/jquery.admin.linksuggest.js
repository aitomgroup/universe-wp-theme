window.LinkSuggest = window.LinkSuggest || {};

(function( window, $, _, Backbone, wp, undefined ) {
	'use strict';

	var l10n,
		LinkSuggest = window.LinkSuggest,
		finder = { view: {} };

	// Link any localized strings.
	l10n = LinkSuggest.l10n = LinkSuggest.l10n || {};

	finder.view.Page = wp.Backbone.View.extend({
		events: {
			'click .jq-suggest-link' : 'openModal'
		},

		initialize: function( options ) {
			this.modal = options.modal;
			this.render();
		},

		render: function() {
			this.$el.append( this.modal.$el );
			return this;
		},

		openModal: function( e ) {
			var $target = $( e.target).prev( '.jq-suggest-field' );
			e.preventDefault();
			this.modal.open( $target );
		}
	});

	finder.view.Modal = wp.Backbone.View.extend({
		tagName: 'div',
		className: 'find-box',
		template: wp.template( 'link-suggest-modal' ),

		events : {
			'click .link-suggest-modal-search-button': 'findPosts',
			'keypress .link-suggest-modal-search-field': 'routeKey',
			'keyup .link-suggest-modal-search-field': 'routeKey',
			'click tr': 'select',
			'click .js-close': 'close'
		},

		initialize: function( options ) {
			this.$pageWrapper = options.pageWrapper;
			this.postTypes = options.postTypes;
			this.$overlay = false;
			this.$target = $();
			this.render();
		},

		render: function() {
			this.$el.hide().html( this.template() );
			this.$field = this.$el.find( '.link-suggest-modal-search-field' );
			this.$overlay = $( '.ui-find-overlay' );
			this.$response = this.$el.find( '.link-suggest-modal-response' );
			this.$spinner = this.$el.find( '.link-suggest-modal-search .spinner' );
			return this;
		},

		close: function() {
			this.$overlay.hide();
			this.$el.hide();
			this.$target = $();
		},

		findPosts: function() {
			var self = this;
			self.$spinner.show();

			wp.ajax.post( 'link_suggest_find_posts', {
				s: self.$field.val(),
				post_types: this.postTypes,
				nonce: $( '#ls-find-posts-ajax-nonce' ).val()
			}).always(function() {
				self.$spinner.hide();
			}).done(function( response ) {
				self.$response.html( response );
			}).fail(function() {
				self.$response.text( l10n.responseError );
			});
		},

		open: function( $target ) {
			this.$target = $target;
			this.$response.html( '' );
			this.$el.show();
			this.$field.focus();

			if ( ! this.$overlay.length ) {
				this.$pageWrapper.append( '<div class="ui-find-overlay"></div>' );
				this.$overlay  = $( '.ui-find-overlay' );
			}

			this.$overlay.show();
			this.findPosts();
		},

		routeKey: function( e ) {
			if ( e.which && 13 === e.which ) {
				this.findPosts();
			} else if ( e.which && 27 === e.which ) {
				this.close();
			}
		},

		select: function( e ) {
			var value = $( e.target ).closest( 'tr' ).find( 'input' ).val();
			if ( value != undefined ) {
                this.$target.val( value );
                this.close();
            }
		}
	});

	// Document ready.
	$(document).ready(function() {
        
		var modal,
			$body = $( 'body' ),
			$page = $( '.wp-full-overlay' );

		if ( $page.length < 1 ) {
			$page = $body;
		}

		modal = new finder.view.Modal({
			postTypes: ['any'],
			pageWrapper: $page
		});

		new finder.view.Page({
			el: $page.get( 0 ),
			modal: modal
		});
	});

})( this, jQuery, _, Backbone, wp );
