<?php

/**
 * theme-admin.php
 *
 * - WP Setup
 * - Customize post type tables columns
 * - Metaboxes
 * - Back-end common functions
 * - Ajax handler for finding posts
 * - Override OptionTree 'text' function
 */

    /*----------------------------------------------------------------------------*
     * LOAD ADMIN STYLES AND SCRIPTS
     *----------------------------------------------------------------------------*/

    add_action( 'admin_head', function() {
        global $post_type, $pagenow;

        // Custom admin style
        wp_enqueue_style( LWD_SLUG . '-admin', LWD_ADMIN_URL . '/assets/css/theme-admin-style.css', false, LWD_VERSION, 'all' );

        // Load script to link suggests
        wp_register_script( LWD_SLUG . '-admin-link-suggest', LWD_ADMIN_URL . '/assets/js/jquery.admin.linksuggest.js', array( 'media-upload', 'media-views', 'wp-backbone', 'wp-util' ) );

        wp_localize_script(
            LWD_SLUG . '-admin-link-suggest',
            'LinkSuggest',
            array(
                'l10n' => array(
                    'responseError'   => __( 'An error has occurred. Please refresh the page and try again.', LWD_TEXT_DOMAIN ),
                )
            )
        );

        if ( $pagenow == 'post.php' ) add_action( 'admin_footer', function() { ?>
            <script type="text/html" id="tmpl-link-suggest-modal">
                <div class="link-suggest-modal-head find-box-head">
                    <?php _e( 'Find a link', LWD_TEXT_DOMAIN ); ?>
                    <div class="link-suggest-modal-close js-close"></div>
                </div>
                <div class="link-suggest-modal-inside find-box-inside">
                    <div class="link-suggest-modal-search find-box-search">
                        <?php wp_nonce_field( 'ls-find-posts', 'ls-find-posts-ajax-nonce', false ); ?>
                        <input type="text" name="s" value="" class="link-suggest-modal-search-field">
                        <span class="spinner"></span>
                        <input type="button" value="<?php esc_attr_e( 'Search', LWD_TEXT_DOMAIN ); ?>" class="button link-suggest-modal-search-button" />
                        <div class="clear"></div>
                    </div>
                    <div class="link-suggest-modal-response"></div>
                </div>
            </script><?php
        } );
        
        if ( class_exists( 'PostLinkShortcodes' ) ) {

            // Modify link url without http protocol for dynamic shortcode link       
            wp_enqueue_script( LWD_SLUG . '-admin-fix-wplink', LWD_ADMIN_URL . '/assets/js/jquey.fixwplink.js', array( 'jquery' ), LWD_VERSION );
            
        }
        
        // Modify move navigation of menu item - up & down
        if ( $pagenow == 'nav-menus.php' ) {
            wp_enqueue_script( LWD_SLUG . '-admin-menu-navigation', LWD_ADMIN_URL . '/assets/js/jquey.menu-navigation.js', array( 'jquery' ), LWD_VERSION );
        }
        
    } );

    /*----------------------------------------------------------------------------*
     * WP SETUP
     *----------------------------------------------------------------------------*/

    // Disable wp update notification
    if ( !current_user_can('administrator') ) {
        add_action( 'admin_menu', function() {
            remove_action( 'admin_notices', 'update_nag', 3 );
        } );
    }

    // Add function on init
    add_action( 'init', function() {
        // Add excerpt field for page
         add_post_type_support( 'page', 'excerpt' );
    } );

    // Allow upload SVG files
    add_filter( 'upload_mimes', function( $mimes ) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    } );

    /*----------------------------------------------------------------------------*
     * ADD GLOBAL CONSTANTS
     *----------------------------------------------------------------------------*/

    global $wp_options;

    $wp_options = array(
        'page_on_front' => get_option( 'page_on_front' ),
        'page_for_posts' => get_option( 'page_for_posts' )
    );

    /*----------------------------------------------------------------------------*
     * REMOVE / RENAME ADMIN MENU ITEMS
     *----------------------------------------------------------------------------*/

    add_action( 'admin_menu', function() {
		global $menu;
	
		$restricted = array( 'shortcodes-ultimate', 'jetpack', 'wp-mcm' ); // 'edit-comments.php'
	
        if ( ! current_user_can( 'manage_options' ) ) {
			$restricted[] = 'wpseo_dashboard';
			$restricted[] = 'tools.php';
            $restricted[] = 'vc-general';
            $restricted[] = 'vc-welcome';
		}
        
        $rename = array(
			'toplevel_page_wpcf7' => __( 'Forms', LWD_TEXT_DOMAIN ),
            'menu-posts' => __( 'Posts', LWD_TEXT_DOMAIN )
		);
        
        $reicon = array(
			'toplevel_page_wpcf7' => 'dashicons-forms',
            'menu-posts' => 'dashicons-media-document'
		);
        
		foreach ( $menu as $key => $item ) {
			//Rescrict menu item
			if ( in_array( $item[2] != NULL ? $item[2] : "", $restricted ) ) {
				unset( $menu[$key] );
			}
			
			//Rename menu item
			if ( isset( $item[5] ) && $item[5] != NULL && !empty( $rename ) && isset( $rename[ $item[5] ] ) ) {
				$menu[$key][0] = $rename[ $item[5] ];
			}
            
            //Reicon menu item
			if ( isset( $item[5] ) && $item[5] != NULL && !empty( $reicon ) && isset( $reicon[ $item[5] ] ) ) {
				$menu[$key][6] = $reicon[ $item[5] ];
			}
		}

	}, 999 );

    /*----------------------------------------------------------------------------*
     * REMOVE METABOXES
     *----------------------------------------------------------------------------*/

    add_action( 'do_meta_boxes', function() {
        //remove_meta_box( 'authordiv' , 'post' , 'normal' ); 
        //remove_meta_box( 'commentstatusdiv' , 'post' , 'normal' );
        //remove_meta_box( 'commentsdiv' , 'post' , 'normal' );
        remove_meta_box( 'postimagediv' , 'page' , 'side' );
        remove_meta_box( 'postcustom' , 'post' , 'normal' ); 
        remove_meta_box( 'authordiv' , 'page' , 'normal' ); 
        remove_meta_box( 'commentstatusdiv' , 'page' , 'normal' );
        remove_meta_box( 'commentsdiv' , 'page' , 'normal' );
        remove_meta_box( 'revisionsdiv' , 'page' , 'normal' ); 
        remove_meta_box( 'postcustom' , 'page' , 'normal' ); 
        
        $post_id = ( isset( $_GET['post'] ) ) ? $_GET['post'] : ( ( isset( $_POST['post_ID'] ) ) ? $_POST['post_ID'] : false );
        
        if ( $post_id ) {
            global $wp_options;
            
            if ( isset( $wp_options['page_on_front'] ) && $post_id == $wp_options['page_on_front'] ) {
                remove_meta_box( 'postimagediv' , 'page' , 'side' );
            }
        }
    } );

    add_action( 'admin_init', function() {
        $post_id = ( isset( $_GET['post'] ) ) ? $_GET['post'] : ( ( isset( $_POST['post_ID'] ) ) ? $_POST['post_ID'] : false );
        
        if( !isset( $post_id ) ) return;
        
        global $wp_options;

        if ( ( isset( $wp_options['page_on_front'] ) && $post_id == $wp_options['page_on_front'] )
        || $post_id == get_option( LWD_OPTIONS_NAME . '_contact' ) ) {
            remove_post_type_support( 'page', 'excerpt' );
        }
        
        if ( isset( $wp_options['page_for_posts'] ) && $post_id == $wp_options['page_for_posts'] ) {
            remove_post_type_support( 'page', 'excerpt' );
            remove_post_type_support( 'page', 'editor' );
        }
    } );

    /*----------------------------------------------------------------------------*
     * REPLACE ATTACHMENT URL TO HTTPS
     *----------------------------------------------------------------------------*/

    add_filter( 'wp_get_attachment_url', function( $url ) {
        if( is_ssl() ) {
            $url = str_replace( 'http://', 'https://', $url );
        }
        return $url;
    }, 10, 1 );

    /*----------------------------------------------------------------------------*
     * ADD NEW COLUMNS FOR BANNERS
     *----------------------------------------------------------------------------*/

    // ADD NEW COLUMNS
    add_filter( 'manage_testimony_posts_columns', function( $columns ) {
        $columns['link'] = __( 'Link', LWD_TEXT_DOMAIN );
        $columns['thumb'] = __( 'Picture', LWD_TEXT_DOMAIN );
        return $columns;
    } );

    add_filter( 'manage_service_posts_columns', function( $columns ) {
        $columns['link'] = __( 'Link', LWD_TEXT_DOMAIN );
        $columns['thumb-icon'] = __( 'Picture / Icon', LWD_TEXT_DOMAIN );
        return $columns;
    } );

    // SHOW COLUMNS VALUES
    add_action( 'manage_testimony_posts_custom_column', 'lwd_add_testimony_service_custom_column', 10, 2 );
    add_action( 'manage_service_posts_custom_column', 'lwd_add_testimony_service_custom_column', 10, 2 );

    function lwd_add_testimony_service_custom_column( $column_name, $post_ID ) {
        
        if ( $column_name == 'link' ) {
            
            $link = lwd_get_link( $post_ID, 'custom-link' );
            
            if ( strpos( $link['url'], '404' ) !== false ) {
                echo __( 'No link', LWD_TEXT_DOMAIN );
            } else if ( !empty( $link ) && isset( $link['url'] ) && isset( $link['text'] ) ) {
                echo sprintf( $link['url'] != '' ? '<a href="%1$s">%2$s</a>' : '%2$s', $link['url'], $link['text'] );
            }  else {
                echo '--';
            };
            
        } else if ( $column_name == 'thumb' ) {
            
            echo get_the_post_thumbnail( $post_ID, 'thumbnail', array( 'style' => 'max-width: 120px; display: inline; height: auto' ) );
        
        } else if ( $column_name == 'thumb-icon' ) {
            
            $icon = get_post_meta( $post_ID, 'icon', true );
            
            if ( $icon != '' ) {
                $icon = wp_get_attachment_image_src( $icon, 'thumbnail' );
                echo !empty( $icon ) ? '<img src="' . $icon[0] . '" alt="Service icon" style="max-width: 120px; display: inline; height: auto" />' : '';
            } else {
                echo get_the_post_thumbnail( $post_ID, 'thumbnail', array( 'style' => 'max-width: 120px; display: inline; height: auto' ) );
            }
            
        }
        
    }

    /*----------------------------------------------------------------------------*
     * INITIALIZE META BOXES
     *----------------------------------------------------------------------------*/

    add_action( 'admin_init', function() {

        /**
        * Create a custom meta boxes array that we pass to 
        * the OptionTree Meta Box API Class.
        */
        
        $lwd_options_meta_box = array();
        
        // Custom link meta box
        $lwd_options_meta_box['custom-link'] = array(
            'id'          => 'custom-link',
            'title'       => __( 'Link to detail', LWD_TEXT_DOMAIN ),
            'desc'        => '',
            'pages'       => array( 'testimony', 'service' ),
            'context'     => 'normal',
            'priority'    => 'high',
            'fields'      => array(
                array(
                    'label'       => __( 'Type of link', LWD_TEXT_DOMAIN ),
                    'id'          => 'custom-link',
                    'desc'        => __( 'Select the type of link', LWD_TEXT_DOMAIN ),
                    'type'        => 'select',
                    'choices'     => array(
                        array(
                            'value'       => '',
                            'label'       => __( 'Classic link to detail', LWD_TEXT_DOMAIN ),
                        ),
                        array(
                            'value'       => 'internal',
                            'label'       => __( 'Internal link', LWD_TEXT_DOMAIN ),
                        ),
                        array(
                            'value'       => 'external',
                            'label'       => __( 'External link', LWD_TEXT_DOMAIN ),
                        ),
                        array(
                            'value'       => 'phone',
                            'label'       => __( 'Phone', LWD_TEXT_DOMAIN ),
                        ),
                        array(
                            'value'       => 'e-mail',
                            'label'       => __( 'E-mail', LWD_TEXT_DOMAIN ),
                        ),
                        array(
                            'value'       => 'none',
                            'label'       => __( 'No link', LWD_TEXT_DOMAIN ),
                        )
                    ),
                    'std'		  => ''
                ),
                array(
                    'label'        => __( 'Select an internal link', LWD_TEXT_DOMAIN ),
                    'id'          => 'custom-link-internal',
                    'type'        => 'text',
                    'class'       => 'ot-select-dynamic-link',
                    'std'		  => '',
                    'condition'   => 'custom-link:is(internal)'
                ),
                array(
                    'label'        => __( 'Enter a URL', LWD_TEXT_DOMAIN ),
                    'id'          => 'custom-link-external',
                    'type'        => 'text',
                    'std'		  => '',
                    'condition'   => 'custom-link:is(external)'
                ),
                array(
                    'label'       => __( 'Open link in new window / tab?', LWD_TEXT_DOMAIN ),
                    'id'          => 'custom-link-target',
                    'type'        => 'on-off',
                    'std'		  => 'off',
                    'condition'   => 'custom-link:is(external)'
                ),
                array(
                    'label'        => __( 'Enter the phone number', LWD_TEXT_DOMAIN ),
                    'id'          => 'custom-link-phone',
                    'type'        => 'text',
                    'std'		  => '',
                    'condition'   => 'custom-link:is(phone)'
                ),
                array(
                    'label'        => __( 'Enter an email address', LWD_TEXT_DOMAIN ),
                    'id'          => 'custom-link-e-mail',
                    'type'        => 'text',
                    'std'		  => '',
                    'condition'   => 'custom-link:is(e-mail)'
                )
            )
        );

        // Gallery meta box
        $lwd_options_meta_box['gallery'] = array(
            'id'          => 'gallery',
            'title'       => __( 'Gallery', LWD_TEXT_DOMAIN ),
            'desc'        => '',
            'pages'       => array( 'testimony', 'service' ),
            'context'     => 'normal',
            'priority'    => 'low',
            'fields'      => array(
                array(
                    'label'       => '',
                    'id'          => 'gallery',
                    'type'        => 'gallery',
                    'class'       => 'ot-gallery-shortcode',
                    'std'		  => ''
                )
            )
        );
        
        // Gallery desc meta box
        $lwd_options_meta_box['content-gallery'] = array(
            'id'          => 'content-gallery',
            'title'       => __( 'Content below the gallery', LWD_TEXT_DOMAIN ),
            'desc'        => '',
            'pages'       => array( 'testimony' ),
            'context'     => 'normal',
            'priority'    => 'low',
            'fields'      => array(
                array(
                    'label'       => '',
                    'id'          => 'gallery-desc',
                    'type'        => 'textarea',
                )
            )
        );
        
        // Featured video meta box
        $lwd_options_meta_box['testimony-video'] = array(
            'id'          => 'testimony-video',
            'title'       => __( 'Featured video', LWD_TEXT_DOMAIN ),
            'desc'        => '',
            'pages'       => array( 'testimony' ),
            'context'     => 'side',
            'priority'    => 'low',
            'fields'      => array(
                array(
                    'label'       => '',
                    'id'          => 'video',
                    'type'        => 'text',
                    'desc'        => __( 'Enter a URL link to Youtube or Vimeo.', LWD_TEXT_DOMAIN ),
                    'class'       => 'ot-side-desc-top',
                    'std'		  => ''
                )
            )
        );
        
        // Featured icon meta box
        $lwd_options_meta_box['service-icon'] = array(
            'id'          => 'service-icon',
            'title'       => __( 'Service icon', LWD_TEXT_DOMAIN ),
            'desc'        => '',
            'pages'       => array( 'service' ),
            'context'     => 'side',
            'priority'    => 'low',
            'fields'      => array(
                array(
                    'label'       => '',
                    'id'          => 'icon',
                    'type'        => 'upload',
                    'desc'        => __( 'Select the icon to display in the services overview.', LWD_TEXT_DOMAIN ),
                    'class'       => 'ot-upload-attachment-id ot-side-desc-top',
                    'std'		  => ''
                )
            )
        );
        
        // Hero meta box
        $lwd_options_meta_box['hero'] = array(
            'id'          => 'hero',
            'title'       => __( 'Page title', LWD_TEXT_DOMAIN ),
            'desc'        => '',
            'pages'       => array( 'page' ),
            'context'     => 'side',
            'priority'    => 'low',
            'fields'      => array(
                array(
                    'label'       => __( 'Display page title?', LWD_TEXT_DOMAIN ),
                    'id'          => 'hero',
                    'type'        => 'on-off',
                    'std'		  => 'on'
                ),
                array(
                    'label'       => __( 'Image under the title', LWD_TEXT_DOMAIN ),
                    'id'          => '_thumbnail_id',
                    'type'        => 'upload',
                    'desc'        => sprintf( __( 'Recommended image size: %s', LWD_TEXT_DOMAIN ), '<strong>1920 x 767px</strong>' ) . ' ' . sprintf( __( 'Image size uploaded should not exceed 250 KB (ideally up to 150 KB). We recommend to use <a href="%s" target="_blank">this image compressor</a>.', LWD_TEXT_DOMAIN ), 'https://www.compressor.io/' ),
                    'class'       => 'ot-upload-attachment-id ot-side-desc-top',
                    'std'		  => '',
                    'condition'   => 'hero:is(on)'
                ),
                array(
                    'label'       => __( 'Custom title', LWD_TEXT_DOMAIN ),
                    'id'          => 'hero-custom-title',
                    'type'        => 'text',
                    'std'		  => '',
                    'desc'        => __( 'You can set your custom title hete. Leave the field blank to use page title.', LWD_TEXT_DOMAIN ),
                    'class'       => 'ot-side-desc-top',
                    'condition'   => 'hero:is(on)'
                ),
            )
        );

        /**
        * Register our meta boxes using the 
        * ot_register_meta_box() function.
        */
        if ( function_exists( 'ot_register_meta_box' ) ) {

            global $pagenow, $wp_options;

            $post_id = ( isset( $_GET['post'] ) ) ? $_GET['post'] : ( ( isset( $_POST['post_ID'] ) ) ? $_POST['post_ID'] : false );
            
            if ( $post_id && isset( $wp_options['page_on_front'] ) && $post_id == $wp_options['page_on_front'] ) {
                
                ot_register_meta_box( array(
                    'id'          => 'hero',
                    'title'       => __( 'Welcome heading', LWD_TEXT_DOMAIN ),
                    'pages'       => array( 'page' ),
                    'context'     => 'normal',
                    'priority'    => 'low',
                    'fields'      => array(
                        array(
                            'label'       => __( 'Text align', LWD_TEXT_DOMAIN ),
                            'id'          => 'hero-align',
                            'type'        => 'select',
                            'choices'     => array(
                                array(
                                    'value'       => '',
                                    'label'       => __( 'Align center', LWD_TEXT_DOMAIN ),
                                ),
                                array(
                                    'value'       => 'left',
                                    'label'       => __( 'Align left', LWD_TEXT_DOMAIN ),
                                ),
                                array(
                                    'value'       => 'right',
                                    'label'       => __( 'Align right', LWD_TEXT_DOMAIN ),
                                )
                            ),
                            'desc'        => __( 'Select alignment of text, description and buttons.', LWD_TEXT_DOMAIN ),
                            'std'		  => ''
                        ),
                        array(
                            'label'       => __( 'Headline text', LWD_TEXT_DOMAIN ),
                            'id'          => 'hero-title',
                            'type'        => 'text',
                            'desc'        => '',
                            'std'		  => ''
                        ),
                        array(
                            'label'       => __( 'Headline color', LWD_TEXT_DOMAIN ),
                            'id'          => 'hero-title-color',
                            'type'        => 'select',
                            'choices'     => array(
                                array(
                                    'value'       => '',
                                    'label'       => __( 'Dark text', LWD_TEXT_DOMAIN ),
                                ),
                                array(
                                    'value'       => 'light',
                                    'label'       => __( 'Bright text', LWD_TEXT_DOMAIN ),
                                )
                            ),
                            'desc'        => '',
                            'std'		  => '',
                            'condition'   => 'hero-title:not()'
                        ),
                        array(
                            'label'       => __( 'Paragraph text', LWD_TEXT_DOMAIN ),
                            'id'          => 'hero-perex',
                            'type'        => 'textarea',
                            'desc'        => __( 'Enter the paragraph text below the title. The text is limited to 200 characters.', LWD_TEXT_DOMAIN ),
                            'rows'        => '6',
                            'std'		  => ''
                        ),
                        array(
                            'label'       => __( 'Headline background', LWD_TEXT_DOMAIN ),
                            'id'          => 'hero-bg',
                            'type'        => 'upload',
                            'desc'        => sprintf( __( 'Recommended image size: %s', LWD_TEXT_DOMAIN ), '<strong>1920 x 1080px</strong>' ) . ' ' . sprintf( __( 'Image size uploaded should not exceed 250 KB (ideally up to 150 KB). We recommend to use <a href="%s" target="_blank">this image compressor</a>.', LWD_TEXT_DOMAIN ), 'https://www.compressor.io/' ),
                            'class'       => 'ot-upload-attachment-id',
                            'std'		  => ''
                        ),
                        array(
                            'label'       => __( 'Headline buttons', LWD_TEXT_DOMAIN ),
                            'desc'        => __( 'Set the buttons display on the title page. The maximum buttons are 2. Each button needs to have text, link and style.', LWD_TEXT_DOMAIN ),
                            'id'          => 'hero-buttons',
                            'type'        => 'list-item',
                            'class'       => 'ot-side-desc-top',
                            'std'		  => '',
                            'settings'    => array(
                                array(
                                    'label'       => __( 'Button style', LWD_TEXT_DOMAIN ),
                                    'id'          => 'style',
                                    'type'        => 'select',
                                    'choices'     => array(
                                        array(
                                            'value'       => '',
                                            'label'       => __( 'Classic button', LWD_TEXT_DOMAIN ),
                                        ),
                                        array(
                                            'value'       => 'highlight',
                                            'label'       => __( 'Highlight button', LWD_TEXT_DOMAIN ),
                                        ),
                                        array(
                                            'value'       => 'border',
                                            'label'       => __( 'Border button', LWD_TEXT_DOMAIN ),
                                        ),
                                        array(
                                            'value'       => 'text-dark',
                                            'label'       => __( 'Black text button', LWD_TEXT_DOMAIN ),
                                        )
                                    ),
                                    'std'		  => ''
                                ),
                                array(
                                    'label'       => __( 'Type of link', LWD_TEXT_DOMAIN ),
                                    'id'          => 'link',
                                    'desc'        => __( 'Select the type of link', LWD_TEXT_DOMAIN ),
                                    'type'        => 'select',
                                    'choices'     => array(
                                        array(
                                            'value'       => '',
                                            'label'       => __( 'Internal link', LWD_TEXT_DOMAIN ),
                                        ),
                                        array(
                                            'value'       => 'external',
                                            'label'       => __( 'External link', LWD_TEXT_DOMAIN ),
                                        )
                                    ),
                                    'std'		  => ''
                                ),
                                array(
                                    'label'        => __( 'Select an internal link', LWD_TEXT_DOMAIN ),
                                    'id'          => 'link-internal',
                                    'type'        => 'text',
                                    'class'       => 'ot-select-dynamic-link',
                                    'std'		  => '',
                                    'condition'   => 'link:not(external)'
                                ),
                                array(
                                    'label'        => __( 'Enter a URL', LWD_TEXT_DOMAIN ),
                                    'id'          => 'link-external',
                                    'type'        => 'text',
                                    'std'		  => '',
                                    'condition'   => 'link:is(external)'
                                ),
                                array(
                                    'label'       => __( 'Open link in new window / tab?', LWD_TEXT_DOMAIN ),
                                    'id'          => 'link-target',
                                    'type'        => 'on-off',
                                    'std'		  => 'off',
                                    'condition'   => 'link:is(external)'
                                )
                            )
                        )
                    )
                ) );
                    
            }
            
            if ( $post_id && $post_id == get_option( LWD_OPTIONS_NAME . '_contact' ) ) {
                
                $lwd_options_meta_box['contact-info'] = array(
                    'id'          => 'contact-info',
                    'title'       => __( 'Contact settings', LWD_TEXT_DOMAIN ),
                    'desc'        => __( 'Define your contact information here. Enter the item name and fill other fields - no fields are required. Each item represents one column on the page.', LWD_TEXT_DOMAIN ),
                    'pages'       => array( 'page' ),
                    'context'     => 'normal',
                    'priority'    => 'high',
                    'fields'      => array(
                        array(
                            'label'       => '',
                            'id'          => 'contacts',
                            'type'        => 'list-item',
                            'class'       => 'ot-side-desc-top',
                            'std'		  => array(
                                array(
                                    'title' => __( 'Contact information', LWD_TEXT_DOMAIN ),
                                    'address' => _x( "Street 123/43,\n678 90 City", 'contact dummy data', LWD_TEXT_DOMAIN ),
                                    'phone' => _x( '+44 777 888 555', 'contact dummy data', LWD_TEXT_DOMAIN ),
                                    'fax' => _x( '+44 232 242 343', 'contact dummy data', LWD_TEXT_DOMAIN ),
                                    'e-mail' => _x( 'info@domain.co.uk', 'contact dummy data', LWD_ELEM_DOMAIN ),
                                    'company-number' => '',
                                    'tax-number' => '',
                                    'desc' => ''
                                ),
                                array(
                                    'title' => __( 'Billing information', LWD_TEXT_DOMAIN ),
                                    'address' => _x( "221 High Holborn,\nLondon", 'contact dummy data', LWD_TEXT_DOMAIN ),
                                    'phone' => _x( '+44 20 6021 1508', 'contact dummy data', LWD_TEXT_DOMAIN ),
                                    'fax' => '',
                                    'e-mail' => _x( 'info@domain.co.uk', 'contact dummy data', LWD_ELEM_DOMAIN ),
                                    'company-number' => '24171816',
                                    'tax-number' => '',
                                    'desc' => ''
                                )
                            ),
                            'settings'    => array(
                                array(
                                    'label'       => __( 'Address', LWD_TEXT_DOMAIN ),
                                    'id'          => 'address',
                                    'type'        => 'textarea-simple',
                                    'rows'        => 4,
                                    'std'		  => ''
                                ),
                                array(
                                    'label'       => __( 'Phone', LWD_TEXT_DOMAIN ),
                                    'id'          => 'phone',
                                    'type'        => 'text',
                                    'std'		  => ''
                                ),
                                array(
                                    'label'       => __( 'E-mail', LWD_TEXT_DOMAIN ),
                                    'id'          => 'e-mail',
                                    'type'        => 'text',
                                    'std'		  => ''
                                ),
                                array(
                                    'label'       => __( 'Company ID', LWD_TEXT_DOMAIN ),
                                    'id'          => 'company-number',
                                    'type'        => 'text',
                                    'std'		  => ''
                                ),
                                array(
                                    'label'       => __( 'VAT ID', LWD_TEXT_DOMAIN ),
                                    'id'          => 'tax-number',
                                    'type'        => 'text',
                                    'std'		  => ''
                                ),
                                array(
                                    'label'       => __( 'Fax', LWD_TEXT_DOMAIN ),
                                    'id'          => 'fax',
                                    'type'        => 'text',
                                    'std'		  => ''
                                ),
                                array(
                                    'label'       => __( 'Additional description', LWD_TEXT_DOMAIN ),
                                    'id'          => 'desc',
                                    'type'        => 'textarea-simple',
                                    'rows'        => 4,
                                    'std'		  => ''
                                ),
                            )
                        )
                    )
                );
                
                if ( isset( $lwd_options_meta_box['hero'] ) ) unset( $lwd_options_meta_box['hero'] );
            }
            
            $lwd_options_meta_box = apply_filters( 'lwd_filter_meta_boxes', $lwd_options_meta_box );

            foreach ( $lwd_options_meta_box as $meta_box ) {
                if ( isset( $meta_box['page_template'] ) && isset( $post_template ) && isset( $pagenow ) && ( $post_template != $meta_box['page_template'] || $pagenow != $meta_box['page_template'] ) ) continue;
                
                ot_register_meta_box( $meta_box );
            }
        }
    } );

    // Filter for OT Video Meta-box  
    add_filter( 'ot_meta_box_post_format_video', function() {

        return array(
            'id'        => 'ot-post-format-video',
            'title'     => __( 'Featured video', LWD_TEXT_DOMAIN ),
            'desc'      => __( 'Enter a URL link to Youtube or Vimeo.', LWD_TEXT_DOMAIN ),
            'pages'     => array( 'post' ),
            'context'   => 'side',
            'priority'  => 'low',
            'fields'    => array(
                array(
                    'id'          => 'video',
                    'type'        => 'text',
                )
            )
        );
    } );

    // Filter for OT Link Meta-box  
    add_filter( 'ot_meta_box_post_format_link', function() {

        return array(
            'id'        => 'ot-post-format-link',
            'title'     => __( 'Link', LWD_TEXT_DOMAIN ),
            'pages'     => array( 'post' ),
            'context'   => 'side',
            'priority'  => 'low',
            'fields'    => array(
                array(
                    'id'          => 'custom-link',
                    'type'        => 'text',
                    'desc'        => __( 'Enter a URL', LWD_TEXT_DOMAIN ),
                    'class'       => 'ot-select-dynamic-link'
                )
            )
        );
    } );

    // Filter for OT Gallery Meta-box  
    add_filter( 'ot_meta_box_post_format_gallery', function() {

        return array(
            'id'        => 'ot-post-format-gallery',
            'title'     => __( 'Gallery', LWD_TEXT_DOMAIN ),
            'pages'     => array( 'post' ),
            'context'   => 'side',
            'priority'  => 'low',
            'fields'    => array(
                array(
                    'label'       => '',
                    'id'          => 'gallery',
                    'type'        => 'gallery',
                    'class'       => 'ot-gallery-shortcode',
                    'std'		  => ''
                )
            )
        );
    } );

    /*----------------------------------------------------------------------------*
     * REGISTER PERMALINK SETTINGS
     *----------------------------------------------------------------------------*/

    add_action( 'load-options-permalink.php', function() {

        if ( isset( $_POST['lwd_rewrite_slug'] ) ) {

            foreach ( $_POST['lwd_rewrite_slug'] as $key => $value ) {
                update_option( $key, sanitize_title( $value ) );
            }    
        }
        
        add_settings_field( 'testimony',  __( 'Testimony base', LWD_TEXT_DOMAIN ), 'lwd_setting_input', 'permalink', 'optional', array ( 'label' => '', 'name' => 'testimony' ) );
        
        add_settings_field( 'service',  __( 'Service base', LWD_TEXT_DOMAIN ), 'lwd_setting_input', 'permalink', 'optional', array ( 'label' => '', 'name' => 'service' ) );

    } );

    add_filter( 'admin_init' , function() {
        
        register_setting( 'reading',  LWD_OPTIONS_NAME . '_contact' );

        add_settings_field( 'event',  __( 'Contact page', LWD_TEXT_DOMAIN ), 'lwd_setting_select_pages', 'reading', 'default', array ( 'label' => '', 'name' => 'contact' ) );
        
    }, 10, 1 );

    // Display and fill the form field
    
    function lwd_setting_select_pages( $args ) {
        if ( !isset( $args['name'] ) || $args['name'] == '' ) return false;
        
        global $lwd_pages_loop;
        
        if ( empty( $lwd_pages_loop ) ) {
            $pages = get_pages();
            $lwd_pages_loop = $pages;
        } else {
            $pages = $lwd_pages_loop;
        }
        
        $options_name = LWD_OPTIONS_NAME . '_' . $args['name'];
       
        echo '<label for="' . $options_name . '">' . ( isset( $args['label'] ) && $args['label'] != '' ? $args['label'] . ': ' : '' );
        echo '<select name="' . $options_name . '" id="' . $options_name . '">';
        echo '<option value="0">' . __( '-- Select an option --', LWD_TEXT_DOMAIN ) . '</option>';
        foreach ( $pages as $page ) {
            echo '<option class="level-' . ( $page->post_parent != 0 ? 1 : 0 ) . '" value="' . $page->ID . '"' . ( get_option( $options_name ) == $page->ID ? ' selected="selected"' : '' ) . '>' . ( $page->post_parent != 0 ? '&nbsp;&nbsp;&nbsp;' : '' ) . $page->post_title . '</option>';
        }
        echo '</select>';
        echo '</label>';
    }

    function lwd_setting_input( $args ) {
        if ( !isset( $args['name'] ) || $args['name'] == '' ) return false;
        
        $options_name = LWD_OPTIONS_NAME . '_' . $args['name'];
       
        echo '<label for="' . $options_name . '">' . ( isset( $args['label'] ) && $args['label'] != '' ? $args['label'] . ': ' : '' );
        
        echo '<input type="text" id="' . $options_name . '" name="lwd_rewrite_slug[' . $options_name . ']" value="' . esc_attr( get_option( $options_name, '' ) ) . '" class="regular-text" />';
        
        if ( isset( $args['desc'] ) && $args['desc'] != '' ) {
            echo '<p class="description">'  . $args['desc'] . '</p>';
        }
    }

    /*----------------------------------------------------------------------------*
     * ADD RECOMMEND RESOLUTION FOR POST THUMBNAIL
     *----------------------------------------------------------------------------*/

    add_filter( 'admin_post_thumbnail_html', function( $content ) {
        global $post;
        
        if ( !empty( $post ) ) $content .= '<p>' . sprintf( __( 'Recommended size if featured image: %s', LWD_TEXT_DOMAIN ), '<strong>' . ( $post->post_type == 'page' ? '1920 x 767' : '760 x 506' ) . 'px</strong>' ) . ' ' . sprintf( __( 'Image size uploaded should not exceed 250 KB (ideally up to 150 KB). We recommend to use <a href="%s" target="_blank">this image compressor</a>.', LWD_TEXT_DOMAIN ), 'https://www.compressor.io/' ) .'</p>';
        
        return $content;
    }, 10, 1 );

    add_action( 'load-upload.php', 'lwd_add_upload_help_tab' );
    add_action( 'load-media-new.php', 'lwd_add_upload_help_tab' );

    function lwd_add_upload_help_tab() {
        $screen = get_current_screen();
        
        $screen->add_help_tab( array(
            'id'		=> 'recommended-size',
            'title'		=> __( 'Recommended sizes' ),
            'priority'  => 15,
            'content'	=>
                '<p>' . sprintf( __( 'Size of each uploaded image should not exceed 250 KB (ideally up to 150 KB). We recommend to use <a href="%s" target="_blank">this image compressor</a> before upload to Media library. This compressor reduce image size and keep quality.', LWD_TEXT_DOMAIN ), 'https://www.compressor.io/' ) . '</p>'
        ) );
    }

    /*----------------------------------------------------------------------------*
     * CUSTOM UPDATED MESSAGES FOR CUSTOM POST TYPES
     *----------------------------------------------------------------------------*/

    add_filter( 'post_updated_messages', function( $messages ) {
        global $post, $post_ID;
        
        if ( !isset( $messages['testimony'] ) || !isset( $messages['service'] ) ) $post_url = get_permalink( $post_ID );
        
        if ( !isset( $messages['testimony'] ) ) {
            $messages['testimony'] = $messages['post'];
            
            $messages['testimony'][1] = sprintf( __( 'Testimony updated. <a href="%s">Show testimony</a> detail', LWD_TEXT_DOMAIN ), esc_url( $post_url ) );
            $messages['testimony'][4] = __( 'Testimony updated.', LWD_TEXT_DOMAIN );
            $messages['testimony'][6] = sprintf( __( 'Testimony published. <a href="%s">Show testimony</a> detail', LWD_TEXT_DOMAIN ), esc_url( $post_url ) );
            $messages['testimony'][7] = __( 'Testimony saved.', LWD_TEXT_DOMAIN );
            $messages['testimony'][8] = sprintf( __( 'Testimony draft submitted for approval. <a target="_blank" href="%s">Show testimony preview</a>', LWD_TEXT_DOMAIN ), esc_url( add_query_arg( 'preview', 'true', $post_url ) ) );
            $messages['testimony'][9] = sprintf( __( 'Testimony scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Show testimony preview</a>', LWD_TEXT_DOMAIN ), date_i18n( __( 'F j, Y @ g:i a', LWD_TEXT_DOMAIN ), strtotime( $post->post_date ) ), esc_url( $post_url ) );
            $messages['testimony'][10] = sprintf( __( 'Testimony draft updated. <a target="_blank" href="%s">Show testimony preview</a>', LWD_TEXT_DOMAIN ), esc_url( add_query_arg( 'preview', 'true', $post_url ) ) );
        }
        
        if ( !isset( $messages['service'] ) ) {
            $messages['service'] = $messages['post'];
            
            $messages['service'][1] = sprintf( __( 'Service updated. <a href="%s">Show service</a> detail', LWD_TEXT_DOMAIN ), esc_url( $post_url ) );
            $messages['service'][4] = __( 'Service updated.', LWD_TEXT_DOMAIN );
            $messages['service'][6] = sprintf( __( 'Service published. <a href="%s">Show service</a> detail', LWD_TEXT_DOMAIN ), esc_url( $post_url ) );
            $messages['service'][7] = __( 'Service saved.', LWD_TEXT_DOMAIN );
            $messages['service'][8] = sprintf( __( 'Service draft submitted for approval. <a target="_blank" href="%s">Show service preview</a>', LWD_TEXT_DOMAIN ), esc_url( add_query_arg( 'preview', 'true', $post_url ) ) );
            $messages['service'][9] = sprintf( __( 'Service scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Show service preview</a>', LWD_TEXT_DOMAIN ), date_i18n( __( 'F j, Y @ g:i a', LWD_TEXT_DOMAIN ), strtotime( $post->post_date ) ), esc_url( $post_url ) );
            $messages['service'][10] = sprintf( __( 'Service draft updated. <a target="_blank" href="%s">Show service preview</a>', LWD_TEXT_DOMAIN ), esc_url( add_query_arg( 'preview', 'true', $post_url ) ) );
        }
        
        return $messages;
    } );

    /*----------------------------------------------------------------------------*
     * AJAX HANDLER FOR FINDING POSTS
     *----------------------------------------------------------------------------*/

    add_action( 'wp_ajax_link_suggest_find_posts', function() {
		check_ajax_referer( 'ls-find-posts', 'nonce' );

		$post_types = array();

		if ( ! empty( $_POST['post_types'] ) ) {
			foreach ( $_POST['post_types'] as $post_type ) {
				$post_types[ $post_type ] = get_post_type_object( $post_type );
			}
		}

		if ( empty( $post_types ) ) {
			$post_types['post'] = get_post_type_object( 'post' );
		}

		$args = array(
			'post_type'      => array_keys( $post_types ),
			'post_status'    => 'any',
			'posts_per_page' => 30,
		);

		if ( ! empty( $_POST['s'] ) ) {
			$args['s'] = wp_unslash( $_POST['s'] );
		}

		$posts = get_posts( $args );

		if ( ! $posts ) {
			wp_send_json_error( __( 'Nothing found.', LWD_TEXT_DOMAIN ) );
		}
        
        $html = sprintf(
			'<table class="widefat"><thead><tr><th>%1$s</th><th class="no-break">%2$s</th><th class="no-break">%3$s</th><th class="no-break">%4$s</th></tr></thead><tbody>',
			__( 'Title', LWD_TEXT_DOMAIN ),
			__( 'Type', LWD_TEXT_DOMAIN ),
			__( 'Date', LWD_TEXT_DOMAIN ),
			__( 'Status', LWD_TEXT_DOMAIN )
		);
        
        $date_format = get_option( 'date_format' );

		foreach ( $posts as $post ) {
            
			$title     = trim( $post->post_title ) ? $post->post_title : __( '(without title)', LWD_TEXT_DOMAIN );
			
            if ( 'attachment' == get_post_type( $post->ID ) ) {
                $post_link =  wp_get_attachment_url( $post->ID );
            } else {
                $post_link = ( class_exists( 'PostLinkShortcodes' ) ? '[' . $post->post_type . '_url ' . $post->ID . ']' : get_permalink( $post->ID ) );
            }
            
			$status    = '';

			switch ( $post->post_status ) {
				case 'publish' :
				case 'private' :
					$status = __( 'Published', LWD_TEXT_DOMAIN );
					break;
				case 'future' :
					$status = __( 'Scheduled', LWD_TEXT_DOMAIN );
					break;
				case 'pending' :
					$status = __( 'Pending Review', LWD_TEXT_DOMAIN );
					break;
				case 'draft' :
					$status = __( 'Draft', LWD_TEXT_DOMAIN );
					break;
			}

			if ( '0000-00-00 00:00:00' == $post->post_date ) {
				$time = '';
			} else {
				/* translators: date format in table columns, see http://php.net/date */
				$time = mysql2date( $date_format, $post->post_date );
			}

			$post_type_label = get_post_type_object( $post->post_type )->labels->singular_name;
            
			if ( 'attachment' == get_post_type( $post->ID ) ) {
				if ( preg_match( '/^.*?\.(\w+)$/', get_attached_file( $post->ID ), $matches ) ) {
					$post_type_label = esc_html( strtoupper( $matches[1] ) );
				} else {
					$post_type_label = strtoupper( str_replace( 'image/', '', get_post_mime_type( $post->ID ) ) );
				}
			}

			$html .= sprintf(
				'<tr class="found-posts"><td>%1$s <input type="hidden" value="%2$s"></td><td class="no-break">%3$s</td><td class="no-break">%4$s</td><td class="no-break">%5$s</td></tr>',
				esc_html( $title ),
				$post_link,
				esc_html( $post_type_label ),
				esc_html( $time ),
				esc_html( $status )
			);
		}

		$html .= '</tbody></table>';

		wp_send_json_success( $html );
	} );

    /*----------------------------------------------------------------------------*
     * SUPPORT DYNAMIC LINK FOR WYSIWYG
     *----------------------------------------------------------------------------*/

    if ( class_exists( 'PostLinkShortcodes' ) ) {

        add_filter( 'wp_link_query', function( $results, $query ) {

            foreach( $results as $index => $result ) {
                if ( isset( $result['ID'] ) ) $results[$index]['permalink'] = '[' . get_post_type( $result['ID'] ) . '_url ' . $result['ID'] . ']';
            }

            return $results;
        }, 10, 2 );
        
    }

    /*----------------------------------------------------------------------------*
     * DISABLE YOAST ANNOING NOTIFICATIONS 
     *----------------------------------------------------------------------------*/

    if ( class_exists( 'Yoast_Notification_Center' ) ) {
        remove_action( 'admin_notices', array( Yoast_Notification_Center::get(), 'display_notifications' ) );
        remove_action( 'all_admin_notices', array( Yoast_Notification_Center::get(), 'display_notifications' ) );
    }

    /*----------------------------------------------------------------------------*
     * OVERRIDE OT TEXT FUNCTION - TO ADD SVG MAP FOR SELECT LOCATION & LINK SUGGEST
     *----------------------------------------------------------------------------*/

    function ot_type_text( $args = array() ) {
    
        /* turns arguments array into variables */
        extract( $args );
        
        /* LWD EDIT: Add jquery scripts */
        if ( strpos( $field_class, 'ot-select-dynamic-link' ) !== false ) {
            wp_enqueue_script( LWD_SLUG . '-admin-link-suggest' );
            
            $field_class .= ' jq-suggest-field';
        }
        /* LWD EDIT END */
    
        /* verify a description */
        $has_desc = $field_desc ? true : false;
        
        /* format setting outer wrapper */
        echo '<div class="format-setting type-text ' . ( $has_desc ? 'has-desc' : 'no-desc' ) . '">';
      
            /* description */
            echo $has_desc ? '<div class="description">' . htmlspecialchars_decode( $field_desc ) . '</div>' : '';
      
            /* format setting inner wrapper */
            echo '<div class="format-setting-inner">';
      
                /* build text input */
                echo '<input type="text" name="' . esc_attr( $field_name ) . '" id="' . esc_attr( $field_id ) . '" value="' . esc_attr( $field_value ) . '" class="widefat option-tree-ui-input ' . esc_attr( $field_class ) . '" />';
        
                /* LWD EDIT: Add jquery scripts */
                if ( strpos( $field_class, 'ot-select-dynamic-link' ) !== false ) {
                    echo '<button class="option-tree-ui-search-button jq-suggest-link dashicons dashicons-search" title="' . __( 'Suggest URL', LWD_TEXT_DOMAIN ) . '"></button>';
                }
                /* LWD EDIT END */
        
            echo '</div>';
    
        echo '</div>';
    
    }