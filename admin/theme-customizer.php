<?php

/**
 * theme-customizer.php
 *
 * - Theme options
 * - Sanitize inputs for Theme options
 */

    /*----------------------------------------------------------------------------*
     * INITIALIZE THEME CUSTOMIZER
     *----------------------------------------------------------------------------*/

    add_action( 'customize_register', 'lwd_customizer_register' );
	
	function lwd_customizer_register( $wp_customize ) {
        
        /**
		 * Remove Custom CSS panel
	 	 */
        
        $wp_customize->remove_section( 'custom_css' );
        $wp_customize->remove_control( 'custom_css' );
        
         /**
		 * Custom logo
	 	 */
	 	
	 	$wp_customize->add_setting( 'lwd_customizer_logo', array(
			'sanitize_callback' => 'lwd_sanitize_text'
		) );

		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'lwd_customizer_logo', array(
			'label'    => esc_html__( 'Website Logo', LWD_TEXT_DOMAIN ),
			'section'  => 'title_tagline',
			'settings' => 'lwd_customizer_logo',
		) ) );
        
        $wp_customize->add_setting( 'lwd_customizer_retina_logo', array(
			'sanitize_callback' => 'lwd_sanitize_text'
		) );
        
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'lwd_customizer_retina_logo', array(
			'label'    => esc_html__( 'Retina Logo', LWD_TEXT_DOMAIN ),
			'section'  => 'title_tagline',
			'settings' => 'lwd_customizer_retina_logo',
		) ) );
        
        /**
         * Theme options Panel
         */
        
        $wp_customize->add_panel( 'lwd_theme_panel', array(
            'priority'   => 1,
            'capability' => 'edit_theme_options',
            'title'      => esc_html__( 'Theme options', LWD_TEXT_DOMAIN ),
        ) );


        /**
         * General Settings Section
         */
        $wp_customize->add_section( 'lwd_general_settings', array(
            'title'    => esc_html__( 'General settings', LWD_TEXT_DOMAIN ),
            'priority' => 1,
            'panel'    => 'lwd_theme_panel',
        ) );
        
        
        /**
		 * Google API
	 	 */
        
        $wp_customize->add_setting( 'lwd_general_gapi', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_text',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_general_gapi_text', array(
    		'settings' => 'lwd_general_gapi',
        	'label'    => esc_html__( 'API key', LWD_TEXT_DOMAIN ),
        	'description' => sprintf( esc_html__( 'Enter your Google Map key. See more here: %s', LWD_TEXT_DOMAIN ), '<a href="" target="_blank">https://developers.google.com/maps/documentation/javascript/get-api-key</a>' ),
			'section'  => 'lwd_general_settings',
			'type'	   => 'text',
			'priority' => 5
    	) );
        
        /**
		 * Tracking Code
	 	*/
	 	
	 	$wp_customize->add_setting( 'lwd_tracking_code', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_textarea',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_tracking_code', array(
    		'settings' => 'lwd_tracking_code',
        	'label'    => esc_html__( 'Tracking code', LWD_TEXT_DOMAIN ),
        	'description' => esc_html__( 'Enter your analytics code here (eg generated from Google Analytics). Enter the entire Javascript code and not just the ID number.', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_general_settings',
			'type'	   => 'textarea',
			'priority' => 18
    	) );
        
        /**
         * Header Settings Section
         */
        
        $wp_customize->add_section( 'lwd_header_settings', array(
            'title'    => esc_html__( 'Header', LWD_TEXT_DOMAIN ),
            'priority' => 2,
            'panel'    => 'lwd_theme_panel',
        ) );
        
        /**
		 * Phone number
	 	 */
        
        $wp_customize->add_setting( 'lwd_search', array(
			'default'           => 0,
			'sanitize_callback' => 'lwd_sanitize_checkbox',
		) );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'lwd_search', array(
            'settings' => 'lwd_search',
			'label'    => esc_html__( 'Display search?', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_header_settings',
			'type'     => 'checkbox',
            'priority' => 1
		) ) );
        
        /**
		 * Phone number
	 	 */
	 	
	 	$wp_customize->add_setting( 'lwd_phone', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_text',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_phone', array(
    		'settings' => 'lwd_phone',
        	'label'    => esc_html__( 'Phone', LWD_TEXT_DOMAIN ),
        	'description' => esc_html__( 'Enter the phone number to display in the header next to the menu.', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_header_settings',
			'type'	   => 'text',
			'priority' => 2
    	) );
        
        /**
         * Footer Settings Section
         */
        
        $wp_customize->add_section( 'lwd_footer_settings', array(
            'title'    => esc_html__( 'Footer', LWD_TEXT_DOMAIN ),
            'priority' => 3,
            'panel'    => 'lwd_theme_panel',
        ) );
        
        /**
		 * Copyright text
	 	 */
	 	
	 	$wp_customize->add_setting( 'lwd_copyright', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_textarea',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_copyright', array(
    		'settings' => 'lwd_copyright',
        	'label'    => esc_html__( 'Copyright', LWD_TEXT_DOMAIN ),
        	'description' => esc_html__( 'Enter copyright text in footer.', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_footer_settings',
			'type'	   => 'textarea',
			'priority' => 10
    	) );
        
        /**
         * Social Networks Settings Section
         */
        
        $wp_customize->add_section( 'lwd_social_settings', array(
            'title'    => esc_html__( 'Social networks', LWD_TEXT_DOMAIN ),
            'priority' => 4,
            'panel'    => 'lwd_theme_panel',
        ) );
        
        /**
		 * Facebook url
	 	 */
	 	
	 	$wp_customize->add_setting( 'lwd_facebook', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_text',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_facebook', array(
    		'settings' => 'lwd_facebook',
        	'label'    => esc_html__( 'Facebook URL', LWD_TEXT_DOMAIN ),
        	'description' => esc_html__( 'Enter the URL of your Facebook profile.', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_social_settings',
			'type'	   => 'text',
			'priority' => 2
    	) );
        
        /**
		 * Linked-in url
	 	 */
	 	
	 	$wp_customize->add_setting( 'lwd_linkedin', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_text',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_linkedin', array(
    		'settings' => 'lwd_linkedin',
        	'label'    => esc_html__( 'Linked-in URL', LWD_TEXT_DOMAIN ),
        	'description' => esc_html__( 'Enter the URL of your Linked-in profile.', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_social_settings',
			'type'	   => 'text',
			'priority' => 3
    	) );
        
        /**
		 * Google+ url
	 	 */
	 	
	 	$wp_customize->add_setting( 'lwd_google', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_text',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_google', array(
    		'settings' => 'lwd_google',
        	'label'    => esc_html__( 'Google+ URL', LWD_TEXT_DOMAIN ),
        	'description' => esc_html__( 'Enter the URL of your Google+ profile.', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_social_settings',
			'type'	   => 'text',
			'priority' => 1
    	) );
        
        /**
		 * Twitter url
	 	 */
	 	
	 	$wp_customize->add_setting( 'lwd_twitter', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_text',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_twitter', array(
    		'settings' => 'lwd_twitter',
        	'label'    => esc_html__( 'Twitter URL', LWD_TEXT_DOMAIN ),
        	'description' => esc_html__( 'Enter the URL of your Twitter profile.', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_social_settings',
			'type'	   => 'text',
			'priority' => 4
    	) );
        
        /**
		 * Instagram url
	 	 */
	 	
	 	$wp_customize->add_setting( 'lwd_pinterest', array(
       		'default'       	   => '',
        	'capability'     	   => 'edit_theme_options',
        	'sanitize_callback'    => 'lwd_sanitize_text',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
        ) );
 
    	$wp_customize->add_control( 'lwd_pinterest', array(
    		'settings' => 'lwd_pinterest',
        	'label'    => esc_html__( 'Pinterest URL', LWD_TEXT_DOMAIN ),
        	'description' => esc_html__( 'Enter the URL of your Pinterest dashboard.', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_social_settings',
			'type'	   => 'text',
			'priority' => 5
    	) );
        
        /**
		 * Add archive pages settings
	 	 */
        
        $wp_customize->get_section( 'static_front_page' )->title = esc_html__( 'Page display settings', LWD_TEXT_DOMAIN );
        
        $wp_customize->add_setting( LWD_OPTIONS_NAME . '_contact', array(
       		'default'    => get_option( LWD_OPTIONS_NAME . '_contact' ),
        	'capability' => 'manage_options',
        	'type'       => 'option'
        ) );
 
    	$wp_customize->add_control( LWD_OPTIONS_NAME . '_contact', array(
        	'label'     => esc_html__( 'Contact page', LWD_TEXT_DOMAIN ),
			'section'   => 'static_front_page',
			'type'	    => 'dropdown-pages'
    	) );
        
        $wp_customize->add_section( 'lwd_rewrite_settings', array(
            'title'       => esc_html__( 'Permalinks', LWD_TEXT_DOMAIN ),
            'description' => esc_html__( 'Base url settings.', LWD_TEXT_DOMAIN ),
            'priority'    => 199
        ) );
        
        $wp_customize->add_setting( LWD_OPTIONS_NAME . '_testimony', array(
       		'default'    => get_option( LWD_OPTIONS_NAME . '_testimony', '' ),
        	'capability' => 'manage_options',
        	'type'       => 'option'
        ) );
 
    	$wp_customize->add_control( LWD_OPTIONS_NAME . '_testimony', array(
        	'label'    => esc_html__( 'Testimony base', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_rewrite_settings',
			'type'	   => 'text'
    	) );
        
        $wp_customize->add_setting( LWD_OPTIONS_NAME . '_service', array(
       		'default'    => get_option( LWD_OPTIONS_NAME . '_service', '' ),
        	'capability' => 'manage_options',
        	'type'       => 'option'
        ) );
 
    	$wp_customize->add_control( LWD_OPTIONS_NAME . '_service', array(
        	'label'    => esc_html__( 'Service base', LWD_TEXT_DOMAIN ),
			'section'  => 'lwd_rewrite_settings',
			'type'	   => 'text'
    	) );
	
	}

    // Sanitize text
    function lwd_sanitize_text( $input ) {
        return wp_kses_post( force_balance_tags( $input ) );
    }

    // Sanitize textarea output
    function lwd_sanitize_textarea( $text ) {
        return esc_textarea( $text );
    }

    // Sanitize checkbox
    function lwd_sanitize_checkbox( $input ) {
        return ( 1 == $input ) ? 1 : '';
    }

    /**
     * ADD WP EDITOR CONTROL
     */
    if ( !class_exists( 'Text_Editor_Custom_Control' ) ) {
        if ( ! class_exists( 'WP_Customize_Control' ) ) 
            return NULL;
        
        // Register editor control scripts
        add_action( 'customize_controls_enqueue_scripts', 'lwd_admin_customizer_scripts' );
        
        function lwd_admin_customizer_scripts(){
            wp_enqueue_style( LWD_SLUG . '-admin', LWD_ADMIN_URL . '/assets/css/theme-admin-style.css', false, LWD_VERSION, 'all' );
	        wp_enqueue_script( LWD_SLUG . '-admin-customizer', LWD_ADMIN_URL . '/assets/js/jquery.admin.customizer.js', array( 'jquery' ), LWD_VERSION, true );
        }
        
        class Text_Editor_Custom_Control extends WP_Customize_Control {
            
            public $type = 'textarea';

            public function render_content() { ?>
                <label>
                    
                    <?php if ( isset( $this->label ) && !empty( $this->label ) ) { ?>
                        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                    <?php } ?>
                    
                    <?php if ( isset( $this->description ) && !empty( $this->description ) ) { ?>
                        <span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
                    <?php } ?>
                    
                    <?php
                        // Load TinyMCE editor
                        $settings = array(
                            'textarea_name'    => $this->id,
                            'media_buttons'    => false,
                            'drag_drop_upload' => false,
                            'teeny'            => true,
                            'default_editor'  => 'tmce',
                            'quicktags'        => false
                        );
                        
                        $this->filter_editor_setting_link();
                        wp_editor( $this->value(), $this->id, $settings );
                    ?>
                </label>
                <?php
                    // Enqueue editor scripts
                    _WP_Editors::editor_js();
                                              
                    if ( did_action( 'admin_print_footer_scripts' ) == 0 ) {
                        do_action( 'admin_print_footer_scripts' );
                    }                             
            }
            
            // Add textarea identificator
            private function filter_editor_setting_link() {
                add_filter( 'the_editor', function( $output ) { 
                    return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 );
                } );
            }
        }
    }
