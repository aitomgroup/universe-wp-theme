<?php

/**
 * theme-video-gallery.php
 *
 * - Modify gallery 'link to' options
 * - Add new video url field to attachment
 */

    /*----------------------------------------------------------------------------*
     * MODIFY GALLERY 'LINK TO' OPTIONS
     *----------------------------------------------------------------------------*/

    add_action( 'print_media_templates', function() { ?>

        <script type="text/javascript">
            jQuery( document ).ready( function() {
                _.extend( wp.media.gallery.defaults, {
                    link: 'imageVideo', // Set default 'Link to' options to 'imageVideo' option. Can be also be 'none', 'file'
                } );

                wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend( {
                    template: function( view ) {
                        
                        // Replace post link to add custom link
                        var replace_template = wp.media.template( 'gallery-settings' )( view );

                        replace_template = replace_template.replace(/<label class=\"setting\" *?>[\s\S]*?<\/label>/gi, function replaceFunction(x){
                            var attrs = x.match("select class=\"([A-Za-z0-9\-\ _]*)\"");
                            if ( attrs !== null && typeof attrs[1] !== 'undefined' ) {
                                x = x.replace(/class=\"setting\"/, 'class=\"setting ' + attrs[1] + '\"');
                            }
                            return x;
                        });
                        
                        replace_template = replace_template.replace(/<option value=\"post\" *?>[\s\S]*?<\/option>/, '<option value=\"imageVideo\"><?php esc_html_e( 'Video / picture', LWD_TEXT_DOMAIN ); ?><\/option>');
                        replace_template = replace_template.replace(/<option value=\"file\" *?>[\s\S]*?<\/option>/, '<option value=\"file\"><?php esc_html_e( 'Picture', LWD_TEXT_DOMAIN ); ?><\/option>');
   
                        return replace_template;

                    }
                } );
            } );
        </script>

    <?php } );

    /*----------------------------------------------------------------------------*
     * ADD NEW VIDEO URL FIELD TO ATTACHMENT
     *----------------------------------------------------------------------------*/

    add_filter( 'attachment_fields_to_edit', function( $form_fields, $post = null ) {

        if ( strpos( $post->post_mime_type, 'image') !== false ) {
            $form_fields['video-url'] = array(
                'label' => __( 'Video link', LWD_TEXT_DOMAIN ),
                'input' => 'text',
                'value' => get_post_meta( $post->ID, '_video_url', true ),
                'helps' => __( 'Enter the video URL. Click on image will open the link to Youtube or Vimeo in lightbox.', LWD_TEXT_DOMAIN ),
            );
        }
        
        return $form_fields;
    }, 10, 2 );

    add_filter( 'attachment_fields_to_save', function( $post, $attachment ) {

        if ( isset( $attachment['video-url'] ) )
            update_post_meta( $post['ID'], '_video_url', $attachment['video-url'] );

        return $post;
    }, 10, 2 );

    add_action( 'wp_ajax_save-attachment-compat', function() {

        $post_id = $_POST['id'];

        if( isset( $_POST['attachments'][$post_id]['av-custom-link'] ) )
            update_post_meta( $post_id, 'av-custom-link', $_POST['attachments'][$post_id]['av-custom-link'] );

        clean_post_cache( $post_id );

    }, 0, 1 );
