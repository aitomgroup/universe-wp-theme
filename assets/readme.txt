- nepouzivame zadny soubor z js/libs
- nepouzivame liveform validation v custom.js
- nahrazuji se složky less, images, js
- nemeli by se přepisovat soubory less/style.less a js/scripts.json
- nove styly a js je nutné nacist v less/style.less (-child/assets/css/style.less) a v js/scripts.json (-child/assets/js/scripts.json)
- slozka templates/icons presunuto do images/icons
- dalsi ikony lze vlozit z balicku Octicons (https://octicons.github.com/)

Posledni synchronizace s Universal template (https://bitbucket.org/aitomgroup/universal-template/):
18.5.2017 - commit d91c9b9