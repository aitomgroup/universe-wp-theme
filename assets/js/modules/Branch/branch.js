var GoogleMap = GoogleMap || {};

GoogleMap = function(element) {
	this.element = element;
	this.options = {};
	this.map;

	this.setup();
};

GoogleMap.prototype = {

	constructor: GoogleMap,

	setup: function() {
		this.setOptions();
		return this;
	},

	setOptions: function() {
		this.options.detail = parseInt(this.element.data('detail'));
		this.options.zoom = parseInt(this.element.data('zoom'));
		this.options.lat = parseFloat(this.element.data('lat'));
		this.options.lng = parseFloat(this.element.data('lng'));
		this.options.marker = this.element.data('marker').length > 0 ? this.element.data('marker') : null;
		this.options.url = this.element.data('url');

		return this;
	},

	initialize: function() {
		var base = this;
		var googleMapOptions = {
			center: new google.maps.LatLng(base.options.lat, base.options.lng),
			zoom: base.options.zoom,
			mapTypeControl: true,
			navigationControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		base.map = new google.maps.Map(base.element.get(0), googleMapOptions);

		if (base.options.detail) {
			var point = new google.maps.LatLng(this.options.lat, this.options.lng);
			base.createMarker(point, 1);
		} else {
			base.loadMarkers();
		}
	},

	loadMarkers: function() {
		var base = this;

		$.get(base.options.url, {
			timeStamp: new Date().getTime()
		}, function(json) {
			for (var i = 0, length = json.length; i < length; i++) {
				var data = json[i];
				var latLng = new google.maps.LatLng(data.lat, data.lng);
				base.createMarker(latLng, data.id, data.title, data.info);
			}
		});
	},

	// create google maps marker
	createMarker: function(latLng, id, title, info) {
		var base = this;
		var marker = new google.maps.Marker({
			position: latLng,
			map: base.map,
			icon: base.options.marker,
			id: id,
			title: title
		});

		if (info) {
			var infoWindow = new google.maps.InfoWindow();
			google.maps.event.addListener(marker, 'click', function(e) {
				infoWindow.setContent(info);
				infoWindow.open(base.map, marker);
			});
		}
	}
};

function googleMapInitialize() {
	var map;

	$('.jq_moduleBranch_GoogleMaps').each(function(index, value) {
		map = new GoogleMap($(this));
		map.initialize();
	});
}

function loadGoogleMapsScript() {
	if (php.googleMapsApiKey) {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = '//maps.googleapis.com/maps/api/js?key=' + php.googleMapsApiKey + '&v=3.exp&callback=googleMapInitialize';
		document.head.appendChild(script);
	} else {
		console.warn('Google maps API key not present.');
	}
}


// async call google api only if module exist on page
$(function() {
	if ($('.jq_moduleBranch_GoogleMaps').length) {
		loadGoogleMapsScript();
	}
});
