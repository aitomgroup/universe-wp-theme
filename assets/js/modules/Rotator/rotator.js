/**
* module Rotatator JS
* used in Aitom\Fe\Rotator
* 
* @author Radim Hašek
*/
$(document).ready(function() {
		
	
	$(".jq_owl-carousel").each(function( index ) {				
		
		// load carousel options defined by CMS
		var options = $(this).data('carouselOptions');		
		
		// if you want manualy set or add options, use 
		// options['optinName'] = optionValue;
		
		// init Carousel
		$(this).owlCarousel(options);		
	});

});