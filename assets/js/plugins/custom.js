$( document ).ready(function() {
    
	////////////////////////////////////////////////////////////////////////////
	///// vyjíždění mobilního menu //////
	////////////////////////////////////
	/* menu funguje bez js pomocí input:checked,
	   toto je pouze doplňují js skrývající scrollovací lištu */
	$('#jq_open_menu').click( function(){
		$('html').toggleClass('noScroll');
	});
	
   
	////////////////////////////////////////////////////////////////////////////
	///// vyjíždění mobilního hledání //////
	////////////////////////////////////
	/* menu funguje bez js pomocí input:checked,
	   toto je pouze doplňují javasript*/
	
	// fce skrytí hledání
	function hideSearch(){
		$('.jq_search-status').prop( "checked", false);
	}
	
	// pokud je kliknuto mimo hledání, hledání se zavře
	$(document).click(function (e) {
		if((!$(e.target).closest("#jq_search").length) && !$(e.target).hasClass('jq_search-status')){
			hideSearch();
		}
	})
	
	// pokud se scrolluje, hledání se zavře
	$( window ).scroll(function() {
		if( $('.jq_search-status').is(":checked") ){
			hideSearch();
		} 
	});	
	
	// nastavení Headroom
	$(".g-header").headroom({
		offset : 65,
		tolerance : {
			up : 5,
			down : 0
		},
		classes : {
			// when element is initialised
			initial : "g-header",
			// when scrolling up
			pinned : "g-header--pinned",
			// when scrolling down
			unpinned : "g-header--unpinned",
			// when above offset
			top : "g-header--top",
			// when below offset
			notTop : "g-header--not-top",
			// when at bottom of scoll area
			bottom : "g-header--bottom",
			// when not at bottom of scroll area
			notBottom : "g-header--not-bottom"
		}
	});
	
	
	//Scroll under the element
	$(".js_scroll_under").click(function(event){
		event.preventDefault();
		var bottom =  $(this).parent().offset().top + $(this).parent().outerHeight();
		if(window.matchMedia( "(min-width: 60em)" ).matches){
			bottom -= $(".g-header__bottom").outerHeight()-10;	
		}
		$('html, body').animate({
				scrollTop: bottom
		}, 700);
	});
	
	//Trigger pro filtr
	$('#jq_filter input[type=radio]').change(function() {
         $('.jq_filter-button').trigger('click');
  	});
	
	
	///////////////////////////////////////////////////////////////
	//////////////// Ajax akce////////////////
	//////////////////////////////////////////////////////////////
	$(document).ajaxStart(function() {
		$('.ajax__content').addClass('ajax__content--start');
		$('.ajax__alert').addClass('ajax__alert--start');
	});

	$(document).ajaxSuccess(function() {
		$('.ajax__content').removeClass('ajax__content--start');
		$('.ajax__alert').removeClass('ajax__alert--start');
	});
	
	//Lazysizec
	document.addEventListener('lazybeforeunveil', function(e){
    	var bg = e.target.getAttribute('data-bg');
		if(bg){
			e.target.style.backgroundImage = 'url(' + bg + ')';
		}
	});
	
	//Placeholder
	(function(){
		$(window).scroll(function () {
		   if($('.placeholder').height() <= $(window).height()){
				$('.placeholder__fix').css({
				  'position' : 'fixed',
				  'top' : -($(this).scrollTop()/3)+"px"
				}); 
				$('.placeholder__wrap').css({
				  'top' : -($(this).scrollTop()/2)+"px"
				}); 
		   }else{

				$('.placeholder_fix').css({
					'position' : 'relative',
					'top' : 0
				});
			   $('.placeholder__wrap').css({
				  'top' :0
				});
		   }
	   });
	   $(window).resize(function(){
			$('.placeholder__fix').css({
				'position' : 'relative',	
			});		
	   });
	})();
	
	//Paralaxy
	(function(){
		var config = {
			duration: 1000,
			scale: 0.8,			
			viewOffset: {
				top: 60
			},
			afterReveal: function (domEl) {
				//reset attributes
				domEl.style.transform = "";
				domEl.style.transition = "";
				domEl.style.visibility = "";
				domEl.style.opacity = "";
			}
			//reset: true
		};

		window.sr = ScrollReveal(config);
		
		sr.reveal('.js_loadFade');
		
		sr.reveal('.js_loadFadeDelayed', {
			
		}, 100);
		
		sr.reveal('.js_slideFromTop', {
			origin: 'top',
		});
		
		sr.reveal('.js_slideFromSide:nth-of-type(odd)', {
			origin: 'left', 
			distance: '20rem',
		});
		
		sr.reveal('.js_slideFromSide:nth-of-type(even)', {
			origin: 'right',
			distance: '20rem',
		});
		
		sr.reveal('.js_slideFromLeft', {
			origin: 'left', 
			distance: '20rem',
		});
		
		sr.reveal('.js_slideFromRight', {
			origin: 'right', 
			distance: '20rem',
		});
	

	 })();

	
});