/* 
 * Takova trosku sranda - pomocne metody pro lepsi praci s google mapami.
 * @author Tak ruzne, dohromady dal Ales Zrak.
 */
;(function($, window, document, php, aitomMapUtils, undefined){
	
	var googleIsLoading = false;
	var callbackQueue = [];
	
	var Map = function($element, options){
		
		// public methods and properties
		var self = {
			
			// google maps object
			map: null,
			
			// markers placed on google map - null if no markers
			markers: null,
			
			// sets marker infoWindow dom ready callback
			setInfoWindowDomReady: function(callback){
				defaultOptions.onInfowindowOpen = callback;
			},
			
			// fits map to currently added markers
			fitMapToMarkers: function(){
				var bounds = new google.maps.LatLngBounds();
				$.each(self.markers, function(key, marker){
					bounds.extend(marker.googleMarker.getPosition());
				});
				self.map.fitBounds(bounds);
			},
			
			// opens infoWindow for marker specified with given key
			openInfoWindow: function(markerKey){

				var markerData = self.markers[markerKey];
				
				if(!markerData.infoWindowContent){
					return;
				}
				
				if(infoWindows[markerKey]){
					infoWindows[markerKey].open(self.map, markerData.googleMarker);
					return;
				}
				
				infoWindows[markerKey] = new google.maps.InfoWindow({
					content: '<div id="jq_iw_body_'+markerKey+'">' + markerData.infoWindowContent + '</div>'
				});
					
				infoWindows[markerKey].addListener('domready', function() {
					var $infoWinBody = $('#jq_iw_body_' + markerKey);
					var $iw = $infoWinBody.parents('.gm-style-iw');
					$infoWinBody.addClass('g-map__iw__content');
					$iw.parent().removeClass().addClass(defaultOptions.windowClass).addClass('g-map__iw');
					$iw.next().addClass('g-map__iw__close');
					var $wBackgroundDivsCont = $iw.parent().children('div:first-child');
					$wBackgroundDivsCont.children().each(function(){
						// now we need to identify arrow and arrowShadow - only possible detection is that arrow divs do not have 'border-radius' style and that shadow of arrow does not have child nodes
						var borderRadius = parseInt($(this).css('border-radius'));
						if(!isNaN(borderRadius) && borderRadius > 0){
							// skip elements with border-radius - those are not parts of the arrow
							return true; //continue with next element
						}

						if($(this).children().length > 0){
							// if element has child nodes, it is arrow itself
							$(this).addClass('g-map__iw__arrow');
							return true; //continue with next element
						}

						// if element has none child nodes, it is arrow shadow
						$(this).addClass('g-map__iw__arrow-shadow');
					});

					// run callback if any
					if(markerData.onInfowindowOpen && typeof markerData.onInfowindowOpen == 'function'){
						markerData.infoWindowDomReady($infoWinBody, self.map);
					}else if(defaultOptions.onInfowindowOpen && typeof defaultOptions.onInfowindowOpen == 'function'){
						defaultOptions.onInfowindowOpen($infoWinBody, self.map);
					}
				});
				
				infoWindows[markerKey].open(self.map, markerData.googleMarker);
			},
			
			/**
			 * Closes all infowindows on map.
			 * 
			 * @returns void
			 */
			closeInfoWindows: function(){
				$.each(infoWindows, function(key, window){
					window.close();
				});
			},
			
			/**
			* Adds given markers to googlemap. Accepts array of markers. Possible keys for each marker are:
			*		- lat, lng - this sets the position of the marker
			*		- zoom - map will be zoomed to given level on click on the marker
			*		- title - title which will be displayed when marker is hovered by mouse
			*		- infoWindowContent - html with content of infowindow open on marker click
			*		- infoWindowClass - class will be appended to infoWindow main container
			*		- onInfowindowOpen - function that will be triggered when infoWindow is open As an argument, root jquery element of an infoWindow will be passed.
			*		- icon - full url to marker icon
			*
			* @param Map map
			* @param {type} markers
			* @returns {Array}
			*/
			addMarkers: function(markers){

				var markerIndex = 0;
				$.each(markers, function(key, markerData){

					if(!markerData.lat || !markerData.lng){
						if(window.console){
							window.console.warn('Marker skipped - must have lat, lng and infoWindowContent set.');
						}
						return true; // continue with next marker
					}

					var markerOptions = {
						position: {lat: markerData.lat, lng: markerData.lng},
						map: self.map
					};
					if(markerData.title){
						markerOptions.title = markerData.title;
					}
					if(markerData.icon){
						markerOptions.icon = markerData.icon;
					}

					var googleMarker = new google.maps.Marker(markerOptions);
					googleMarker.markerIndex = markerIndex;
					googleMarker.addListener('click', function() {
						self.map.panTo(this.getPosition());
						if(markerData.zoom){
							self.map.setZoom(markerData.zoom);
						}
						
						self.openInfoWindow(this.markerIndex);
					});

					if(self.markers === null){
						self.markers = [];
					}
					markerData.googleMarker = googleMarker;
					self.markers.push(markerData);
					markerIndex++;
				});
			},
			
			/**
			* Vykresli na mapu vlastni ovladaci prvek - input pro vyhledavani mezi markery na mape.
			* Vysledny input bude mit tridu mapMarkerSearchInput.
			* 
			* Options:
			*		 - placeholder - zastupny text ve vyhledavacim poli.
			*		 - callback - funkce, ktera se zavola po vyhledani nove lokace pres vyhledavaci pole, jako argument obrdzi mapu a konkretni misto
			*		 - position - pozice na mape dle https://developers.google.com/maps/documentation/javascript/3.exp/reference#ControlPosition
			*					- priklad: google.maps.ControlPosition.LEFT_TOP - to je take vychozi hodnota
			*		 - zoom - zoom mapy při vyhledání adresy
			*		 - supressInfoWindow - při vyhledání markeru neotevře jeho info okno
			* 
			* @param array options
			* @returns void
			*/
			applyOwnMarkerSearchField: function(options){

				var placeholderText = (options && options.placeholder) ? options.placeholder : 'Vyhledat';
				var callback = (options && typeof options.callback == 'function') ? options.callback : null;
				var zoom = (options && options.zoom) ? options.zoom : 10;
				var position = (options && options.position) ? options.position : google.maps.ControlPosition.LEFT_TOP;

				var $searchContainer = $('<div class="g-map__search jq_MarkerSearchContainer"></div>');
				var $searchInput = $('<input class="g-map__search__input" type="text" name="searchMarker" placeholder="'+placeholderText+'" />');
				var $autocompleteCont = $('<div class="g-map__search__ac jq_autocompleteCont" style="display:none;"></div>');
				var $autocompleteItem = $('<div class="g-map__search__ac__i"></div>');

				$searchContainer.append($searchInput);
				$searchContainer.append($autocompleteCont);

				self.map.controls[position].push($searchContainer.get(0));
				
				$searchInput.click(function(){
					var searchVal = $(this).val();
					if(searchVal.length > 2){
						$autocompleteCont.show();
					}
				});

				$searchInput.keyup(function(){

					$autocompleteCont.children().remove();

					var searchVal = $(this).val();
					if(searchVal.length > 2){
						var matches = [];
						$.each(self.markers, function(key, item){
							if(item.title.toLowerCase().indexOf(searchVal.toLowerCase()) === 0){
								// match! display in autocomplete
								var $item = $autocompleteItem.clone();
								$item.attr('data-marker', key);
								$item.html('<strong>' + item.title.substr(0, searchVal.length) + '</strong>' + item.title.substr(searchVal.length));
								$item.click(function(){
									var markerKey = $(this).data('marker');
									var targetMarker = self.markers[markerKey];

									var markerPosition = targetMarker.googleMarker.getPosition();
									self.map.panTo(markerPosition);
									self.map.setZoom(zoom);

									if(!options || !options.supressInfoWindow){
										self.openInfoWindow(markerKey);
									}

									if(callback){
										callback.apply(self, targetMarker);
									}
									
									$autocompleteCont.hide();
								});

								matches.push($item);
							}
						});

						if(matches.length > 0){
							$.each(matches, function(key, $item){
								$autocompleteCont.append($item);
							});
							$autocompleteCont.show();
						}else{
							$autocompleteCont.hide();
						}
					}else{
						$autocompleteCont.hide();
					}
				});
			}
		};
		
		// protected methods and properties
		
		var defaultOptions = {};
		var infoWindows = {};
		
		function initMap($element, options){
			
			$element.addClass('hasMap g-map');
			
			// first parse map options from data attributes
			var lat = $element.data('lat'); // these two are to center the map
			var lng = $element.data('lng') ;
			var zoom = parseInt($element.data('zoom')); // default zoom of the map
			var infoWindowClass = $element.data('windowClass');
			var title = $element.data('title');
			var icon = $element.data('icon');
			var icosmall = $element.data('iconsmall');
			var ownGui = $element.data('ownGui') ? true : false;
			var noGui = $element.data('noGui') ? true : false;
			var fitMarkers = $element.data('fitmarkers') ? true : false;
			
			// now parse our gui options
			var zoomPosition = $element.data('zoomPosition');
			var zoomOptions = {
				plusText: $element.data('zoomPlustext'),
				minusText: $element.data('zoomMinustext'),
				position: google.maps.ControlPosition[zoomPosition] ? google.maps.ControlPosition[zoomPosition] : 0
			};
			var searchPosition = $element.data('searchPosition');
			var searchOptions = {
				placeholder: $element.data('searchPlaceholder'),
				position: google.maps.ControlPosition[searchPosition] ? google.maps.ControlPosition[searchPosition] : 0,
				zoom: $element.data('searchZoom'),
				supressInfoWindow: $element.data('searchSuppressInfo')
			};
			
			defaultOptions = {
				windowClass: infoWindowClass,
				markerIcon: icon,
				markerSmallIcon: icosmall,
				markerZoom: zoom,
				markerTitle: title,
				onInfowindowOpen: null
			};
			
			// options for google map to be created
			var googleMapOptions = {
				disableDefaultUI: ownGui || noGui,
				zoom: isNaN(zoom) ? 12: zoom,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			if(!isNaN(lat) && !isNaN(lng)){
				googleMapOptions.center = new google.maps.LatLng(lat, lng);
			}else if(options.center){
				// nothing - center will be set by extending the object lower
				
			}else if(fitMarkers){
				// nothing - map will be set up later by self.fitMapToMarkers
			
			}else{
				if(window.console){
					window.console.warn('No center point is set for google map. The map could be displayed incorrectly or not at all.');
				}
				return false; // continue with next map
			}
			
			// merge parsed options with provided google map options
			$.extend(googleMapOptions, options);
			
			// get markers
			var markers = aitomMapUtils.parseMarkers($element, defaultOptions);
			
			// create the googlemap
			self.map = new google.maps.Map($element.get(0), googleMapOptions);
			
			// parse options from data attributes
			self.addMarkers(markers);
			
			if(!noGui && ownGui){
				aitomMapUtils.applyOwnZoomButtons(self.map, zoomOptions);
				self.applyOwnMarkerSearchField(searchOptions);
			}
			
			if(fitMarkers){
				self.fitMapToMarkers();
			}
		}
		
		initMap($element, options);
		return self;
	};
	
	
	/**
	 * Inits given cusom map element. First you need to call aitomMapUtils.loadGoogleMapsScript with your callback.
	 * Returns Map object.
	 * 
	 * @param jQuery $map - element to turn into a map
	 * 
	 * @returns Map
	 */
	aitomMapUtils.initMap = function($map, googleMapOptions){
		
		if (typeof google !== 'object' || typeof google.maps !== 'object'){
			if(window.console){
				window.console.warn('Google maps are not initialized. You need to run aitomMapUtils.loadGoogleMapsScript first.');
			}
			return;
		}
		
		var ourMap = new Map($map, googleMapOptions);
		$map.data('gmapData', ourMap);
		return ourMap;
	};
	
	/**
	 * Vsechny elementy, ktere maji classu jq_autoGMap se nahradi mapou a pouziji se parametry definovane v jejich datovych atributech. 
	 * V nich uvedeni potomci s tridou jq_mapMarker budou take prevedeni na markery.
	 * Podporovane datove atributy pro celou mapu:
	 *  - <div class="jq_autoGMap"
	 *		data-icon=""
	 *		data-iconsmall=""
	 *		data-zoom=""
	 *		data-lat=""
	 *		data-lng=""
	 *		data-title=""
	 *		data-window-class="customClass"
	 *		data-own-gui=""
	 *		data-no-gui=""
	 *		data-fitmarkers=""
	 *	> ... makery nebo prazdny tag ... </div>
	 * 
	 * @internal
	 * @returns void
	 */
	aitomMapUtils.displayMaps = function(){

		$('.jq_autoGMap:not(.hasMap)').each(function(){
			var ourMap = new Map($(this), {});
			$(this).data('gmapData', ourMap);
		});
		
		$(document).trigger('gmaps_loaded');
	};
	
	/**
	 * Zavolane po nacteni google map. Zavola vsechny prirazene callbacky. Interni metoda.
	 * 
	 * @internal
	 * @returns void
	 */
	aitomMapUtils.googleLoaded = function(){
		$.each(callbackQueue, function(key, callback){
			executeFunctionByName(callback);
		});
		
		googleIsLoading = false;
		callbackQueue = [];
	};
	
	
	/**
	 * Loads google javascript and fires callback.
	 * Pokud je na strance nejaka automaticky zobrazovana mapa (s tridou jq_autoGMap), pripoji google maps script a zavola aitomMapUtils.displayMaps
	 * 
	 * @param string callback
	 * @returns void
	 */
	aitomMapUtils.loadGoogleMapsScript = function(callback) {

		// callback has to be string
		if(typeof callback !== 'string'){
			if(window.console){
				window.console.warn('aitomMapUtils.loadGoogleMapsScript - callback has to be string');
			}
		}

		// check if script is already loaded - if so, just call the callback
		if (typeof google === 'object' && typeof google.maps === 'object'){
			executeFunctionByName(callback);
			return;
		}
		
		// if script is (or will be) loading, place current callback to queue
		callbackQueue.push(callback);
		
		if(googleIsLoading){
			return;
		}
		
		if (php.googleMapsApiKey) {
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = '//maps.googleapis.com/maps/api/js?key=' + php.googleMapsApiKey + '&v=3.exp&libraries=places&callback=aitomMapUtils.googleLoaded';
			document.head.appendChild(script);
			
			googleIsLoading = true;
			
		} else if (window.console){
			window.console.warn('Google maps API key not present.');
		}
	};
	
	/**
	 * Parsuje markery ze zadane mapy. Kazdy marker by mel byt uveden jako div.
	 * Vraci pole markeru ktere jde rovnou pouzit pro konfiguracni volbu 
	 * @todo write doc to wiki
	 * 
	 * <span class="jq_mapMarker"
	 *		data-icon=""
	 *		data-iconsmall=""
	 *		data-zoom=""
	 *		data-lat=""
	 *		data-lng=""
	 *		data-title=""
	 *		data-window-class="customClass"
	 *	>Marker content html</span>
	 *	
	 *	Automaticky priradi nasledujici dalsi tridy pro infowindow:
	 *		- closeButton - zaviraci krizek pro infoWindow
	 *		- arrow - sipka smerujici od infoWindow k markeru
	 *		- arrowShadow - stin sipky smerujici od infoWindow k markeru
	 *		- infoWindow - cele infowindow - jeho kontejner
	 *		- infoWindowContent - div s obsahem okna
	 * 
	 * @param jQueryObject $mapDiv
	 * @param map map Map object
	 * @param object options Objekt s nastavenim generovani markeru. Moznosti:
	 *				- windowClass - Jedna nebo vic trid, ktere se priradi kazdemu infoWindow, oddeleno mezerou
	 *				- markerIcon - Cesta k ikone markeru - pokud bude uvedena, nastavi se vsem markerum, mozne pretizit datovym atributem
	 *				- markerSmallIcon - Cesta k ikone markeru - pokud bude uvedena, nastavi se vsem markerum, mozne pretizit datovym atributem. Pouzije se pro obrazovky mensi nez 900 px
	 *				- markerZoom - Zoom pri kliknuti na marker - pokud je uveden, nastavi se pro vsechny markery, mozne pretizit datovym atributem
	 *				- markerTitle - Title pro markery - pokud je nastaven, nastavi se pro vsechny markery
	 *	
	 * @returns array
	 */
	aitomMapUtils.parseMarkers = function($mapDiv, options){
		
		var markersArray = [];
		$mapDiv.find('.jq_mapMarker').each(function(key){

			var markerData = {};

			markerData.lat = parseFloat($(this).data('lat'));
			markerData.lng = parseFloat($(this).data('lng'));
			markerData.zoom = parseInt($(this).data('zoom'));
			markerData.title = $(this).data('title');
			markerData.id = $(this).data('id');
			markerData.infoWindowClass = $(this).data('windowClass');
			markerData.infoWindowContent = $(this).html();
			markerData.icon = $(this).data('icon');
			var icosmall = $(this).data('iconsmall');
			
			if(markerData.icon){
				if( icosmall && $(window).width() <= 900 ){
					markerData.icon = icosmall;
				}
			}else if(options && options.markerIcon){
				if( $(window).width() > 900 && options && options.markerSmallIcon){
					markerData.icon = options.markerSmallIcon;
				}else{
					markerData.icon = options.markerIcon;
				}
			}
			
			if(!markerData.infoWindowClass && options && options.windowClass){
				markerData.infoWindowClass += ' ' + options.windowClass;
			}
			if(!markerData.zoom && options && options.markerZoom){
				markerData.zoom = options.markerZoom;
			}
			if(!markerData.title && options && options.markerTitle){
				markerData.title = options.markerTitle;
			}

			markersArray.push(markerData);
		});
		
		return markersArray;
	};

	/**
	 * Vykresli na mapu vlastni zoom buttons. Zaroven zakaze vykresleni defaultnich zoomovacich prvku.
	 * 
	 * Tlacitka maji nasledujici tridy:
	 *		- kontejner: zoom
	 *		- plus tlacitko: zoomplus
	 *		- minus tlacitko: zoomminus
	 * 
	 * Options:
	 *		- plusText - text plusoveho tlacitka
	 *		- minusText - text minusoveho tlacitka
	 *		- position - pozice na mape dle https://developers.google.com/maps/documentation/javascript/3.exp/reference#ControlPosition
	 *					- priklad: google.maps.ControlPosition.LEFT_TOP - to je take vychozi hodnota
	 *		- callback - metoda, ktera se zavola pri kliknuti na obe tlacitka (pak je mozne detekovat pomoci $button.hasClass(), ktere tlacitko to bylo, $button je argument, ktery callback dostane jako prvni)
	 * 
	 * @param googleMapObject map
	 * @param array options
	 * @returns void
	 */
	aitomMapUtils.applyOwnZoomButtons = function(map, options){ 
		
		var plusText = (options && options.plusText) ? options.plusText : '+';
		var minusText = (options && options.minusText) ? options.minusText : '-';
		var position = (options && options.position) ? options.position: google.maps.ControlPosition.LEFT_TOP;
		
		var $zoomContainer = $('<div class="g-map_zoom"><a href="#" class="g-map__zoom__i--p jq_ZoomPlus">'+plusText+'</a><a href="#" class="g-map__zoom__i--m jq_ZoomMinus">'+minusText+'</a></div>');
		$zoomContainer.find('.jq_ZoomPlus').click(function(){
			if(options && typeof options.callback == 'function'){
				options.callback($(this));
			}
			map.setZoom(map.getZoom() + 1);
			return false;
		});
		$zoomContainer.find('.jq_ZoomMinus').click(function(){
			if(options && typeof options.callback == 'function'){
				options.callback($(this));
			}
			map.setZoom(map.getZoom() - 1);
			return false;
		});
		
		map.controls[position].push($zoomContainer.get(0));
		map.setOptions({
			zoomControl: false
		});
	};
	
	/**
	 * Vykresli na mapu vlastni ovladaci prvek - input pro vyhledavani mist na mape. Toto nehleda mezi markery!
	 * Vysledny input bude mit tridu mapSearchInput.
	 * 
	 * Options:
	 *		 - placeholder - zastupny text ve vyhledavacim poli.
	 *		 - callback - funkce, ktera se zavola po vyhledani nove lokace pres vyhledavaci pole, jako argument obrdzi mapu a konkretni misto
	 *		 - position - pozice na mape dle https://developers.google.com/maps/documentation/javascript/3.exp/reference#ControlPosition
	 *					- priklad: google.maps.ControlPosition.LEFT_TOP - to je take vychozi hodnota
	 *		 - zoom - zoom mapy při vyhledání adresy
	 * 
	 * @param googleMapObject map
	 * @param array options
	 * @returns void
	 */
	aitomMapUtils.applyOwnPlaceSearchField = function(map, options){
		
		var placeholderText = (options && options.placeholder) ? options.placeholder : 'Vyhledat';
		var callback = (options && typeof options.callback == 'function') ? options.callback : null;
		var zoom = (options && options.zoom) ? options.zoom : 10;
		var position = (options && options.position) ? options.position : google.maps.ControlPosition.LEFT_TOP;
		
		var $searchContainer = $('<input class="g-map__search__input" type="text" name="searchPlace" placeholder="'+placeholderText+'" />');
		var googleSearchBox = new google.maps.places.SearchBox($searchContainer.get(0));
		map.controls[position].push($searchContainer.get(0));
		
		googleSearchBox.addListener('places_changed', function() {
			var places = googleSearchBox.getPlaces();
			if (places.length == 0) {
				return;
			}
			
			var bounds = new google.maps.LatLngBounds();
			places.forEach(function(place) {
				if (!place.geometry){
					if(window.console){
						window.console.warn("Returned place contains no geometry");
					}
					return;
				}
				
				if (place.geometry.viewport) {
					// Only geocodes have viewport.
					bounds.union(place.geometry.viewport);
				} else {
					bounds.extend(place.geometry.location);
				}
				map.setZoom(zoom);
				map.fitBounds(bounds);
				
				if(callback){
					callback.apply(map, place);
				}
			});
		});
	};
	
	/**
	 * Vykresli na mapu vlastni ovladaci prvek - input pro vyhledavani mezi markery na mape.
	 * Vysledny input bude mit tridu mapMarkerSearchInput.
	 * 
	 * Options:
	 *		 - placeholder - zastupny text ve vyhledavacim poli.
	 *		 - callback - funkce, ktera se zavola po vyhledani nove lokace pres vyhledavaci pole, jako argument obrdzi mapu a konkretni misto
	 *		 - position - pozice na mape dle https://developers.google.com/maps/documentation/javascript/3.exp/reference#ControlPosition
	 *					- priklad: google.maps.ControlPosition.LEFT_TOP - to je take vychozi hodnota
	 *		 - zoom - zoom mapy při vyhledání adresy
	 *		 - supressInfoWindow - při vyhledání markeru neotevře jeho info okno
	 * 
	 * @param Map map
	 * @param array options
	 * @returns void
	 */
	aitomMapUtils.applyOwnMarkerSearchField = function(map, options){
		map.applyOwnMarkerSearchField(options);
	};
	
	/**
	 * Nastyluje mapu.
	 * Jako parametr styleOptions muzete pridat cely objekt, ktery najdete u tematu napr. na https://snazzymaps.com/
	 * 
	 * @param googlemap map
	 * @param {type} styleOptions
	 * @returns void
	 */
	aitomMapUtils.styleMap = function(map, styleOptions){
		
		var styledMapType = new google.maps.StyledMapType(styleOptions, {name: 'Styled'});
		map.mapTypes.set('styled_map', styledMapType);
		map.setMapTypeId('styled_map');
	};
	
	var loadAutoMaps = function(){
		var mapCount = $('.jq_autoGMap:not(.hasMap)').length;
		if(mapCount > 0){
			aitomMapUtils.loadGoogleMapsScript('aitomMapUtils.displayMaps');
		}
	};
	
	var initMapAnchors = function(){
		var $anchors = $('.jq_gmapMarkerAnchor:not(.set)');
		$anchors.each(function(key, anchor){
			var $anchor = $(anchor);
			$anchor.addClass('set');
			
			$anchor.click(function(){
				// first element is map id, second is marker id
				var target = $anchor.data('for').split(':');
				if(target.length < 2){
					if(window.console){
						window.console.warn('Bad target format. Correct is mapId:markerId');
					}
					return false;
				}
				
				// options
				var zoom = $anchor.data('zoom');
				var suppressInfoWindow = $anchor.data('suppressInfo');
				
				var $map = $('div[data-id='+target[0]+']');
				if($map.length < 1){
					if(window.console){
						window.console.warn('Map with id ' + target[0] + ' was not found.');
					}
					return false;
				}
				
				var localMap = $map.aitGmap('getMap');
				$.each(localMap.markers, function(markerKey, marker){
					if(marker.id == target[1]){
						var markerPosition = marker.googleMarker.getPosition();
						localMap.map.panTo(markerPosition);
						localMap.map.setZoom(zoom ? zoom : 12);
						localMap.closeInfoWindows();

						if(!suppressInfoWindow){
							localMap.openInfoWindow(markerKey);
						}
					}
				});
				
				return false;
			});
		});
	};
	
	var executeFunctionByName = function(stringName){
		var arr = stringName.split('.');
		var fn = window;
		
		$.each(arr, function(key, namePart){
			if(fn[namePart]){
				fn = fn[namePart];
			}else if(window.console){
				return false; // break
			}
		});
		
		if(typeof fn === 'function'){
			fn.apply(window);
		}else if(window.console){
			window.console.warn('Callback ' + stringName + ' does not exist or it is not a function.');
		}
		
	};
	
	// jquery plugin to provide jquery access to object
	$.fn.aitGmap = function(action, options){
		
		var singleResult = null;
		
		var multipleResult = this.each(function(){
			var ourMap = $(this).data('gmapData');
			if(!ourMap && console){
				console.warn('Map is not ready yet. Did you bind your script to "gmaps_loaded" event?');
				return true; // continue with next map
			}
	
			if(action == 'setInfoWindowDomReady'){
				ourMap.setInfoWindowDomReady(options);
				
			}else if(action == 'setOwnZoomGui'){
				aitomMapUtils.applyOwnZoomButtons(ourMap.map, options); 
				
			}else if(action == 'setOwnSearchGui'){
				ourMap.applyOwnMarkerSearchField(options);
				
			}else if(action == 'setGoogleMapOptions'){
				ourMap.map.setOptions(options);
				
			}else if(action == 'setStyle'){
				aitomMapUtils.styleMap(ourMap.map, options);
			
			}else if(action == 'fitMapToMarkers'){
				ourMap.fitMapToMarkers();
				
			}else if(action == 'closeInfoWindows'){
				ourMap.closeInfoWindows();
				
			}else if(action == 'getMap'){
				singleResult = ourMap;
				return false; // do not continue
			}else if(window.console){
				window.console.warn('Unsupported action.');
			}
		});
		
		return singleResult ? singleResult : multipleResult;
	};
	
	$(document).ready(function(){
		loadAutoMaps();
		initMapAnchors();
	});
	if($.nette){
		$.nette.ext('aitom5-googlemaps', {
			success: function(payload){
				loadAutoMaps();
				initMapAnchors();
			}
		});
	}
	
	

})(jQuery, window, document, php, window.aitomMapUtils = window.aitomMapUtils || {});
