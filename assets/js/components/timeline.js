/*
* Zavísí na pluginu jquery.viewport.min.js 
*/

$( document ).ready(function() {
	$(".js_timeline").each(function(){
		$(this).addClass("timeline__i__hidden");
	});
	
	$(".js_timeline:in-viewport").each(function(){
		$(this).removeClass("timeline__i__hidden");
		$(this).addClass("timeline__i__bounce");
	});	
});

$(document).scroll(function(){
	$(".js_timeline:in-viewport").each(function(){
		$(this).removeClass("timeline__i__hidden");
		$(this).addClass("timeline__i__bounce");
	});	
});