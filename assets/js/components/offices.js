$(document).on('gmaps_loaded', function(){
	 //pozice vyhledávače na základě nastavení layoutu
	 var positionSearch = google.maps.ControlPosition.TOP_LEFT;
	 if($('.offices').hasClass('offices--left')){
		positionSearch = 	google.maps.ControlPosition.TOP_RIGHT; 
	 }
	
     $('.offices__map').aitGmap('setOwnZoomGui', {
			position: google.maps.ControlPosition.RIGHT_BOTTOM
       }).aitGmap('setOwnSearchGui', {
		    position: positionSearch,
		  	placeholder: "Vyhledejte pobočku"
	  });
	/*
	var map = $('.offices__map').aitGmap('getMap');
	//Prováže markery z mapy s výpisem poboček
	map.markers.forEach(function(marker, index){
		var item = $( ".offices__i:eq("+index+")");
		item.click(function(event){
			event.preventDefault();
			infoWindow = map.openInfoWindow(index);
			map.map.setZoom(15);
    		map.map.setCenter({lat:marker.lat, lng:marker.lng});
		});
	})
	*/
});

//For up and down position
$(document).ready(function() {
  $(".offices--up .offices__items, .offices--down  .offices__items").owlCarousel({
      itemsCustom : [
        [0, 1],
        [600, 2],
        [900, 3],
      ],
      navigation : true,
	  pagination: false
  });
});