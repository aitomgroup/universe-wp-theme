/*
* Zavísí na pluginech jquery.viewport.min.js a countUp-1.7.1.js
*/
var options = {
  useEasing : true, 
  useGrouping : true, 
  separator : ' ', //oddělovač mezi tisíci
  decimal : '.', // oddělovač desetiných čísel
  prefix : '', // předpona
  suffix : '' //přípona - např když chci počítat pro roky
};

$( document ).ready(function() {
	$(".js_countUp").each(function(){
		
		var id = $(this).attr("id");
		//konverze na Float kdyby si někdo chtěl nastavit desetiná čísla
		var value = parseFloat($(this).text());

		// selecetor, od, do, kolik desetiných míst, čas, nastavení
		var counter = new CountUp(id, 0, value, 0, 3, options);
		var start = false;
		
		$(this).bind('counter',function(start) {
			if(start) counter.start();			
		});
	});	
	
	$(".js_countUp:in-viewport").each(function(){
		var self = this;
		setTimeout(function(){
			$(self).trigger('counter', [true]);	
		}, 500);
	});	
});

$(document).scroll(function(){
	$(".js_countUp:in-viewport").each(function(){
		var self = this;
		setTimeout(function(){
			$(self).trigger('counter', [true]);	
		}, 500);
	});	
});