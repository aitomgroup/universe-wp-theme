/*
 *
 * Přidává podporu zavření aktuální položky při volbě akordeonu, 
 * který má aktivní maximálně jen jednu položku
 *
*/

$( document ).ready(function() {
	$('.js-accordion').click(function(event){
		var radio = $(this).parent().prev();
		if (radio.is( ":checked" ) == true){
			 event.preventDefault();
			 radio.prop('checked',false)	
		}
	});
});