var $ = jQuery.noConflict();

jQuery(function($) {
    
	if (typeof ($.magnificPopup) !== 'undefined') {
        
        var $turnOffwidth = 80; // pokud je obrázek šírší než n procent obrazovky, tak lightbox nevyskočí

        // pokud je obrázek moc velký, tak přida do body data-lightbox="false"
        $('.lightbox').click(function() {
            var widthWindow = $(window).width();
            var widtimg = $(this).width();

            if ((widthWindow * ($turnOffwidth / 100)) < widtimg) {
                $('body').data('lightbox', false);
                return false;
            } else {
                $('body').data('lightbox', true);
            }

        });


        $('.lightbox, .js-lightbox').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            },
            removalDelay: 500, //allow out-animation
            disableOn: function() {
                //pokud je body data-lightbox="false"
                if (($('body').data('lightbox')) == false) {
                    return false;
                }
                return true;
            },
            callbacks: {
                // add css3 animation classes
                beforeOpen: function() {


                    // hlídá velikost obrázku v lightboxu
                    $(".lightbox").each(function() {

                        if (!$(this).hasClass('noResponsive')) {

                            var $windowWidth = $(window).width(); // šířka okna
                            var $widthRecount = 1000; // od jaké šířky směrem dolů se bude kontrolovat velikost obrázků
                            var $wDef = $(this).data('wDef'); // původní url z atributu href
                            var c = $widthRecount;

                            if ($wDef) {
                                $(this).attr('href', $wDef); // vrácení do atributu href výchozí url
                            } else {
                                $(this).data('wDef', ($(this).attr('href'))); // href přeuloží do data-wDef
                            }

                            // cyklus po stovkách směrem dolů
                            while (c >= $windowWidth) {
                                var $attrW = $(this).data('w' + c); // dynamický název data atributu sdata uloženou url (w300,w400,w500...)
                                var $loadData = $(this).data('loadData'); // název data atrinutu největšího už staženého obrázku
                                var $newHref; // nová adresa obrázku
                                var $saveW; // uložení názvu použitého data atributu

                                // pokud už se využil obrázek z href, tak se použije znovu
                                if ($loadData && ($loadData == 'usingHref')) {
                                    break;
                                }
                                // pokud se už použil nějaký větší obrázek, použije se
                                else if ($loadData && (('w' + c) == $loadData)) {
                                    console.log('w' + c + '====' + $loadData);
                                    $newHref = $attrW;
                                    $saveW = 'w' + c;
                                    break;
                                }
                                // pokud to najde vhodnou velikost, uloží ji do proměnných
                                else if ($attrW) {
                                    $newHref = $attrW;
                                    $saveW = 'w' + c;
                                }
                                c -= 100;
                            }

                            // pokud se našel nějaký obrázek, aplikuje se
                            if ($saveW) {
                                $(this).attr('href', $newHref); // změna cílové url obrázku
                                $(this).data('loadData', $saveW); // uloží použitou velikost
                            } else {
                                // označí, pokud se použila výchozí url z href
                                $(this).data('loadData', 'usingHref');
                            }
                        }
                    });

                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = 'mfp-zoom-in';
                }
            }
        });

        $('.youtube, .js-lightbox-video').magnificPopup({
            //disableOn: 400, // přesměrování na youtube pokud je oknu užší než
            //disableOn: 700,
            type: 'iframe',
            removalDelay: 500, //allow out-animation
            preloader: false,
            fixedContentPos: false,
            iframe: {
                markup: '<div class="mfp-iframe-scaler">' +
                        '<div class="mfp-close"></div>' +
                        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                        '<div class="mfp-bottom-bar">' +
                        '<div class="mfp-title"></div>' +
                        '</div>' +
                        '</div>'
            },
            callbacks: {
                // add css3 animation classes
                beforeOpen: function() {
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = 'mfp-zoom-in';
                },
                markupParse: function(template, values, item) {
                    var title = '';
                    if (item.el.attr('title') !== undefined && item.el.attr('title').length > 0) {
                        title += item.el.attr('title');
                    }
                    if (item.el.attr('data-desc') !== undefined && item.el.attr('data-desc').length > 0) {
                        title += '<small>' + item.el.attr('data-desc') + '</small>';
                    }

                    if ( title != '' ) values.title = title;
                }
            }
        });
        
	}
    
	// add target attribute to all external links
	$('a.external').attr('target', '_blank');
    
    // Player with cover image
    if ( $('.js-video').length > 0 ) {
        $('.js-video').each(function(){
            var $imageOverlay = $(this).find( '.elementor-custom-embed-image-overlay' ),
                $videoFrame = $(this).find( 'iframe' );

            if ( ! $imageOverlay.length ) {
                return;
            }
            
            $imageOverlay.on( 'click', function() {
                $imageOverlay.remove();
                var newSourceUrl = $videoFrame[0].src;
                // Remove old autoplay if exists
                newSourceUrl = newSourceUrl.replace( '&autoplay=0', '' );

                $videoFrame[0].src = newSourceUrl + '&autoplay=1';
            } );
        });
    }

});

/* `Check if go to anchor after load
----------------------------------------------------------------------------------------------------*/

function checkAnchorOnLoad() {
	"use strict";
	
	var hash = window.location.hash.replace('#', ''),
		scrollOffset = jQuery('.g-header').height() + 20;

    if ( hash !== '' && jQuery('[data-anchor="' + hash + '"]').length > 0 ) {
        jQuery('html, body').animate({
            scrollTop: jQuery('[data-anchor="' + hash + '"]').offset().top - scrollOffset
        }, 1e3);
    }
}

jQuery(window).load(checkAnchorOnLoad);

/* `Go to anchor
----------------------------------------------------------------------------------------------------*/

function initHashClick() {
	"use strict";
	
	jQuery(document).on('click', '.jq_menu a, .js-go-to-anchor, .g-btn', function() {
        var hash = jQuery(this).prop('hash');

        if ( jQuery(this).attr('href') !== '' && jQuery(this).attr('href') !== undefined ) {
            if ( ( hash !== '' && hash !== 'undefined' && jQuery(this).attr('href').split('#')[0] === '' ) || ( hash !== '' && jQuery(this).attr('href').split('#')[0] !== '' && hash === window.location.hash ) || ( jQuery(this).attr('href').split('#')[0] == window.location.href.split('#')[0] ) ) {
                var scrollOffset = jQuery('.g-header').height() + 20,
                    hash = hash.replace('#', '');

                if ( jQuery('[data-anchor="' + hash + '"], #' + hash).length > 0 ) {
                    jQuery('html, body').animate({
                        scrollTop: jQuery('[data-anchor="' + hash+'"], #' + hash).offset().top - scrollOffset
                    }, 1e3);
                }
                if ( history.pushState ) {
                    history.pushState(null, null, '#' + hash);
                }

                return false;
            }
        }
	});
}

jQuery(document).ready(initHashClick);