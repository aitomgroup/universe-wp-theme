<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Element_Custom_Section extends Element_Base {

	protected static $_edit_tools;

	private static $presets = [];

	protected static function get_default_edit_tools() {
		return [
			'duplicate' => [
				'title' => __( 'Duplicate', LWD_ELEM_DOMAIN ),
				'icon' => 'files-o',
			],
			'save' => [
				'title' => __( 'Save', LWD_ELEM_DOMAIN ),
				'icon' => 'floppy-o',
			],
			'remove' => [
				'title' => __( 'Remove', LWD_ELEM_DOMAIN ),
				'icon' => 'times',
			],
		];
	}

	public function get_name() {
		return 'section';
	}

	public function get_title() {
		return __( 'Section', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-columns';
	}

	public static function get_presets( $columns_count = null, $preset_index = null ) {
		if ( ! self::$presets ) {
			self::init_presets();
		}

		$presets = self::$presets;

		if ( null !== $columns_count ) {
			$presets = $presets[ $columns_count ];
		}

		if ( null !== $preset_index ) {
			$presets = $presets[ $preset_index ];
		}

		return $presets;
	}

	public static function init_presets() {
		$additional_presets = [
			2 => [
				[
					'preset' => [ 33, 66 ],
				],
				[
					'preset' => [ 66, 33 ],
				],
			],
			3 => [
				[
					'preset' => [ 25, 25, 50 ],
				],
				[
					'preset' => [ 50, 25, 25 ],
				],
				[
					'preset' => [ 25, 50, 25 ],
				],
				[
					'preset' => [ 16, 66, 16 ],
				],
			],
		];

		foreach ( range( 1, 10 ) as $columns_count ) {
			self::$presets[ $columns_count ] = [
				[
					'preset' => [],
				],
			];

			$preset_unit = floor( 1 / $columns_count * 100 );

			for ( $i = 0; $i < $columns_count; $i++ ) {
				self::$presets[ $columns_count ][0]['preset'][] = $preset_unit;
			}

			if ( ! empty( $additional_presets[ $columns_count ] ) ) {
				self::$presets[ $columns_count ] = array_merge( self::$presets[ $columns_count ], $additional_presets[ $columns_count ] );
			}

			foreach ( self::$presets[ $columns_count ] as $preset_index => & $preset ) {
				$preset['key'] = $columns_count . $preset_index;
			}
		}
	}

	protected function _get_initial_config() {
		$config = parent::_get_initial_config();

		$config['presets'] = self::get_presets();

		return $config;
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_LAYOUT,
			]
		);

		$this->add_control(
			'layout',
			[
				'label' => __( 'Content width', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'default' => 'boxed',
				'options' => [
					'boxed' => __( 'Fixed width', LWD_ELEM_DOMAIN ),
					'full_width' => __( 'Window width', LWD_ELEM_DOMAIN ),
				],
			]
		);
        
        $this->add_control(
			'structure',
			[
				'label' => __( 'Structure', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::STRUCTURE,
				'default' => '10',
				'render_type' => 'none',
			]
		);
        
        $this->add_control(
			'view',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'section',
                'prefix_class' => '',
                'hide_in_inner' => true
			]
		);
        
        $this->end_controls_section();
        
        $this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_LAYOUT,
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Section title', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
                'label_block' => true,
			]
		);
        
        $this->add_control(
			'title_tag',
			[
				'label' => __( 'HTML Tag', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'default' => 'h1',
				'options' => [
					'h1' => 'H1',
                    'h2' => 'H2',
                    'h3' => 'H3',
                    'h4' => 'H4',
                    'h5' => 'H5',
                    'h6' => 'H6'
				],
                'default' => 'h2',
			]
		);
        
        $this->add_control(
			'title_color',
			[
				'label' => __( 'Heading color', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Dark color', LWD_ELEM_DOMAIN ),
					'light' => __( 'Bright color', LWD_ELEM_DOMAIN ),
				],
			]
		);
        
        $this->add_control(
			'title_align',
			[
				'label' => __( 'Text align', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					],
				],
			]
		);
        
        $this->add_control(
			'title_perex',
			[
				'label' => __( 'Perex', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => '',
			]
		);

		$this->end_controls_section();
        
		// Section background
		$this->start_controls_section(
			'section_background',
			[
				'label' => __( 'Background', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'types' => [ 'none', 'classic', 'video' ],
			]
		);

		$this->end_controls_section();

		// Background Overlay
		$this->start_controls_section(
			'background_overlay_section',
			[
				'label' => __( 'Background overlay', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'background_background' => [ 'classic', 'video' ],
				],
			]
		);

		$this->add_control(
			'background_overlay',
			[
				'label' => __( 'Overlay color', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
                'default' => '',
				'selectors' => [
					'{{WRAPPER}} > .elementor-background-overlay' => 'background-color: {{SIZE}};',
				],
				'condition' => [
					'background_background' => [ 'classic', 'video' ],
				],
			]
		);

		$this->add_control(
			'background_overlay_opacity',
			[
				'label' => __( 'Color opacity (%)', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => .5,
				],
				'range' => [
					'px' => [
						'max' => 1,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} > .elementor-background-overlay' => 'opacity: {{SIZE}};',
				],
			]
		);

		$this->end_controls_section();
        
        // Section padding
		$this->start_controls_section(
			'section_padding',
			[
				'label' => __( 'Padding', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
        
        $this->add_control(
			'padding',
			[
				'label' => __( 'Section inner padding', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'default' => 'large',
				'options' => [
					'large' => __( 'High padding', LWD_ELEM_DOMAIN ),
					'middle' => __( 'Medium padding', LWD_ELEM_DOMAIN ),
                    'tight' => __( 'Small padding', LWD_ELEM_DOMAIN ),
				],
                'prefix_class' => 'section--padding-',
			]
		);
        
        $this->end_controls_section();

		// Section Typography
		$this->start_controls_section(
			'section_typo',
			[
				'label' => __( 'Typography', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
        
        $this->add_control(
			'text_color',
			[
				'label' => __( 'Text color', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
                'default' => '',
				'selectors' => [
					'{{WRAPPER}} > .content, {{WRAPPER}} > .content p' => 'color: {{SIZE}};',
				],
			]
		);

		$this->add_control(
			'text_align',
			[
				'label' => __( 'Text align', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					],
				],
                'selectors' => [
					'{{WRAPPER}} > .content' => 'text-align: {{SIZE}};',
				],
			]
		);

		$this->end_controls_section();

		// Section Advanced
		$this->start_controls_section(
			'section_advanced',
			[
				'label' => __( 'Advanced', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);

		$this->add_control(
			'_element_id',
			[
				'label' => __( 'CSS ID', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'label_block' => true,
				'title' => __( 'Add your own ID without a hash # at the beginning and no spaces, for example: my-id', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'css_classes',
			[
				'label' => __( 'CSS classes', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'prefix_class' => '',
				'label_block' => true,
				'title' => __( 'Add your own class without a dot . at the beginning and no spaces, for example: my-class', LWD_ELEM_DOMAIN ),
			]
		);
        
        $this->add_control(
			'anchor',
			[
				'label' => __( 'Anchor', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
                'label_block' => true,
			]
		);

		$this->end_controls_section();

		// Section Responsive
		$this->start_controls_section(
			'_section_responsive',
			[
				'label' => __( 'Responsive', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);

		$this->add_control(
			'hide_desktop',
			[
				'label' => __( 'Hide on computer', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
				'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
				'return_value' => 'hidden-desktop',
			]
		);

		$this->add_control(
			'hide_tablet',
			[
				'label' => __( 'Hide on tablet', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
				'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
				'return_value' => 'hidden-tablet',
			]
		);

		$this->add_control(
			'hide_mobile',
			[
				'label' => __( 'Hide on mobile', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
				'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
				'return_value' => 'hidden-phone',
			]
		);

		$this->end_controls_section();
	}

	protected function _render_settings() { ?>
        <# if ( settings.view && settings.view == 'section' ) { #>
        <div class="elementor-element-overlay">
			<div class="column-title"></div>
			<div class="elementor-editor-element-settings elementor-editor-column-settings" style="bottom: auto; top: -1px; border-radius: 0 0 3px 3px">
				<ul class="elementor-editor-element-settings-list elementor-editor-section-settings-list">
					<li class="elementor-editor-element-setting elementor-editor-element-trigger">
						<a href="#" title="<?php _e( 'Edit section', LWD_ELEM_DOMAIN ); ?>" style="cursor: pointer"><?php _e( 'Section', LWD_ELEM_DOMAIN ); ?></a>
					</li>
				</ul>
			</div>
		</div>
        <# } #>
    <?php }

	protected function _content_template() { ?>
		<# if ( 'video' === settings.background_background ) {
			var videoLink = settings.background_video_link;
	
			if ( videoLink ) {
				var videoID = elementor.helpers.getYoutubeIDFromURL( settings.background_video_link ); #>
	
				<div class="elementor-background-video-container elementor-hidden-phone">
					<# if ( videoID ) { #>
						<div class="elementor-background-video" data-video-id="{{ videoID }}"></div>
					<# } else { #>
						<video class="elementor-background-video" src="{{ videoLink }}" autoplay loop muted></video>
					<# } #>
				</div>
			<# }
	
			if ( settings.background_video_fallback ) { #>
				<div class="elementor-background-video-fallback" style="background-image: url({{ settings.background_video_fallback.url }})"></div>
			<# }
		}

		if ( settings.background_overlay != '' ) { #>
			<div class="elementor-background-overlay"></div>
		<# } 
           
        if ( settings.title != '' ) {
            var title_tag = settings.title_tag != '' ? settings.title_tag : 'h1';
            var title_class = 'title-perex'; 
           
           if ( settings.title_color != '' ) title_class += ' title-perex--' + settings.title_color;
           if ( settings.title_align != '' ) title_class += ' title-perex--' + settings.title_align;
           
           #>
            
            <div class="content">
                <div class="{{ title_class }}"> 	
                        <{{ title_tag }} class="title-perex__title js_loadFade">{{ settings.title }}</{{ title_tag }}>

                        <# if ( settings.title_perex != '' ) { #>
                            <div class="title-perex__perex js_loadFade">{{{ settings.title_perex }}}</div>
                        <# } #>
                </div>
            </div>
        <# } #>
            
		<div class="elementor-container<# if ( settings.layout === 'boxed' ) { #> content<# } #>">
			<div class="elementor-row"></div>
		</div>
            
		<?php
	}

	public function before_render() {
		$section_type = $this->get_data( 'isInner' ) ? 'inner' : 'top';

		$this->add_render_attribute( 'wrapper', 'class', [
			'elementor-section',
			'elementor-element',
			'elementor-element-' . $this->get_id(),
			'elementor-' . $section_type . '-section',
		] );
        
        if ( $section_type == 'top' ) $this->add_render_attribute( 'wrapper', 'class', 'section' );

		$settings = $this->get_settings();

		foreach ( $this->get_class_controls() as $control ) {
			if ( empty( $settings[ $control['name'] ] ) )
				continue;
            
            if ( $section_type == 'inner' && $control['name'] == 'padding' )
                continue;
            
            if ( !in_array( $control['name'], [ 'padding', 'css_classes', 'hide_desktop', 'hide_tablet', 'hide_mobile' ] ) )
                continue;

			if ( ! $this->is_control_visible( $control ) )
				continue;

			$this->add_render_attribute( 'wrapper', 'class', $control['prefix_class'] . $settings[ $control['name'] ] );
		}

		if ( ! empty( $settings['_element_id'] ) ) {
			$this->add_render_attribute( 'wrapper', 'id', trim( $settings['_element_id'] ) );
		}
        
        if ( ! empty( $settings['anchor'] ) ) {
			$this->add_render_attribute( 'wrapper', 'data-anchor', trim( $settings['anchor'] ) );
		}

		//$this->add_render_attribute( 'wrapper', 'data-element_type', $this->get_name() );
		?>
            
		<section <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
            
			<?php
			if ( 'video' === $settings['background_background'] ) :
        
				if ( $settings['background_video_link'] ) :
					$video_id = Utils::get_youtube_id_from_url( $settings['background_video_link'] );
					?>
            
					<div class="elementor-background-video-container elementor-hidden-phone">
                        
						<?php if ( $video_id ) :
                            wp_enqueue_script( 'lwd-elementor-youtube' ); ?>
                        
                            <div id="video-<?php echo $this->get_id(); ?>" class="elementor-background-video"></div>
                            <script>
                                function onYouTubeIframeAPIReady() {
                                    var player,
                                        playerW = parseInt( jQuery('#video-<?php echo $this->get_id(); ?>').parent().outerWidth() ),
                                        playerH = parseInt( jQuery('#video-<?php echo $this->get_id(); ?>').parent().outerHeight() );

                                    if ( playerH / 1.7778 < playerW ) {
                                        playerH = playerW / 1.7778;
                                    } else {
                                        playerW = playerH / 1.7778;
                                    }

                                    player = new YT.Player('video-<?php echo $this->get_id(); ?>', {
                                        videoId: '<?php echo $video_id; ?>', // YouTube Video ID
                                        width: playerW + 'px', // Player width (in px)
                                        height: playerH + 'px', // Player height (in px)
                                        playerVars: {
                                            autoplay: 1, // Auto-play the video on load
                                            controls: 0, // Show pause/play buttons in player
                                            showinfo: 0, // Hide the video title
                                            modestbranding: 1, // Hide the Youtube Logo
                                            loop: 1, // Run the video in a loop
                                            fs: 0, // Hide the full screen button
                                            cc_load_policty: 0, // Hide closed captions
                                            iv_load_policy: 3, // Hide the Video Annotations
                                            autohide: 0 // Hide video controls when playing
                                        },
                                        events: {
                                            onReady: function(e) {
                                                e.target.mute();
                                            }
                                        }
                                    });
                                }
                            </script>
                        
						<?php else : ?>
                        
							<video class="elementor-background-video elementor-html5-video" src="<?php echo $settings['background_video_link'] ?>"<?php echo ( isset( $settings['background_video_fallback'] ) && $settings['background_video_fallback'] != '' ? ' poster="' . $settings['background_video_fallback'] . '"' : '' ); ?> autoplay loop muted></video>
                        
						<?php endif; ?>
                        
					</div>
            
				<?php endif;
        
			endif;

			if ( ! empty( $settings['background_overlay'] ) ) : ?>
				<div class="elementor-background-overlay"></div>
			<?php endif; ?>
            
            <?php if ( isset( $settings['title'] ) && $settings['title'] != '' ) { ?>
            
                <?php
                  
                    $this->add_render_attribute( 'section_title', 'class', 'title-perex' );
                
                     if ( ! empty( $settings['title_color'] ) ) {
                        $this->add_render_attribute( 'section_title', 'class', 'title-perex--' . trim( $settings['title_color'] ) );
                    }
                
                    if ( ! empty( $settings['title_align'] ) ) {
                        $this->add_render_attribute( 'section_title', 'class', 'title-perex--' . trim( $settings['title_align'] ) );
                    }
                
                    $title_tag = isset( $settings['title_tag'] ) && $settings['title_tag'] != '' ? $settings['title_tag'] : 'h1';
                                                                                  
                ?>
            
                <div class="content">
                    <div <?php echo $this->get_render_attribute_string( 'section_title' ); ?>> 	
                            <<?php echo $title_tag; ?> class="title-perex__title js_loadFade"><?php echo $settings['title']; ?></<?php echo $title_tag; ?>>
                            
                            <?php if ( isset( $settings['title_perex'] ) && $settings['title_perex'] != '' ) { ?>
                                <div class="title-perex__perex js_loadFade"><?php echo do_shortcode( apply_filters( 'wpautop', $settings['title_perex'] ) ); ?></div>
                            <?php } ?>
                    </div>
                </div>
            <?php } ?>
            
			<?php if ( isset( $settings['layout'] ) && 'boxed' === $settings['layout'] ) { ?><div class="content"><?php } ?>
				<div class="pure-g">
		<?php
	}

	public function after_render() {
		?>
				</div>
			<?php if ( isset( $settings['layout'] ) &&  'boxed' === $settings['layout'] ) { ?></div><?php } ?>
		</section>
		<?php
	}

	protected function _get_default_child_type( array $element_data ) {
		return Plugin::$instance->elements_manager->get_element_types( 'column' );
	}
}
