<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Element_Custom_Column extends Element_Base {

	protected static $_edit_tools;

	protected static function get_default_edit_tools() {
		return [
			'duplicate' => [
				'title' => __( 'Duplicate', LWD_ELEM_DOMAIN ),
				'icon' => 'files-o',
			],
			'add' => [
				'title' => __( 'Save', LWD_ELEM_DOMAIN ),
				'icon' => 'plus',
			],
			'remove' => [
				'title' => __( 'Remove', LWD_ELEM_DOMAIN ),
				'icon' => 'times',
			],
		];
	}

	public function get_name() {
		return 'column';
	}

	public function get_title() {
		return __( 'Column', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-columns';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Background and border', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SECTION,
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'types' => [ 'none', 'classic' ],
				'selector' => '{{WRAPPER}} > .elementor-element-populated',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'border',
				'selector' => '{{WRAPPER}} > .elementor-element-populated',
			]
		);

		$this->end_controls_section();

		// Section Column Background Overlay
		$this->start_controls_section(
			'section_background_overlay',
			[
				'label' => __( 'Background overlay', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'background_background' => [ 'classic' ],
				],
			]
		);

		$this->add_control(
			'background_overlay',
			[
				'label' => __( 'Overlay color', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
                'default' => '',
				'selectors' => [
					'{{WRAPPER}} > .elementor-element-populated >  .elementor-background-overlay' => 'background-color: {{SIZE}};',
				],
				'condition' => [
					'background_background' => [ 'classic' ],
				],
			]
		);

		$this->add_control(
			'background_overlay_opacity',
			[
				'label' => __( 'Color opacity (%)', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => .5,
				],
				'range' => [
					'px' => [
						'max' => 1,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} > .elementor-element-populated >  .elementor-background-overlay' => 'opacity: {{SIZE}};',
				],
				'condition' => [
					'background_background' => [ 'classic' ],
				],
			]
		);

		$this->end_controls_section();

		// Section Typography
		$this->start_controls_section(
			'section_typo',
			[
				'label' => __( 'Typography', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SECTION,
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		/*$this->add_control(
			'heading_color',
			[
				'label' => __( 'Barva nadpisu', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-element-populated .elementor-heading-title' => 'color: {{VALUE}};',
				],
			]
		);*/

		$this->add_control(
			'color_text',
			[
				'label' => __( 'Text color', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} > .elementor-element-populated' => 'color: {{VALUE}};',
				],
			]
		);

		/*$this->add_control(
			'color_link',
			[
				'label' => __( 'Barva odkazu', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-element-populated a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'color_link_hover',
			[
				'label' => __( 'Barva odkazu při najetí', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-element-populated a:hover' => 'color: {{VALUE}};',
				],
			]
		);*/

		$this->add_control(
			'text_align',
			[
				'label' => __( 'Text align', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} > .elementor-element-populated' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		// Section Advanced
		$this->start_controls_section(
			'section_advanced',
			[
				'label' => __( 'Advanced', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SECTION,
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label' => __( 'Margin', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} > .elementor-element-populated' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label' => __( 'Padding', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} > .elementor-element-populated' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
        
        global $elem_animation;

        if ( !empty( $elem_animation ) ) {
            $this->add_control(
                'animation',
                [
                    'label' => __( 'Animation', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::SELECT,
                    'default' => '',
                    'options' => $elem_animation,
                    'prefix_class' => 'js_',
                    'label_block' => true,
                    ]
            );
        }

		$this->add_control(
			'_element_id',
			[
				'label' => __( 'CSS ID', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'label_block' => true,
				'title' => __( 'Add your own ID without a hash # at the beginning and no spaces, for example: my-id', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'css_classes',
			[
				'label' => __( 'CSS classes', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'prefix_class' => '',
				'label_block' => true,
				'title' => __( 'Add your own class without a dot . at the beginning and no spaces, for example: my-class', LWD_ELEM_DOMAIN ),
			]
		);

		$this->end_controls_section();

		// Section Responsive
		$this->start_controls_section(
			'section_responsive',
			[
				'label' => __( 'Responsive', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);

		$responsive_points = [
			'screen_sm' => [
				'title' => __( 'Mobile width', LWD_ELEM_DOMAIN ),
                'subtitle' => __( '%s on mobile', LWD_ELEM_DOMAIN ),
				'class_prefix' => 'v350',
				'classes' => '',
				'description' => '',
			],
            /*'screen_md' => [
				'title' => __( 'Šířka na tabletu', LWD_ELEM_DOMAIN ),
                'subtitle' => __( '%s na tabletu', LWD_ELEM_DOMAIN ),
				'class_prefix' => 'v700',
				'classes' => '',
				'description' => '',
			],*/
		];

		foreach ( $responsive_points as $point_name => $point_data ) {
			$this->add_control(
				$point_name,
				[
					'label' => $point_data['title'],
					'type' => Controls_Manager::SELECT,
					'default' => 'default',
					'options' => [
						'default' => __( 'Default', LWD_ELEM_DOMAIN ),
						'custom' => __( 'Custom', LWD_ELEM_DOMAIN ),
					],
					'description' => $point_data['description'],
					'classes' => $point_data['classes'],
				]
			);

			$this->add_control(
				$point_name . '_width',
				[
					'label' => sprintf( $point_data['subtitle'], __( 'Column width', LWD_ELEM_DOMAIN ) ),
					'type' => Controls_Manager::SELECT,
					'options' => [
                        '4-24' => '16%',
                        '6-24' => '25%',
                        '8-24' => '33%',
                        //'10-24' => '41%',
						'12-24' => '50%',
                        //'14-24' => '58%',
                        '16-24' => '67%',
                        '18-24' => '75%',
                        //'20-24' => '83%',
						'24-24' => '100%',
					],
					'default' => '100',
					'condition' => [
						$point_name => [ 'custom' ],
					],
					'prefix_class' => 'pure-u-' . $point_data['class_prefix'] . '-',
				]
			);
		}

		$this->end_controls_section();
	}

	protected function _render_settings() {
		?>
		<div class="elementor-element-overlay">
			<div class="column-title"></div>
			<div class="elementor-editor-element-settings elementor-editor-column-settings">
				<ul class="elementor-editor-element-settings-list elementor-editor-column-settings-list">
					<li class="elementor-editor-element-setting elementor-editor-element-trigger">
						<a href="#" title="<?php _e( 'Move column', LWD_ELEM_DOMAIN ); ?>"><?php _e( 'Column', LWD_ELEM_DOMAIN ); ?></a>
					</li>
					<?php foreach ( self::get_edit_tools() as $edit_tool_name => $edit_tool ) : ?>
						<li class="elementor-editor-element-setting elementor-editor-element-<?php echo $edit_tool_name; ?>">
							<a href="#" title="<?php echo $edit_tool['title']; ?>">
								<span class="elementor-screen-only"><?php echo $edit_tool['title']; ?></span>
								<i class="fa fa-<?php echo $edit_tool['icon']; ?>"></i>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
				<ul class="elementor-editor-element-settings-list  elementor-editor-section-settings-list">
					<li class="elementor-editor-element-setting elementor-editor-element-trigger">
						<a href="#" title="<?php _e( 'Move section', LWD_ELEM_DOMAIN ); ?>"><?php _e( 'Section', LWD_ELEM_DOMAIN ); ?></a>
					</li>
					<?php foreach ( Element_Section::get_edit_tools() as $edit_tool_name => $edit_tool ) : ?>
						<li class="elementor-editor-element-setting elementor-editor-element-<?php echo $edit_tool_name; ?>">
							<a href="#" title="<?php echo $edit_tool['title']; ?>">
								<span class="elementor-screen-only"><?php echo $edit_tool['title']; ?></span>
								<i class="fa fa-<?php echo $edit_tool['icon']; ?>"></i>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<?php
	}

	protected function _content_template() {
		?>
		<div class="elementor-column-wrap">
			<div class="elementor-background-overlay"></div>
			<div class="elementor-widget-wrap"></div>
		</div>
		<?php
	}

	public function before_render() {
		$is_inner = $this->get_data( 'isInner' );

		$column_type = ! empty( $is_inner ) ? 'inner' : 'top';

		$settings = $this->get_settings();

		$this->add_render_attribute( 'wrapper', 'class', [
			'elementor-column',
			'elementor-element',
			'elementor-element-' . $this->get_id(),
            'pure-u-1-1',
            'pure-u-v700-' . elem_get_col( 24 * ( ( isset( $settings['_inline_size'] ) && $settings['_inline_size'] != '' ? $settings['_inline_size'] : $settings['_column_size'] ) / 100 ) ) . '-24'
		] );

		foreach ( self::get_class_controls() as $control ) {
			if ( empty( $settings[ $control['name'] ] ) )
				continue;

			if ( ! $this->is_control_visible( $control ) )
				continue;

			$this->add_render_attribute( 'wrapper', 'class', $control['prefix_class'] . $settings[ $control['name'] ] );
		}

		if ( ! empty( $settings['_element_id'] ) ) {
			$this->add_render_attribute( 'wrapper', 'id', trim( $settings['_element_id'] ) );
		}

		//$this->add_render_attribute( 'wrapper', 'data-element_type', $this->get_name() );
		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="elementor-column-wrap<?php if ( $this->get_children() ) echo ' elementor-element-populated'; ?>">
			<?php if ( ! empty( $settings['background_overlay'] ) ) : ?>
				<div class="elementor-background-overlay"></div>
			<?php endif; ?>
		<div class="elementor-widget-wrap">
		<?php
	}

	public function after_render() {
		?>
				</div>
			</div>
		</div>
		<?php
	}

	protected function _get_default_child_type( array $element_data ) {
		if ( 'section' === $element_data['elType'] ) {
			return Plugin::$instance->elements_manager->get_element_types( 'section' );
		}

		return Plugin::$instance->widgets_manager->get_widget_types( $element_data['widgetType'] );
	}
}
