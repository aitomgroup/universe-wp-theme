#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: LWD Custom Elementor 0.1\n"
"POT-Creation-Date: 2017-09-19 07:33+0200\n"
"PO-Revision-Date: 2015-08-12 15:38+0200\n"
"Last-Translator: Jan Semilsky <info@lightwebdesign.cz>\n"
"Language-Team: LWD <info@lwd.cz>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.3\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;"
"__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;"
"_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;"
"esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;"
"transChoice:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: assets\n"

#: controls/internal-url.php:35 controls/internal-url.php:36
msgid "Suggest URL"
msgstr ""

#: elements/column.php:13 elements/section.php:15
msgid "Duplicate"
msgstr ""

#: elements/column.php:17 elements/section.php:19
msgid "Save"
msgstr ""

#: elements/column.php:21 elements/section.php:23
msgid "Remove"
msgstr ""

#: elements/column.php:32 elements/column.php:358
msgid "Column"
msgstr ""

#: elements/column.php:43
msgid "Background and border"
msgstr ""

#: elements/column.php:72 elements/section.php:267
msgid "Background overlay"
msgstr ""

#: elements/column.php:83 elements/section.php:278
msgid "Overlay color"
msgstr ""

#: elements/column.php:98 elements/section.php:293
msgid "Color opacity (%)"
msgstr ""

#: elements/column.php:124 elements/section.php:342
msgid "Typography"
msgstr ""

#: elements/column.php:145 elements/section.php:350 widgets/heading.php:209
#: widgets/image.php:314 widgets/text-editor.php:79
msgid "Text color"
msgstr ""

#: elements/column.php:181 elements/section.php:214 elements/section.php:362
msgid "Text align"
msgstr ""

#: elements/column.php:185 elements/section.php:218 elements/section.php:366
#: widgets/button.php:146 widgets/counter.php:89 widgets/heading.php:164
#: widgets/icon-box.php:105 widgets/image.php:55 widgets/image.php:288
#: widgets/offices.php:114 widgets/person.php:96 widgets/posts.php:51
#: widgets/service.php:51 widgets/testimony.php:51 widgets/text-editor.php:54
msgid "Left"
msgstr ""

#: elements/column.php:189 elements/section.php:222 elements/section.php:370
#: widgets/button.php:150 widgets/counter.php:93 widgets/heading.php:168
#: widgets/icon-box.php:109 widgets/image.php:59 widgets/image.php:292
#: widgets/person.php:95 widgets/posts.php:55 widgets/service.php:55
#: widgets/testimony.php:55 widgets/text-editor.php:58
msgid "Center"
msgstr ""

#: elements/column.php:193 elements/section.php:226 elements/section.php:374
#: widgets/button.php:154 widgets/counter.php:97 widgets/heading.php:172
#: widgets/icon-box.php:113 widgets/image.php:63 widgets/image.php:296
#: widgets/offices.php:115 widgets/person.php:97 widgets/posts.php:59
#: widgets/service.php:59 widgets/testimony.php:59 widgets/text-editor.php:62
msgid "Right"
msgstr ""

#: elements/column.php:209 elements/section.php:390
msgid "Advanced"
msgstr ""

#: elements/column.php:218
msgid "Margin"
msgstr ""

#: elements/column.php:230 elements/section.php:316
msgid "Padding"
msgstr ""

#: elements/column.php:245 widgets/common.php:31
msgid "Animation"
msgstr ""

#: elements/column.php:258 elements/section.php:398 widgets/common.php:44
msgid "CSS ID"
msgstr ""

#: elements/column.php:262 elements/section.php:402 widgets/common.php:48
msgid ""
"Add your own ID without a hash # at the beginning and no spaces, for "
"example: my-id"
msgstr ""

#: elements/column.php:269 elements/section.php:409 widgets/common.php:55
msgid "CSS classes"
msgstr ""

#: elements/column.php:274 elements/section.php:414 widgets/common.php:60
msgid ""
"Add your own class without a dot . at the beginning and no spaces, for "
"example: my-class"
msgstr ""

#: elements/column.php:284 elements/section.php:434 widgets/common.php:69
msgid "Responsive"
msgstr ""

#: elements/column.php:291
msgid "Mobile width"
msgstr ""

#: elements/column.php:292
#, php-format
msgid "%s on mobile"
msgstr ""

#: elements/column.php:314 widgets/offices.php:127
msgid "Default"
msgstr ""

#: elements/column.php:315
msgid "Custom"
msgstr ""

#: elements/column.php:325
msgid "Column width"
msgstr ""

#: elements/column.php:358
msgid "Move column"
msgstr ""

#: elements/column.php:371
msgid "Move section"
msgstr ""

#: elements/column.php:371 elements/section.php:34 elements/section.php:488
msgid "Section"
msgstr ""

#: elements/section.php:121
msgid "Layout"
msgstr ""

#: elements/section.php:129
msgid "Content width"
msgstr ""

#: elements/section.php:133
msgid "Fixed width"
msgstr ""

#: elements/section.php:134
msgid "Window width"
msgstr ""

#: elements/section.php:142
msgid "Structure"
msgstr ""

#: elements/section.php:152 widgets/accordion.php:87 widgets/button.php:170
#: widgets/counter.php:121 widgets/heading.php:190 widgets/heading.php:201
#: widgets/icon-box.php:93 widgets/image-gallery.php:69 widgets/image.php:191
#: widgets/offices.php:79 widgets/offices.php:90 widgets/person.php:84
#: widgets/text-editor.php:42 widgets/video.php:304
msgid "Display"
msgstr ""

#: elements/section.php:165 widgets/heading.php:13 widgets/heading.php:24
#: widgets/heading.php:31 widgets/icon-box.php:31 widgets/offices.php:46
#: widgets/timeline.php:54
msgid "Title"
msgstr ""

#: elements/section.php:173
msgid "Section title"
msgstr ""

#: elements/section.php:183 widgets/heading.php:143
msgid "HTML Tag"
msgstr ""

#: elements/section.php:201
msgid "Heading color"
msgstr ""

#: elements/section.php:205
msgid "Dark color"
msgstr ""

#: elements/section.php:206
msgid "Bright color"
msgstr ""

#: elements/section.php:236
msgid "Perex"
msgstr ""

#: elements/section.php:248
msgid "Background"
msgstr ""

#: elements/section.php:324
msgid "Section inner padding"
msgstr ""

#: elements/section.php:328
msgid "High padding"
msgstr ""

#: elements/section.php:329
msgid "Medium padding"
msgstr ""

#: elements/section.php:330
msgid "Small padding"
msgstr ""

#: elements/section.php:421
msgid "Anchor"
msgstr ""

#: elements/section.php:442 widgets/common.php:77
msgid "Hide on computer"
msgstr ""

#: elements/section.php:446 elements/section.php:459 elements/section.php:472
#: widgets/accordion.php:69 widgets/common.php:81 widgets/common.php:94
#: widgets/common.php:107 widgets/heading.php:48 widgets/image-gallery.php:60
#: widgets/offices.php:101 widgets/posts.php:90 widgets/posts.php:113
#: widgets/service.php:90 widgets/testimony.php:90 widgets/testimony.php:113
#: widgets/video.php:115 widgets/video.php:128 widgets/video.php:141
#: widgets/video.php:155 widgets/video.php:170 widgets/video.php:183
#: widgets/video.php:196 widgets/video.php:210 widgets/video.php:224
#: widgets/video.php:325 widgets/video.php:350
msgid "Yes"
msgstr ""

#: elements/section.php:447 elements/section.php:460 elements/section.php:473
#: widgets/accordion.php:70 widgets/common.php:82 widgets/common.php:95
#: widgets/common.php:108 widgets/heading.php:49 widgets/image-gallery.php:61
#: widgets/offices.php:102 widgets/posts.php:91 widgets/posts.php:114
#: widgets/service.php:91 widgets/testimony.php:91 widgets/testimony.php:114
#: widgets/video.php:114 widgets/video.php:127 widgets/video.php:140
#: widgets/video.php:154 widgets/video.php:169 widgets/video.php:182
#: widgets/video.php:195 widgets/video.php:209 widgets/video.php:223
#: widgets/video.php:324 widgets/video.php:351
msgid "No"
msgstr ""

#: elements/section.php:455 widgets/common.php:90
msgid "Hide on tablet"
msgstr ""

#: elements/section.php:468 widgets/common.php:103
msgid "Hide on mobile"
msgstr ""

#: elements/section.php:488
msgid "Edit section"
msgstr ""

#: init.php:192 init.php:193 widgets/posts.php:154 widgets/service.php:144
#: widgets/testimony.php:153
msgid "Settings"
msgstr ""

#: init.php:196 init.php:197
msgid "List of modules"
msgstr ""

#: init.php:204
msgid ""
"There are no available predefined templates.<br>But you can create your "
"own templates.<br>Go to \"My Templates\" tab."
msgstr ""

#: init.php:233
msgid "An error has occurred. Please refresh the page and try again."
msgstr ""

#: init.php:243
msgid "Find a link"
msgstr ""

#: init.php:251
msgid "Search"
msgstr ""

#: init.php:256
msgid "Copy this code and paste it into link field."
msgstr ""

#: init.php:320
msgid "No animation"
msgstr ""

#: init.php:321
msgid "Fade"
msgstr ""

#: init.php:322
msgid "Slide from top"
msgstr ""

#: init.php:323
msgid "Slide from sides"
msgstr ""

#: init.php:324
msgid "Slide from left"
msgstr ""

#: init.php:325
msgid "Slide from right"
msgstr ""

#: init.php:329 widgets/button.php:71 widgets/heading.php:70
#: widgets/image.php:118
msgid "Internal link"
msgstr ""

#: init.php:330 widgets/button.php:84 widgets/heading.php:84
#: widgets/image.php:132
msgid "External link"
msgstr ""

#: init.php:331 widgets/person.php:62
msgid "Phone"
msgstr ""

#: init.php:332 widgets/person.php:72
msgid "E-mail"
msgstr ""

#: widgets/accordion.php:13 widgets/accordion.php:24
msgid "Accordion"
msgstr ""

#: widgets/accordion.php:31
msgid "Accordion items"
msgstr ""

#: widgets/accordion.php:35
msgid "Accordion #1"
msgstr ""

#: widgets/accordion.php:36 widgets/accordion.php:40 widgets/timeline.php:37
#: widgets/timeline.php:42
msgid ""
"I am the content of this item. Click on Edit button to change this text. "
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, "
"luctus nec ullamcorper mattis, pulvinar dapibus leo."
msgstr ""

#: widgets/accordion.php:39
msgid "Accordion #2"
msgstr ""

#: widgets/accordion.php:46
msgid "Title and content"
msgstr ""

#: widgets/accordion.php:48
msgid "Item title"
msgstr ""

#: widgets/accordion.php:53
msgid "Content"
msgstr ""

#: widgets/accordion.php:55
msgid "Item content"
msgstr ""

#: widgets/accordion.php:66
msgid "Allow open all panels"
msgstr ""

#: widgets/accordion.php:77
msgid "Active panel"
msgstr ""

#: widgets/accordion.php:80
msgid ""
"Set number of item that you want to open when the page is loaded. Leave "
"the field blank to close all panels when the page is loaded."
msgstr ""

#: widgets/button.php:13 widgets/button.php:24
msgid "Button"
msgstr ""

#: widgets/button.php:31
msgid "Button style"
msgstr ""

#: widgets/button.php:35
msgid "Classic button"
msgstr ""

#: widgets/button.php:36
msgid "Highlight button"
msgstr ""

#: widgets/button.php:37
msgid "Border button"
msgstr ""

#: widgets/button.php:38
msgid "Black text button"
msgstr ""

#: widgets/button.php:47
msgid "Buttton text"
msgstr ""

#: widgets/button.php:49 widgets/button.php:50
msgid "Click me"
msgstr ""

#: widgets/button.php:61 widgets/heading.php:57 widgets/image.php:105
msgid "Type of link"
msgstr ""

#: widgets/button.php:86 widgets/button.php:128 widgets/heading.php:86
#: widgets/heading.php:131 widgets/image.php:134 widgets/image.php:179
msgid "http://www.example.com"
msgstr ""

#: widgets/button.php:98 widgets/heading.php:99 widgets/image.php:147
msgid "Enter the phone number"
msgstr ""

#: widgets/button.php:100 widgets/heading.php:101 widgets/image.php:149
msgid "+44 02 7778 8855"
msgstr ""

#: widgets/button.php:112 widgets/heading.php:114 widgets/image.php:162
msgid "Enter an email address"
msgstr ""

#: widgets/button.php:114 widgets/heading.php:116 widgets/image.php:164
#: widgets/person.php:74
msgid "john.doe@mail.co.uk"
msgstr ""

#: widgets/button.php:126 widgets/heading.php:45 widgets/heading.php:129
#: widgets/image-gallery.php:43 widgets/image.php:87 widgets/video.php:45
#: widgets/video.php:59 widgets/video.php:73
msgid "Link"
msgstr ""

#: widgets/button.php:142 widgets/counter.php:85 widgets/heading.php:160
#: widgets/icon-box.php:101 widgets/image.php:51 widgets/image.php:284
#: widgets/posts.php:47 widgets/service.php:47 widgets/testimony.php:47
#: widgets/text-editor.php:50
msgid "Alignment"
msgstr ""

#: widgets/button.php:158
msgid "Fullwidth"
msgstr ""

#: widgets/common.php:20
msgid "Module style"
msgstr ""

#: widgets/counter.php:13 widgets/counter.php:24
msgid "Counter"
msgstr ""

#: widgets/counter.php:31
msgid "Items"
msgstr ""

#: widgets/counter.php:37
msgctxt "counter example description"
msgid "This is the counter label. Click on Edit button to change this text."
msgstr ""

#: widgets/counter.php:42
msgid "Number"
msgstr ""

#: widgets/counter.php:47
msgid "Unit"
msgstr ""

#: widgets/counter.php:52
msgid "Text prefix"
msgstr ""

#: widgets/counter.php:57
msgid "Description"
msgstr ""

#: widgets/counter.php:70 widgets/posts.php:35 widgets/service.php:35
#: widgets/testimony.php:35
msgid "Number of columns"
msgstr ""

#: widgets/counter.php:108 widgets/offices.php:124
msgid "Color scheme"
msgstr ""

#: widgets/counter.php:111
msgid "Dark scheme"
msgstr ""

#: widgets/counter.php:112
msgid "Bright scheme"
msgstr ""

#: widgets/heading.php:33 widgets/icon-box.php:33
msgid "Enter your headline here"
msgstr ""

#: widgets/heading.php:34
msgid "This is the title module"
msgstr ""

#: widgets/heading.php:176 widgets/image.php:300 widgets/text-editor.php:66
msgid "Justify"
msgstr ""

#: widgets/icon-box.php:13 widgets/icon-box.php:24
msgid "Icon box"
msgstr ""

#: widgets/icon-box.php:34
msgid "This is the title"
msgstr ""

#: widgets/icon-box.php:42
msgid "Box type"
msgstr ""

#: widgets/icon-box.php:45
msgid "Text or number"
msgstr ""

#: widgets/icon-box.php:46
msgid "Icon"
msgstr ""

#: widgets/icon-box.php:84 widgets/text-editor.php:33
msgid ""
"I'm a block of text. Click on Edit button to change this text. Lorem ipsum "
"dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec "
"ullamcorper mattis, pulvinar dapibus leo."
msgstr ""

#: widgets/image-gallery.php:13 widgets/image-gallery.php:28
msgid "Gallery"
msgstr ""

#: widgets/image-gallery.php:35
msgid "Add images"
msgstr ""

#: widgets/image-gallery.php:47
msgid "Image / video"
msgstr ""

#: widgets/image-gallery.php:48 widgets/image.php:92
msgid "Media file"
msgstr ""

#: widgets/image-gallery.php:49 widgets/image.php:91
msgid "No link"
msgstr ""

#: widgets/image-gallery.php:57
msgid "Random order"
msgstr ""

#: widgets/image.php:13 widgets/image.php:24 widgets/image.php:202
#: widgets/video.php:332
msgid "Image"
msgstr ""

#: widgets/image.php:31 widgets/person.php:41 widgets/timeline.php:65
msgid "Select an image"
msgstr ""

#: widgets/image.php:43
msgid "Image size"
msgstr ""

#: widgets/image.php:77 widgets/image.php:276
msgid "Caption"
msgstr ""

#: widgets/image.php:80
msgid "Enter image caption"
msgstr ""

#: widgets/image.php:93 widgets/image.php:177
msgid "Custom URL"
msgstr ""

#: widgets/image.php:210
msgid "Size (%)"
msgstr ""

#: widgets/image.php:232
msgid "Opacity (%)"
msgstr ""

#: widgets/image.php:254
msgid "Border type"
msgstr ""

#: widgets/image.php:262
msgid "Border radius"
msgstr ""

#: widgets/offices.php:13 widgets/offices.php:24
msgid "Branches"
msgstr ""

#: widgets/offices.php:31
msgid "List of branches"
msgstr ""

#: widgets/offices.php:35
msgctxt "offices default name"
msgid "AITOM"
msgstr ""

#: widgets/offices.php:36
msgctxt "offices default address"
msgid ""
"221 High Holborn,\n"
"London"
msgstr ""

#: widgets/offices.php:39
msgctxt "offices default name"
msgid "AITOM Bristol"
msgstr ""

#: widgets/offices.php:40
msgctxt "offices default address"
msgid ""
"4 Queen Square,\n"
"Bristol"
msgstr ""

#: widgets/offices.php:48
msgid "Branch name"
msgstr ""

#: widgets/offices.php:53
msgid "Address"
msgstr ""

#: widgets/offices.php:60
msgid "GPS Address"
msgstr ""

#: widgets/offices.php:63
msgid "Set in format 50.0523074,14.4565214"
msgstr ""

#: widgets/offices.php:68
msgid "Pin"
msgstr ""

#: widgets/offices.php:98
msgid "Show list"
msgstr ""

#: widgets/offices.php:109
msgid "List position"
msgstr ""

#: widgets/offices.php:112
msgid "Above the map"
msgstr ""

#: widgets/offices.php:113
msgid "Under the map"
msgstr ""

#: widgets/offices.php:128
msgid "For a light background"
msgstr ""

#: widgets/person.php:13 widgets/person.php:24
msgid "Person"
msgstr ""

#: widgets/person.php:31
msgid "Name and Surname"
msgstr ""

#: widgets/person.php:33
msgid "John Doe"
msgstr ""

#: widgets/person.php:52
msgid "Short caption"
msgstr ""

#: widgets/person.php:92
msgid "Picture alignment"
msgstr ""

#: widgets/posts.php:13
msgid "Posts"
msgstr ""

#: widgets/posts.php:28
msgid "Listing of posts"
msgstr ""

#: widgets/posts.php:70 widgets/service.php:70 widgets/testimony.php:70
msgid "Include categories"
msgstr ""

#: widgets/posts.php:80
msgid ""
"Select categories from which display posts. To display all, leave the "
"field blank."
msgstr ""

#: widgets/posts.php:87 widgets/service.php:87 widgets/testimony.php:87
msgid "Category filter"
msgstr ""

#: widgets/posts.php:98
msgid "Number of posts on one page"
msgstr ""

#: widgets/posts.php:110 widgets/testimony.php:110
msgid "Pagination"
msgstr ""

#: widgets/posts.php:121 widgets/service.php:112 widgets/testimony.php:121
msgid "Sort by"
msgstr ""

#: widgets/posts.php:124 widgets/service.php:115 widgets/testimony.php:124
msgid "By date"
msgstr ""

#: widgets/posts.php:125
msgid "By author"
msgstr ""

#: widgets/posts.php:126 widgets/service.php:116 widgets/testimony.php:125
msgid "By title"
msgstr ""

#: widgets/posts.php:127 widgets/service.php:117 widgets/testimony.php:126
msgid "Random"
msgstr ""

#: widgets/posts.php:139 widgets/service.php:129 widgets/testimony.php:138
#: widgets/timeline.php:79
msgid "Descending"
msgstr ""

#: widgets/posts.php:140 widgets/service.php:130 widgets/testimony.php:139
#: widgets/timeline.php:80
msgid "Ascending"
msgstr ""

#: widgets/posts.php:162 widgets/service.php:152 widgets/testimony.php:161
msgid "Listing style"
msgstr ""

#: widgets/posts.php:176 widgets/service.php:166 widgets/testimony.php:175
msgid "Query"
msgstr ""

#: widgets/posts.php:261 widgets/service.php:263 widgets/testimony.php:273
msgid "All"
msgstr ""

#: widgets/service.php:13
msgid "Services"
msgstr ""

#: widgets/service.php:28
msgid "Listing of services"
msgstr ""

#: widgets/service.php:80
msgid ""
"Select categories from which display services. To display all, leave the "
"field blank."
msgstr ""

#: widgets/service.php:98
msgid "Number of services on one page"
msgstr ""

#: widgets/service.php:105
msgid ""
"Enter the number of services listed on one page. The value \"-1\" displays "
"all available services."
msgstr ""

#: widgets/shortcode.php:13 widgets/shortcode.php:28
msgid "Shortcode"
msgstr ""

#: widgets/shortcode.php:35
msgid "Put your shortcode here"
msgstr ""

#: widgets/testimony.php:13
msgid "Testimony"
msgstr ""

#: widgets/testimony.php:28
msgid "Listing of testimonies"
msgstr ""

#: widgets/testimony.php:80
msgid ""
"Select categories from which display testimonies. To display all, leave "
"the field blank."
msgstr ""

#: widgets/testimony.php:98
msgid "Number of testimonies on one page"
msgstr ""

#: widgets/text-editor.php:13 widgets/text-editor.php:24
msgid "Text editor"
msgstr ""

#: widgets/timeline.php:13 widgets/timeline.php:24
msgid "Timeline"
msgstr ""

#: widgets/timeline.php:31
msgid "Timeline items"
msgstr ""

#: widgets/timeline.php:36
msgid "Event title #1"
msgstr ""

#: widgets/timeline.php:41
msgid "Event title #2"
msgstr ""

#: widgets/timeline.php:48
msgid "Year"
msgstr ""

#: widgets/timeline.php:76
msgid "Years sorting"
msgstr ""

#: widgets/video.php:13 widgets/video.php:24
msgid "Video"
msgstr ""

#: widgets/video.php:31
msgid "Video type"
msgstr ""

#: widgets/video.php:35
msgid "YouTube"
msgstr ""

#: widgets/video.php:36
msgid "Vimeo"
msgstr ""

#: widgets/video.php:47
msgid "Enter a YouTube video link"
msgstr ""

#: widgets/video.php:61
msgid "Enter a Vimeo video link"
msgstr ""

#: widgets/video.php:87
msgid "Aspect Ratio"
msgstr ""

#: widgets/video.php:102
msgid "Video settings"
msgstr ""

#: widgets/video.php:112 widgets/video.php:167
msgid "Autoplay"
msgstr ""

#: widgets/video.php:125
msgid "Suggested videos"
msgstr ""

#: widgets/video.php:138
msgid "Player controls"
msgstr ""

#: widgets/video.php:152
msgid "Player Title & Actions"
msgstr ""

#: widgets/video.php:180
msgid "Loop"
msgstr ""

#: widgets/video.php:193
msgid "Intro title"
msgstr ""

#: widgets/video.php:207
msgid "Intro Portrait"
msgstr ""

#: widgets/video.php:221
msgid "Intro Byline"
msgstr ""

#: widgets/video.php:235
msgid "Controls Color"
msgstr ""

#: widgets/video.php:315 widgets/video.php:322
msgid "Featured image"
msgstr ""

#: widgets/video.php:346
msgid "Play icon"
msgstr ""

#: widgets/wpcf7.php:13 widgets/wpcf7.php:28
msgid "Form"
msgstr ""

#: widgets/wpcf7.php:35
#, php-format
msgid ""
"Beware: you must have already create at least one form <a href=\"%1$s\" "
"target=\"_blank\">here</a>. If you do not have one, <a href=\"%1$s\" "
"target=\"_blank\">create it</a>."
msgstr ""

#: widgets/wpcf7.php:47
msgid "-- Select an option --"
msgstr ""

#: widgets/wpcf7.php:59
msgid "Select a form from the menu"
msgstr ""
