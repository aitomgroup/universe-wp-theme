<?php

/**
 * init.php
 *
 * - Elementor setup
 * - Unregister default WP widgets
 * - Modify Elementor elements and widgets
 * - Register new controls
 * - Customize Elementor style
 * - Setaup global vars
 */

    // Prevent direct script access
    if ( !defined( 'ABSPATH' ) ) exit;

    defined( 'LWD_ELEM_DIR' )
        || define( 'LWD_ELEM_DIR', LWD_DIR . '/elementor' );

    defined( 'LWD_ELEM_URL' )
        || define( 'LWD_ELEM_URL', LWD_URL . '/elementor' );

    defined( 'LWD_ELEM_DOMAIN' )
        || define( 'LWD_ELEM_DOMAIN', LWD_SLUG . '-elem' );

    // Set up translations
    load_theme_textdomain( LWD_ELEM_DOMAIN, LWD_ELEM_DIR . '/lwd-i18n' );

    /*----------------------------------------------------------------------------*
     * ELEMENTOR SETUP
     *----------------------------------------------------------------------------*/

    // Set Elementor settings
    add_filter( 'pre_option_elementor_cpt_support', function() { return array( 'page' ); } );
    add_filter( 'pre_option_elementor_tracker_notice', function() { return false; } );
    add_filter( 'pre_option_elementor_stretched_section_container', function() { return 'body'; } );
    add_filter( 'pre_option_elementor_allow_tracking', function() { return 'no'; } );
    add_filter( 'pre_option_elementor_exclude_user_roles', function() { return array(); } );
    add_filter( 'pre_option_elementor_disable_color_schemes', function() { return 'yes'; } );
    add_filter( 'pre_option_elementor_disable_typography_schemes', function() { return 'yes'; } );
    add_filter( 'pre_option_elementor_default_generic_fonts', function() { return ''; } );
    add_filter( 'pre_option_elementor_container_width', function() { return ''; } );

    // Remove Elementor settings page
    add_action( 'admin_menu', function() {
        remove_menu_page( 'elementor' );
    }, 22 );

    /*----------------------------------------------------------------------------*
     * UNREGISTER DEFAULT WP WIDGETS
     *----------------------------------------------------------------------------*/

    add_action( 'widgets_init', function() {

        $elementor = \Elementor\Plugin::instance();
        
        if ( ! $elementor->preview->is_preview_mode() ) {
            unregister_widget( 'WP_Widget_Pages' );
            unregister_widget( 'WP_Widget_Calendar' );
            unregister_widget( 'WP_Widget_Archives' );
            unregister_widget( 'WP_Widget_Links' );
            unregister_widget( 'WP_Widget_Meta' );
            unregister_widget( 'WP_Widget_Search' );
            unregister_widget( 'WP_Widget_Text' );
            unregister_widget( 'WP_Widget_Categories' );
            unregister_widget( 'WP_Widget_Recent_Posts' );
            unregister_widget( 'WP_Widget_Recent_Comments' );
            unregister_widget( 'WP_Widget_RSS' );
            unregister_widget( 'WP_Widget_Tag_Cloud' );
            unregister_widget( 'WP_Nav_Menu_Widget' );
        }

    }, 999 );

    /*----------------------------------------------------------------------------*
     * MODIFY ELEMENTOR ELEMENTS
     *----------------------------------------------------------------------------*/

    add_action( 'elementor/elements/elements_registered', function() {

        $elementsManager = \Elementor\Plugin::instance()->elements_manager;

        $elementsManager->unregister_element_type( 'column' );
        $elementsManager->unregister_element_type( 'section' );

        require LWD_ELEM_DIR . '/elements/column.php';
        require LWD_ELEM_DIR . '/elements/section.php';

        $elementsManager->register_element_type( new Elementor\Element_custom_column() );
        $elementsManager->register_element_type( new Elementor\Element_custom_section() );

     } );

    /*----------------------------------------------------------------------------*
     * MODIFY ELEMENTOR WIDGETS
     *----------------------------------------------------------------------------*/

    add_action( 'elementor/widgets/widgets_registered', function() {

        $widgetsManager = \Elementor\Plugin::instance()->widgets_manager;

        // Remove all widgets
        foreach( $widgetsManager->get_widget_types() as $widget_class => $widget_obj ) {
             $widgetsManager->unregister_widget_type( $widget_class );
        }

        // Add custom widget
        require LWD_ELEM_DIR . '/widgets/common.php';
        require LWD_ELEM_DIR . '/widgets/heading.php';
        require LWD_ELEM_DIR . '/widgets/text-editor.php';
        require LWD_ELEM_DIR . '/widgets/button.php';
        require LWD_ELEM_DIR . '/widgets/image.php';
        require LWD_ELEM_DIR . '/widgets/image-gallery.php';
        require LWD_ELEM_DIR . '/widgets/video.php';
        if ( class_exists( 'WPCF7_ContactForm' ) ) require LWD_ELEM_DIR . '/widgets/wpcf7.php';
        require LWD_ELEM_DIR . '/widgets/accordion.php';
        require LWD_ELEM_DIR . '/widgets/counter.php';
        require LWD_ELEM_DIR . '/widgets/person.php';
        require LWD_ELEM_DIR . '/widgets/offices.php';
        require LWD_ELEM_DIR . '/widgets/timeline.php';
        require LWD_ELEM_DIR . '/widgets/icon-box.php';
        require LWD_ELEM_DIR . '/widgets/posts.php';
        require LWD_ELEM_DIR . '/widgets/testimony.php';
        require LWD_ELEM_DIR . '/widgets/service.php';
        require LWD_ELEM_DIR . '/widgets/shortcode.php';
        
        // Add new and sort widgets
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Common() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Heading() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Text_Editor() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Button() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Divider() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Spacer() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Image() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Image_Gallery() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Video() );
        if ( class_exists( 'WPCF7_ContactForm' ) ) $widgetsManager->register_widget_type( new Elementor\Widget_Wpcf7() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Google_Maps() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Accordion() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Counter() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Person() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Offices() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Timeline() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Icon_Box() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Posts() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Testimony() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Service() );
        $widgetsManager->register_widget_type( new Elementor\Widget_Custom_Shortcode() );

    } );

    /*----------------------------------------------------------------------------*
     * REGISTER NEW CONTROLS
     *----------------------------------------------------------------------------*/

    add_action( 'elementor/controls/controls_registered', function() {
        
        $controlsManager = \Elementor\Plugin::instance()->controls_manager;
        
        require LWD_ELEM_DIR . '/controls/internal-url.php';

        $controlsManager->register_control( 'internal_url', new Elementor\Control_Internal_URL() );
        
    } );

    /*----------------------------------------------------------------------------*
     * CUSTOMIZE ELEMENTOR STYLE
     *----------------------------------------------------------------------------*/

    add_action( 'elementor/editor/before_enqueue_scripts', function() {

        $elementor = \Elementor\Plugin::instance();

        // Add admin Roboto subset
        wp_deregister_style( 'google-font-roboto' );
        wp_register_style(
            'google-font-roboto',
            'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=latin-ext',
            [],
            $elementor->get_version()
        );

        // Add admin editor customize
        wp_enqueue_style(
            'custom-elementor-editor',
            LWD_ELEM_URL . '/assets/css/editor.css',
            [
                'elementor-editor',
            ],
            $elementor->get_version()
        );

        // Customize Elementor title panel
        ?><script type="text/template" id="tmpl-elementor-panel-header">
            <div id="elementor-panel-header-menu-button" class="elementor-header-button" title="<?php esc_attr_e( 'Settings', LWD_ELEM_DOMAIN ); ?>">
                <i class="elementor-icon eicon-menu tooltip-target" data-tooltip="<?php esc_attr_e( 'Settings', LWD_ELEM_DOMAIN ); ?>"></i>
            </div>
            <div id="elementor-ai-logo"><span id="elementor-panel-header-title"></span> <a href="https://www.aitom.cz" target="_blank" class="ai-signature"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100.994 34" class="ai-signature__logo"><g class="ai-signature__text"><path class="ai-signature__text__path" d="M50.137 12.632h4.426l3.888 9.133h-3.836l-.384-1.05h-3.76l-.384 1.05H46.25m6.113-6.37h-.026l-1.074 3.095h2.174l-1.074-3.095zM58.95 12.632h3.607v9.133H58.95zM66.766 14.934h-3.34v-2.302h10.285v2.302h-3.337v6.83h-3.607M80.298 12.312c2.188 0 6.242.154 6.242 4.886 0 4.733-4.054 4.886-6.242 4.886-2.187 0-6.242-.14-6.242-4.886 0-4.732 4.055-4.886 6.242-4.886m0 7.47c1.484 0 2.405-.73 2.405-2.584s-.92-2.584-2.405-2.584c-1.483 0-2.404.73-2.404 2.584 0 1.855.92 2.584 2.404 2.584M87.82 12.632h5.078l1.496 5.308h.026l1.496-5.308h5.078v9.133H97.72V15.24h-.026l-1.918 6.525h-2.738L91.12 15.24h-.026v6.525H87.82" class="ai-signature__text__path"/></g><g class="ai-signature__symbol"><path x="-20" y="-20" class="ai-signature__symbol__path" d="M28.495 9.434c.115-.23.14-.358.09-.512-.372-1.036-2.034-.793-2.674-.345-.562.384-.792.934-.792.934s-2.865 5.232-3.172 5.808c-.307.563.14.793.665.793L37.86 16.1h.294c.115 0 .78.04 1.164-.51 1.01-1.472-.64-2.214-1.612-2.253l-.946-.038h-10.36l2.007-3.633c-.025-.025-.025-.025.09-.23M16.586 7.17c-.14-.217-.243-.294-.396-.32-1.087-.192-1.7 1.37-1.637 2.15.05.677.41 1.15.41 1.15s3.12 5.09 3.452 5.64c.333.55.755.27 1.024-.19.267-.46 7.57-13.24 7.57-13.24l.142-.255c.05-.102.422-.652.128-1.266-.768-1.613-2.24-.55-2.75.28l-.5.806-5.154 8.992-2.162-3.543-.128-.205M7.287 13.144c-.256.012-.383.05-.486.166-.728.83.283 2.16.973 2.52.602.307 1.203.255 1.203.255s5.973 0 6.613.013c.638 0 .625-.5.37-.972-.256-.46-7.317-13.38-7.317-13.38l-.153-.255c-.052-.1-.333-.703-1.01-.767C5.7.543 5.84 2.348 6.276 3.218l.422.857 4.962 9.094-4.144-.026h-.23M11.125 24.58c-.116.23-.14.357-.077.51.37 1.036 2.034.793 2.673.346.55-.384.794-.934.794-.934l3.172-5.794c.307-.563-.14-.793-.665-.793l-15.246.012H1.48c-.115 0-.78-.038-1.164.525-1.01 1.47.64 2.213 1.612 2.25l.946.027h10.36l-2.007 3.63-.102.22M23.007 26.83c.14.217.243.294.397.332 1.087.18 1.7-1.368 1.637-2.148-.05-.678-.408-1.152-.408-1.152s-3.12-5.09-3.454-5.64c-.332-.55-.754-.27-1.023.18-.27.46-7.572 13.237-7.572 13.237l-.14.255c-.052.102-.423.652-.13 1.266.77 1.613 2.24.55 2.75-.28l.5-.806 5.155-8.992 2.15 3.543c.012 0 .012 0 .14.205M32.652 20.856c.256-.012.384-.05.486-.166.73-.818-.28-2.16-.972-2.52-.6-.307-1.203-.255-1.203-.255s-5.973-.013-6.613-.013c-.64 0-.626.51-.37.972.255.46 7.316 13.38 7.316 13.38l.154.255c.05.1.332.703 1.01.767 1.778.19 1.637-1.625 1.202-2.495l-.422-.844-4.963-9.094 4.145.025.23-.014" class="ai-signature__symbol__path"/></g></svg></a></div>
            <div id="elementor-panel-header-add-button" class="elementor-header-button" title="<?php esc_attr_e( 'List of modules', LWD_ELEM_DOMAIN ); ?>">
                <i class="elementor-icon eicon-apps tooltip-target" data-tooltip="<?php esc_attr_e( 'List of modules', LWD_ELEM_DOMAIN ); ?>"></i>
            </div>
        </script>

        <script type="text/template" id="tmpl-elementor-template-library-templates">
            <div id="elementor-template-library-templates-container"></div>
            <div id="elementor-template-library-footer-banner">
                <div class="elementor-excerpt"><?php echo __( 'There are no available predefined templates.<br>But you can create your own templates.<br>Go to "My Templates" tab.', LWD_ELEM_DOMAIN ); ?></div>
            </div>
        </script>

        <script type="text/template" id="tmpl-elementor-panel-categories">
            <div id="elementor-panel-categories"></div>
        </script>

        <script type="text/template" id="tmpl-elementor-template-library-template-remote"></script><?php
        
        // Load script to link suggests
        wp_register_script(
            'elementor-editor-link-suggest',
            LWD_ELEM_URL . '/assets/js/editor.linksuggest.js',
            [
                'media-upload',
                'media-views',
                'wp-backbone',
                'wp-util'
            ],
            $elementor->get_version(),
            true
        );

        wp_localize_script(
            'elementor-editor-link-suggest',
            'LinkSuggest',
            [
                'l10n' => [
                    'responseError'   => __( 'An error has occurred. Please refresh the page and try again.', LWD_ELEM_DOMAIN ),
                    //'requireDesc' => __( 'Před uložením nezapomeňte smazat v poli hvezdičku, jinak se nová adresa neuloží.', LWD_ELEM_DOMAIN )
                ]
            ]
        );
        
        wp_enqueue_script( 'elementor-editor-link-suggest' );

        ?><script type="text/html" id="tmpl-link-suggest-modal">
            <div class="link-suggest-modal-head find-box-head">
                <?php _e( 'Find a link', LWD_ELEM_DOMAIN ); ?>
                <div class="link-suggest-modal-close js-close"></div>
            </div>
            <div class="link-suggest-modal-inside find-box-inside wp-core-ui">
                <div class="link-suggest-modal-search find-box-search">
                    <?php wp_nonce_field( 'ls-find-posts', 'ls-find-posts-ajax-nonce', false ); ?>
                    <input type="text" name="s" value="" class="link-suggest-modal-search-field">
                    <span class="spinner"></span>
                    <input type="button" value="<?php esc_attr_e( 'Search', LWD_ELEM_DOMAIN ); ?>" class="button link-suggest-modal-search-button" />
                    <div class="clear"></div>
                </div>
                <div class="link-suggest-modal-output-field">
                    <p><input type="text" value="" />
                    <?php esc_attr_e( 'Copy this code and paste it into link field.', LWD_ELEM_DOMAIN ); ?></p>
                </div>
                <div class="link-suggest-modal-response"></div>
            </div>
        </script><?php

    } );

    // Deregister frontend scripts and styles
    add_action( 'wp_enqueue_scripts', function() {
        
        $elementor = \Elementor\Plugin::instance();
        
        if ( ! $elementor->preview->is_preview_mode() ) {
            
            wp_deregister_style( 'elementor-icons' );
            wp_deregister_style( 'font-awesome' );
            wp_deregister_style( 'elementor-animations' );
            
            wp_deregister_style( 'elementor-frontend' );
            wp_register_style( 'elementor-frontend', false );
            
            wp_register_style( LWD_SLUG . '-font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, '4.7.0' );
            
            wp_deregister_script( 'elementor-waypoints' );
            wp_deregister_script( 'imagesloaded' );
            wp_deregister_script( 'jquery-numerator' );
            wp_deregister_script( 'jquery-slick' );
            
            wp_deregister_script( 'elementor-frontend' );
            //wp_register_script( 'elementor-frontend', false );

            wp_register_script( 'lwd-elementor-youtube', 'https://www.youtube.com/iframe_api#asyncload', false, false );
            
        }
        
    }, 10 );


    /*----------------------------------------------------------------------------*
     * REMOVE FRONTEND COLUMNS WIDTH FROM CSS
     *----------------------------------------------------------------------------*/

    add_action( 'elementor/element/parse_css', function( $post_css, $element ) {
        
        $to_remove = $post_css->get_stylesheet()->get_rules( 'min_tablet' );
        
        if ( !empty( $to_remove ) ) {
            foreach( $to_remove as $elem => $atts ) {
                if ( isset( $atts['width'] ) ) {
                    $post_css->get_stylesheet()->add_rules( $elem, [
                        'width' => '',
                    ], [ 'min' => 'tablet' ] );   
                }
            }
        }
        
    }, 10, 2 );

    /*----------------------------------------------------------------------------*
     * SETUP GLOBAL VARS
     *----------------------------------------------------------------------------*/

    $elem_animation = [
        '' => __( 'No animation', LWD_ELEM_DOMAIN ),
        'loadFade' => __( 'Fade', LWD_ELEM_DOMAIN ),
        'slideFromTop' => __( 'Slide from top', LWD_ELEM_DOMAIN ),
        'slideFromSide' => __( 'Slide from sides', LWD_ELEM_DOMAIN ),
        'slideFromLeft' => __( 'Slide from left', LWD_ELEM_DOMAIN ),
        'slideFromRight' => __( 'Slide from right', LWD_ELEM_DOMAIN ),
    ];

    $elem_link_type = [
        'internal' => __( 'Internal link', LWD_ELEM_DOMAIN ),
        'external' => __( 'External link', LWD_ELEM_DOMAIN ),
        'phone' => __( 'Phone', LWD_ELEM_DOMAIN ),
        'e-mail' => __( 'E-mail', LWD_ELEM_DOMAIN )
    ];

    function elem_get_col( $search, $arr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 ] ) {
       $col = null;
        
       foreach ( $arr as $item ) {
          if ( $col === null || abs( $search - $col ) > abs( $item - $search ) ) {
             $col = $item;
          }
       }
       return $col;
    }