<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Offices extends Widget_Base {

	public function get_name() {
		return 'offices';
	}

	public function get_title() {
		return __( 'Branches', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-google-maps';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Branches', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'items',
			[
				'label' => __( 'List of branches', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
                    [
                        'title' => _x( 'AITOM', 'offices default name', LWD_ELEM_DOMAIN ),
                        'address' => _x( "221 High Holborn,\nLondon", 'offices default address', LWD_ELEM_DOMAIN )
                    ],
                    [
                        'title' => _x( 'AITOM Bristol', 'offices default name', LWD_ELEM_DOMAIN ),
                        'address' => _x( "4 Queen Square,\nBristol", 'offices default address', LWD_ELEM_DOMAIN )
                    ]
				],
				'fields' => [
					[
						'name' => 'title',
						'label' => __( 'Title', LWD_ELEM_DOMAIN ),
						'type' => Controls_Manager::TEXT,
						'default' => __( 'Branch name', LWD_ELEM_DOMAIN ),
						'label_block' => true,
					],
					[
						'name' => 'address',
						'label' => __( 'Address', LWD_ELEM_DOMAIN ),
						'type' => Controls_Manager::TEXTAREA,
						'default' => '',
						'label_block' => true,
					],
                    [
						'name' => 'gps',
						'label' => __( 'GPS Address', LWD_ELEM_DOMAIN ),
						'type' => Controls_Manager::TEXT,
						'default' => '',
                        'placeholder' => __( 'Set in format 50.0523074,14.4565214', LWD_ELEM_DOMAIN ),
						'label_block' => true,
					],
                    [
						'name' => 'pin',
						'label' => __( 'Pin', LWD_ELEM_DOMAIN ),
                        'type' => Controls_Manager::MEDIA,
					],
				],
				'title_field' => '{{{ title }}}',
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);
        
        $this->end_controls_section();
        
        $this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
        
         $this->add_control(
			    'listing',
                [
                    'label' => __( 'Show list', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::SWITCHER,
                    'default' => 'yes',
                    'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
                    'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
                ]
            );
        
        $this->add_control(
			'align',
			[
				'label' => __( 'List position', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Above the map', LWD_ELEM_DOMAIN ),
                    'bottom' => __( 'Under the map', LWD_ELEM_DOMAIN ),
					'left' => __( 'Left', LWD_ELEM_DOMAIN ),
                    'right' => __( 'Right', LWD_ELEM_DOMAIN )
				],
				'default' => ''
			]
		);
        
        $this->add_control(
			'schema',
			[
				'label' => __( 'Color scheme', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Default', LWD_ELEM_DOMAIN ),
                    'dark' => __( 'For a light background', LWD_ELEM_DOMAIN )
				],
				'default' => ''
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();
        
        if ( isset( $settings['items'] ) && !empty( $settings['items'] ) ) {

            $offices = '<div class="offices__items-wrap"><div class="offices__items">';
            
            foreach ( $settings['items'] as $k => $item ) {
                if ( !isset( $item['title'] ) || $item['title'] == '' || !isset( $item['address'] ) || $item['address'] == '' ) continue;
                
                $settings['items'][$k]['gps'] = $this->get_gps_address( $item );

                $offices .= '<div class="offices__i">
                    <div class="offices__i__middle jq_gmapMarkerAnchor" data-for="myMap:marker' . $k . '" data-zoom="13">
                        <h3 class="offices__i__title">' . $item['title'] . '</h3>
                        <div class="office__i__address">' . nl2br( trim( $item['address'] ) ) . '</div>
                    </div>
                </div>';
            }
            
            $offices .= '</div></div>';

            ?>

            <div class="offices offices--<?php echo ( isset( $settings['align'] ) && $settings['align'] != '' ? trim( $settings['align'] ) : 'up' ) . ( isset( $settings['schema'] ) && $settings['schema'] != '' ? ' offices--' . trim( $settings['schema'] ) : '' ); ?> content">
                
                <?php if ( isset( $settings['align'] ) && in_array( $settings['align'], [ 'left', 'right' ] ) && isset( $settings['listing'] ) && $settings['listing'] == 'yes' ) { ?><div class="pure-g"><?php }
                                                                            
                    if ( ( !isset( $settings['align'] ) || $settings['align'] != 'bottom' ) && isset( $settings['listing'] ) && $settings['listing'] == 'yes' ) { 
                        
                        if ( in_array( $settings['align'], [ 'left', 'right' ] ) ) { ?><div class="pure-u-1 pure-u-v700-8-24 pure-u-v900-6-24<?php if ( $settings['align'] == 'right' ) { ?> pure-push-v900-18-24 pure-push-v700-16-24<?php } ?>"><?php }
                
                            echo $offices;
                
                        if ( in_array( $settings['align'], [ 'left', 'right' ] ) ) { ?></div><?php } 
                    
                    } 
                                                                            
                    if ( isset( $settings['align'] ) && in_array( $settings['align'], [ 'left', 'right' ] ) && isset( $settings['listing'] ) && $settings['listing'] == 'yes' ) { ?><div class="pure-u-1 pure-u-v700-16-24 pure-u-v900-18-24<?php if ( $settings['align'] == 'right' ) { ?> pure-pull-v900-6-24 pure-pull-v700-8-24 <?php } ?>"><?php } ?>
                        
                        <div class="offices__map-wrap">
                            <div class="jq_autoGMap offices__map" 
                             data-id="myMap"
                             data-title="Tohle je marker"
                             data-window-class="mojeKlasa"
                             <?php if ( count( $settings['items'] ) == 1 && isset( $settings['items'][0]['gps'] ) && !empty( $settings['items'][0]['gps'] ) ) { ?>
                             data-lat="<?php echo $settings['items'][0]['gps']['lat']; ?>"
                             data-lng="<?php echo $settings['items'][0]['gps']['lng']; ?>"
                             data-zoom="9"
                             <?php } else { ?>
                             data-fitmarkers="1"
                             <?php } ?>
                             data-own-gui="0"
                             data-no-gui="1">
                                
                                <?php
                                    
                                    foreach ( $settings['items'] as $k => $item ) {
                                        if ( !isset( $item['title'] ) || $item['title'] == '' || !isset( $item['gps'] ) || empty( $item['gps'] ) ) continue;
                                        
                                        echo '<div class="offices__map__marker">
                                            <span class="jq_mapMarker" 
                                            data-id="marker' . $k . '"
                                            data-lat="' . $item['gps']['lat'] . '" 
                                            data-lng="' . $item['gps']['lng'] . '" 
                                            data-title="' . $item['title'] . '"
                                            ' . (  isset( $item['pin'] ) && !empty( $item['pin']['url'] ) ? ' data-icon="' . $item['pin']['url'] . '"' : '' ) . '>
                                                <h4>' . $item['title'] . '</h4>
                                                ' . ( isset( $item['address'] ) && $item['address'] != '' ? nl2br( trim( $item['address'] ) ) : '' ) . '
                                            </span>
                                        </div>';
                                            
                                    }
            
                                ?>
                                
                            </div>
	                   </div>                        
                
                    <?php if ( isset( $settings['align'] ) && in_array( $settings['align'], [ 'left', 'right' ] ) && isset( $settings['listing'] ) && $settings['listing'] == 'yes' ) { ?></div><?php } 
                                                                            
                    if ( isset( $settings['align'] ) && $settings['align'] == 'bottom' && isset( $settings['listing'] ) && $settings['listing'] == 'yes' ) { 
                        echo $offices;
                    } ?>
                
                </div>
                
            </div>
           
        <?php }
        
	}

	protected function _content_template() {}
    
    protected function get_gps_address( $item ) {
        if ( !isset( $item['address'] ) || empty( $item['address'] ) ) return [];
        
        if ( isset( $item['gps'] ) && $item['gps'] != '' ) {
            $gps = explode( ',', $item['gps'] );
            if ( isset( $gps[0] ) && isset( $gps[1] ) ) {
                return [
                    'lat' => trim( $gps[0] ),
                    'lng' => trim( $gps[1] )
                ];
            } else {
                return [];
            }
        } else {
            $address = str_replace( " ", "+", $item['address'] );

            $json = file_get_contents( "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false" );
            $json = json_decode($json);
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $lng = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            
            if ( isset( $lat ) && isset( $lng ) ) {
                return [
                    'lat' => trim( $lat ),
                    'lng' => trim( $lng )
                ];
            } else {
                return [];
            } 
        }
        
    }
}
