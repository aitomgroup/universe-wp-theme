<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Person extends Widget_Base {

	public function get_name() {
		return 'person';
	}

	public function get_title() {
		return __( 'Person', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-person';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_person',
			[
				'label' => __( 'Person', LWD_ELEM_DOMAIN ),
			]
		);
        
        $this->add_control(
			'name',
			[
				'label' => __( 'Name and Surname', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'John Doe', LWD_ELEM_DOMAIN ),
                'label_block' => true
			]
		);
        
        $this->add_control(
			'image',
			[
				'label' => __( 'Select an image', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://placeholdit.imgix.net/~text?txtsize=30&txt=300%C3%97300&w=300&h=300',
				],
			]
		);
        
        $this->add_control(
			'perex',
			[
				'label' => __( 'Short caption', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => '',
                'label_block' => true
			]
		);
        
        $this->add_control(
			'phone',
			[
				'label' => __( 'Phone', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
                'label_block' => true
			]
		);
        
        $this->add_control(
			'email',
			[
				'label' => __( 'E-mail', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'john.doe@mail.co.uk', LWD_ELEM_DOMAIN ),
                'label_block' => true
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'align',
			[
				'label' => __( 'Picture alignment', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Center', LWD_ELEM_DOMAIN ),
					'left' => __( 'Left', LWD_ELEM_DOMAIN ),
                    'right' => __( 'Right', LWD_ELEM_DOMAIN )
				],
				'default' => '',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		//$editor_content = $this->parse_text_editor( $editor_content );
        
        ?>

		<div class="person person--1 person--<?php echo ( isset( $settings['align'] ) && $settings['align'] != '' ? $settings['align'] : 'center' ); ?>"> 
            <div class="person__i js_loadFadeDelayed">
                
                <?php if ( isset( $settings['align'] ) && ( $settings['align'] == 'left' || $settings['align'] == 'right' ) ) { ?>
                    <div class="pure-g">
                        <div class="pure-u-12-24<?php echo ( isset( $settings['align'] ) && $settings['align'] == 'right' ? ' pure-push-12-24' : '' ); ?>">
                <?php } ?>
                        
                <?php if ( isset( $settings['image'] ) && $settings['image'] != '' ) {
                    $image = isset( $settings['image']['id'] ) && $settings['image']['id'] != '' ? wp_get_attachment_image_src( $settings['image']['id'], [300, 300] )[0] : $settings['image']['url']; ?>
                    <div class="person__image">					
                        <div class="person__image__wrap">
                            <noscript>
                                <img class="lazyload" src="<?php echo $image; ?>" alt="<?php echo trim( $settings['name'] ); ?>"/>	
                            </noscript>
                            <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $image; ?>" alt="<?php echo trim( $settings['name'] ); ?>"/> 
                        </div>
                    </div>
                <?php } ?>
                            
				<?php if ( isset( $settings['align'] ) && ( $settings['align'] == 'left' || $settings['align'] == 'right' ) ) { ?>
				        </div>	
				        <div class="pure-u-12-24<?php echo ( isset( $settings['align'] ) && $settings['align'] == 'right' ? ' pure-pull-12-24' : '' ); ?>">
					       <div class="person__wrap">
				<?php } ?>
                        
					<div class="person__information">
                            
						<?php if ( isset( $settings['name'] ) && $settings['name'] != '' ) { ?><h3 class="person__name"><?php echo trim( $settings['name'] ); ?></h3><?php } ?>
                        
						<?php if ( isset( $settings['perex'] ) && $settings['perex'] != '' ) { ?><span class="person__perex"><?php echo trim( $settings['perex'] ); ?></span><?php } ?>
                        
						<?php if ( isset( $settings['phone'] ) && $settings['phone'] != '' ) { ?><a href="tel:<?php echo lwd_format_phone( trim( $settings['phone'] ), false ); ?>" class="person__phone"><?php echo lwd_format_phone( trim( $settings['phone'] ) ); ?></a><?php } ?>
                        
						<?php if ( isset( $settings['email'] ) && $settings['email'] != '' ) { ?><a href="mailto:<?php echo trim( $settings['email'] ); ?>" class="person__email"><?php echo trim( $settings['email'] ); ?></a><?php } ?>
					</div>
			
                <?php if ( isset( $settings['align'] ) && ( $settings['align'] == 'left' || $settings['align'] == 'right' ) ) { ?>
                        </div>
                    </div>
                </div>
                <?php } ?>	
                
            </div>
        </div>

		<?php
	}

	protected function _content_template() {
		?>
		<div class="person person--1 person--<# if ( settings.align ) {#>{{{ settings.align }}}<# } else { #>center<# } #>"> 
            <div class="person__i js_loadFadeDelayed">
                
                <# if ( settings.align && ( settings.align == 'left' || settings.align == 'right' ) ) { #>
                    <div class="pure-g">
                        <div class="pure-u-12-24<# if ( settings.align && settings.align == 'right' ) {#> pure-push-12-24<# } #>">
                <# } #>
                        
                <# if ( settings.image && settings.image.url ) {
                    var image = {
                        id: settings.image.id,
                        url: settings.image.url,
                        size: 'hero-half',
                        model: editModel
                    };

                    var image_url = elementor.imagesManager.getImageUrl( image ); #>
                    <div class="person__image">					
                        <div class="person__image__wrap">
                            <img src="{{ image_url }}" alt="{{  settings.name }}"/> 
                        </div>
                    </div>
                <# } #>
                            
				<# if ( settings.align && ( settings.align == 'left' || settings.align == 'right' ) ) { #>
				        </div>	
				        <div class="pure-u-12-24<# if ( settings.align && settings.align == 'right' ) {#> pure-pull-12-24<# } #>">
					       <div class="person__wrap">
				<# } #>
                        
					<div class="person__information">
                            
						<# if ( settings.name ) { #><h3 class="person__name">{{  settings.name }}</h3><# } #>
                        
						<# if ( settings.perex ) { #><span class="person__perex">{{{ settings.perex }}}</span><# } #>
                        
						<# if ( settings.phone ) { #><a href="#" class="person__phone">{{{ settings.phone }}}</a><# } #>
                        
						<# if ( settings.email ) { #><a href="#" class="person__email">{{{ settings.email }}}</a><# } #>
					</div>
			
                <# if ( settings.align && ( settings.align == 'left' || settings.align == 'right' ) ) { #>
                        </div>
                    </div>
                </div>
                <# } #>
                
            </div>
        </div>
		<?php
	}
}
