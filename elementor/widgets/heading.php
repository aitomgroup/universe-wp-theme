<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Heading extends Widget_Base {

	public function get_name() {
		return 'heading';
	}

	public function get_title() {
		return __( 'Title', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-type-tool';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your headline here', LWD_ELEM_DOMAIN ),
				'default' => __( 'This is the title module', LWD_ELEM_DOMAIN ),
			]
		);
        
        global $elem_link_type;
        
        if ( !empty( $elem_link_type ) ) {
            
            $this->add_control(
			    'link_to',
                [
                    'label' => __( 'Link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::SWITCHER,
                    'default' => '',
                    'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
                    'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
                    'return_value' => 'custom',
                ]
            );
            
            $this->add_control(
                'link_type',
                [
                    'label' => __( 'Type of link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::SELECT,
                    'default' => 'external',
                    'options' => $elem_link_type,
                    'condition' => [
                        'link_to' => 'custom',
                    ],
                ]
            );
            
            $this->add_control(
                'link_internal',
                [
                    'label' => __( 'Internal link', LWD_ELEM_DOMAIN ),
                    'type' => 'internal_url',
                    'placeholder' => '',
                    'condition' => [
                        'link_to' => 'custom',
                        'link_type' => 'internal',
                    ],
                    'show_label' => false,
                ]
            );
            
            $this->add_control(
                'link',
                [
                    'label' => __( 'External link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::URL,
                    'placeholder' => __( 'http://www.example.com', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_to' => 'custom',
                        'link_type' => 'external',
                    ],
                    'show_label' => false,
                ]
            );
            
            $this->add_control(
                'link_phone',
                [
                    'label'       => '',
                    'description' => __( 'Enter the phone number', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::TEXT,
                    'placeholder' => __( '+44 02 7778 8855', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_to' => 'custom',
                        'link_type' => 'phone',
                    ],
                    'label_block' => true,
                ]
            );
            
            $this->add_control(
                'link_email',
                [
                    'label'       => '',
                    'description' => __( 'Enter an email address', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::TEXT,
                    'placeholder' => __( 'john.doe@mail.co.uk', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_to' => 'custom',
                        'link_type' => 'e-mail',
                    ],
                    'label_block' => true,
                ]
            );
            
        } else {
            $this->add_control(
                'link',
                [
                    'label' => __( 'Link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::URL,
                    'placeholder' => __( 'http://www.example.com', LWD_ELEM_DOMAIN ),
                    'default' => [
                        'url' => '',
                    ],
                    'separator' => 'before',
                ]
            );
        }
        
		$this->add_control(
			'header_size',
			[
				'label' => __( 'HTML Tag', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => 'H1',
                    'h2' => 'H2',
                    'h3' => 'H3',
                    'h4' => 'H4',
                    'h5' => 'H5',
                    'h6' => 'H6'
				],
				'default' => 'h2',
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justify', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Text color', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
				    'type' => Scheme_Color::get_type(),
				    'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-heading-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		if ( empty( $settings['title'] ) )
			return;

		$this->add_render_attribute( 'heading', 'class', 'elementor-heading-title' );

		$title = $settings['title'];
        
        $link = $this->get_link_url( $settings );

		if ( $link ) {
			$this->add_render_attribute( 'url', 'href', $link['url'] );

			if ( isset( $link['is_external'] ) && $link['is_external'] ) {
				$this->add_render_attribute( 'url', 'target', '_blank' );
			}

			$title = sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( 'url' ), $title );
		}

		$title_html = sprintf( '<%1$s %2$s>%3$s</%1$s>', $settings['header_size'], $this->get_render_attribute_string( 'heading' ), $title );

		echo $title_html;
	}

	protected function _content_template() {
		?>
		<#
			var title = settings.title;

			if ( '' !== settings.link.url ) {
				title = '<a href="' + settings.link.url + '">' + title + '</a>';
			}

			var title_html = '<' + settings.header_size  + ' class="elementor-heading-title">' + title + '</' + settings.header_size + '>';

			print( title_html );
		#>
		<?php
	}
    
    private function get_link_url( $instance ) {

		if ( 'none' === $instance['link_to'] ) {
			return false;
		}

		if ( 'custom' === $instance['link_to'] ) {
            
            $link_data = [];
            $is_external = false;
			
            foreach( $instance as $option => $value ) {
                if ( strpos( $option, 'link' ) === 0 && $value != '' ) {
                    if ( $option == 'link_to' ) continue;
                    
                    if ( $option == 'link_type' ) {
                        $link_data['link_data'] = $value;
                        continue;
                    }
                    
                    if ( $option == 'link' ) {
                        if ( isset( $value['url'] ) && $value['url'] != '' ) {
                            $link_data[ 'link_data-external' ] = $value['url'];
                            if ( isset( $value['is_external'] ) && $value['is_external'] ) $is_external = true;
                        } else {
                            continue;
                        }
                    } else {
                        $link_data[ str_replace( '_', '_data-', $option ) ] = $value;
                    }
                } 
            }

            if ( empty( $link_data ) ) {
				return false;
			}
            
            $link_data = lwd_get_link( 1, 'link_data', $link_data );
            
            if ( $is_external ) $link_data['is_external'] = true;
            
            return $link_data;

		}

		return false;
	}
}
