<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Shortcode extends Widget_Base {

	public function get_name() {
		return 'shortcode';
	}

	public function get_title() {
		return __( 'Shortcode', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-shortcode';
	}

	public function is_reload_preview_required() {
		return true;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_shortcode',
			[
				'label' => __( 'Shortcode', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'shortcode',
			[
				'label' => __( 'Put your shortcode here', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => '[gallery id="123" size="medium"]',
				'default' => '',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$shortcode = $this->get_settings( 'shortcode' );

		echo do_shortcode( shortcode_unautop( $shortcode ) );
	}

	public function render_plain_content() {
		// In plain mode, render without shortcode
		echo $this->get_settings( 'shortcode' );
	}

	protected function _content_template() {}
}
