<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Wpcf7 extends Widget_Base {

	public function get_name() {
		return 'contact-form-7';
	}

	public function get_title() {
		return __( 'Form', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-form-horizontal';
	}

	public function is_reload_preview_required() {
		return true;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_shortcode',
			[
				'label' => __( 'Form', LWD_ELEM_DOMAIN ),
			]
		);
        
        $this->add_control(
			'shortcode_description',
			[
				'raw' => sprintf( __( 'Beware: you must have already create at least one form <a href="%1$s" target="_blank">here</a>. If you do not have one, <a href="%1$s" target="_blank">create it</a>.', LWD_ELEM_DOMAIN ), admin_url( 'admin.php?page=wpcf7' ) ),
				'type' => Controls_Manager::RAW_HTML,
				'classes' => 'elementor-descriptor',
			]
		);
        
        $forms = get_posts( array(
            'post_type'         => 'wpcf7_contact_form', 
            'post_status'       => 'publish'
        ) );
        
        $forms_options = [
            '' => __( '-- Select an option --', LWD_ELEM_DOMAIN )
        ];
        
        if ( !empty( $forms ) ) {
            foreach ( $forms as $form ) {
                if ( isset( $form->ID ) && isset( $form->post_title ) ) $forms_options['[contact-form-7 id="' . $form->ID . '"]'] = $form->post_title;
            }
        }

		$this->add_control(
			'shortcode',
			[
				'label' => __( 'Select a form from the menu', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => $forms_options,
                'label_block' => true
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$shortcode = $this->get_settings( 'shortcode' );

		echo do_shortcode( shortcode_unautop( $shortcode ) );
	}

	public function render_plain_content() {
		// In plain mode, render without shortcode
		echo $this->get_settings( 'shortcode' );
	}

	protected function _content_template() {}
}
