<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Accordion extends Widget_Base {

	public function get_name() {
		return 'accordion';
	}

	public function get_title() {
		return __( 'Accordion', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-accordion';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Accordion', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => __( 'Accordion items', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'tab_title' => __( 'Accordion #1', LWD_ELEM_DOMAIN ),
						'tab_content' => __( 'I am the content of this item. Click on Edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', LWD_ELEM_DOMAIN ),
					],
					[
						'tab_title' => __( 'Accordion #2', LWD_ELEM_DOMAIN ),
						'tab_content' => __( 'I am the content of this item. Click on Edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', LWD_ELEM_DOMAIN ),
					],
				],
				'fields' => [
					[
						'name' => 'tab_title',
						'label' => __( 'Title and content', LWD_ELEM_DOMAIN ),
						'type' => Controls_Manager::TEXT,
						'default' => __( 'Item title' , LWD_ELEM_DOMAIN ),
						'label_block' => true,
					],
					[
						'name' => 'tab_content',
						'label' => __( 'Content', LWD_ELEM_DOMAIN ),
						'type' => Controls_Manager::WYSIWYG,
						'default' => __( 'Item content', LWD_ELEM_DOMAIN ),
						'show_label' => false,
					],
				],
				'title_field' => '{{{ tab_title }}}',
			]
		);
        
        $this->add_control(
            'tabs_toggle',
            [
                'label' => __( 'Allow open all panels', LWD_ELEM_DOMAIN ),
                'type' => Controls_Manager::SWITCHER,
                'default' => '',
                'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
                'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
            ]
        );
        
        $this->add_control(
            'tabs_active_panel',
            [
                'label' => __( 'Active panel', LWD_ELEM_DOMAIN ),
                'type' => Controls_Manager::TEXT,
                'default' => '',
                'description' => __( 'Set number of item that you want to open when the page is loaded. Leave the field blank to close all panels when the page is loaded.', LWD_ELEM_DOMAIN )
            ]
        );

		$this->add_control(
			'view',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();
        $key = 1; ?>

		<div class="accordion">
            <div class="accordion__list">
                
			<?php foreach ( $settings['tabs'] as $item ) { ?>
                <?php if ( isset( $settings['tabs_toggle'] ) && $settings['tabs_toggle'] == 'yes' ){ ?>
                    <input class="accordion__input" type="checkbox" id="item-<?php echo $key; ?>""<?php echo ( isset( $settings['tabs_active_panel'] ) && $settings['tabs_active_panel'] == $key ? ' checked="checked"' : '' ); ?>/>
                <?php } else { ?>
                    <input class="accordion__input" name="accordionradio" type="radio" id="item-<?php echo $key; ?>"<?php echo ( isset( $settings['tabs_active_panel'] ) && $settings['tabs_active_panel'] == $key ? ' checked="checked"' : '' ); ?>/>
                <?php } ?>
                <label class="accordion__i js_loadFadeDelayed" for="item-<?php echo $key; ?>">
                    <div class="accordion__header js-accordion">
                        <h3 class="accordion__title"><?php echo $item['tab_title']; ?></h3>
                        <div class="accordion__arrow"></div>	
                    </div>
                    <div class="accordion__content">
                        <div class="accordion__wrap">
                            <?php echo $this->parse_text_editor( $item['tab_content'] ); ?>
                        </div>	
                    </div>
                </label><?php
                $key++;
            } ?>
                
            </div>
		</div>

		<?php
	}

	protected function _content_template() {
		?>
		<div class="accordion"><div class="accordion__list">
			<#
			if ( settings.tabs ) {
				var counter = 1;
				_.each( settings.tabs, function( item ) { #>
                    <input class="accordion__input" name="accordionradio" type="radio" id="item-{{ counter }}"{{ editSettings.activeItemIndex == counter ? ' checked="checked"' : '' }}/>
					<label class="accordion__i js_loadFadeDelayed" for="item-{{ counter }}">
                        <div class="accordion__header js-accordion">
                            <h3 class="accordion__title">{{{ item.tab_title }}}</h3>
                            <div class="accordion__arrow"></div>	
                        </div>
                        <div class="accordion__content">
                            <div class="accordion__wrap">
                                {{{ item.tab_content }}}
                            </div>	
                        </div>
                    </label>
				<#
					counter++;
				} );
			} #>
        </div></div>
		<?php
	}
}
