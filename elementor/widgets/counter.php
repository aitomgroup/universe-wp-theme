<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Counter extends Widget_Base {

	public function get_name() {
		return 'counter';
	}

	public function get_title() {
		return __( 'Counter', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-counter';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_counter',
			[
				'label' => __( 'Counter', LWD_ELEM_DOMAIN ),
			]
		);
        
        $this->add_control(
			'counters',
			[
				'label' => __( 'Items', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					'counter_number' => 199,
                    'counter_unit' => '',
                    'counter_prefix' => '',
                    'counter_desc' => _x( 'This is the counter label. Click on Edit button to change this text.', 'counter example description', LWD_ELEM_DOMAIN )
				],
				'fields' => [
					[
                        'name' => 'counter_number',
                        'label' => __( 'Number', LWD_ELEM_DOMAIN ),
                        'type' => Controls_Manager::NUMBER,
                    ],
					[
						'name' => 'counter_unit',
						'label' => __( 'Unit', LWD_ELEM_DOMAIN ),
						'type' => Controls_Manager::TEXT
					],
                    [
						'name' => 'counter_prefix',
						'label' => __( 'Text prefix', LWD_ELEM_DOMAIN ),
						'type' => Controls_Manager::TEXT
					],
                    [
						'name' => 'counter_desc',
						'label' => __( 'Description', LWD_ELEM_DOMAIN ),
						'type' => Controls_Manager::WYSIWYG,
						'default' => '',
						'show_label' => false,
					]
				],
				'title_field' => '{{{ counter_prefix }}}{{{ counter_number }}} {{{ counter_unit }}}',
			]
		);
        
        $this->add_control(
			'columns',
			[
				'label' => __( 'Number of columns', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4
				],
				'default' => 1,
			]
		);
        
        $this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					]
				],
				'default' => 'center',
			]
		);
        
        $this->add_control(
			'schema',
			[
				'label' => __( 'Color scheme', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Dark scheme', LWD_ELEM_DOMAIN ),
					'light' => __( 'Bright scheme', LWD_ELEM_DOMAIN )
				],
				'default' => '',
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();
	}

	protected function _content_template() {
		?>
        <div class="countup countup--<# if ( settings.columns ) {#>{{{ settings.columns }}}<# } else { #>1<# } #> countup--<# if ( settings.align ) {#>{{{ settings.align }}}<# } else { #>center<# } #><# if ( settings.schema ) {#> countup--{{{ settings.schema }}}<# } #>">
            <div class="pure-g">
                <div class="countup__list content">
                    <# if ( settings.counters ) {
                        var counter = 1;
                        _.each( settings.counters, function( item ) {

                       switch ( settings.columns ) {
                            case '4' :
                                var col = ' pure-u-v1100-6-24  pure-u-v600-12-24';
                                break;
                            case '3' :
                                var col = ' pure-u-v900-8-24 pure-u-v600-12-24';
                                break;
                            case '2' :
                                var col = ' pure-u-v600-12-24';
                                break;
                            case '1' : default :
                                var col = '';
                                break;
                       } #>

                            <div class="countup__i js_loadFade pure-u-1{{ col }}">
                                <div class="countup__content">
                                    <h3 class="countup__count">
                                        <# if ( item.counter_prefix ) {#><span class="countup__unit">{{{ item.counter_prefix }}}</span><# } #>
                                        <span class="js_countUp" id="number-{{ counter }}">{{ item.counter_number }}</span>
                                        <# if ( item.counter_unit ) {#><span class="countup__unit">{{{ item.counter_unit }}}</span><# } #>
                                    </h3>

                                    <# if ( item.counter_desc) {#><div class="countup__desc">{{{ item.counter_desc }}}</div><# } #>
                                </div>
                            </div>

                    <#
                        counter++;
                    } );
                } #>
                </div>
            </div>
        </div>
		<?php
	}

	public function render() {
		$settings = $this->get_settings(); ?>
                
        <div class="countup countup--<?php echo ( isset( $settings['columns'] ) && $settings['columns'] != '' ? $settings['columns'] : 1 ) . ' countup--' . ( isset( $settings['align'] ) && $settings['align'] != '' ? $settings['align'] : 'center' ) . ( isset( $settings['schema'] ) && $settings['schema'] != '' ? ' countup--' . $settings['schema'] : '' ); ?>">
            <div class="pure-g">
                <div class="countup__list content">
                    <?php if ( !empty( $settings['counters'] ) ) { foreach ( $settings['counters'] as $key => $item ) {
                        if ( !isset( $item['counter_number'] ) || $item['counter_number'] == '' ) continue;
                        if ( isset( $settings['columns'] ) && $settings['columns'] != '' ) {
                            switch ( $settings['columns'] ) {
                                case 4 :
                                    $col = 'pure-u-v1100-6-24  pure-u-v600-12-24';
                                    break;
                                case 3 :
                                    $col = 'pure-u-v900-8-24 pure-u-v600-12-24';
                                    break;
                                case 2 :
                                    $col = 'pure-u-v600-12-24';
                                    break;
                                case 1 : default :
                                    $col = '';
                                    break;
                            }
                        } ?>

                        <div class="countup__i js_loadFade pure-u-1<?php echo ( $col != '' ? ' ' . $col : '' ); ?>">
                            <div class="countup__content">
                                <h3 class="countup__count">
                                    <?php if ( isset( $item['counter_prefix'] ) ) { ?><span class="countup__unit"><?php echo $item['counter_prefix']; ?></span><?php } ?>
                                    <span class="js_countUp" id="number-<?php echo $key; ?>"><?php echo $item['counter_number']; ?></span>
                                    <?php if ( isset( $item['counter_unit'] ) ) { ?><span class="countup__unit"><?php echo $item['counter_unit']; ?></span><?php } ?>
                                </h3>

                                <?php if ( isset( $item['counter_desc'] ) ) { ?><div class="countup__desc"><?php echo $this->parse_text_editor( $item['counter_desc'] ); ?></div><?php } ?>
                            </div>
                        </div>

                    <?php } } ?>
                </div>
            </div>
        </div>
		
		<?php
	}
}
