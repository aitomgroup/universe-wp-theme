<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Icon_Box extends Widget_Base {

	public function get_name() {
		return 'icon-box';
	}

	public function get_title() {
		return __( 'Icon box', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-icon-box';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_editor',
			[
				'label' => __( 'Icon box', LWD_ELEM_DOMAIN ),
			]
		);
        
        $this->add_control(
			'title',
			[
				'label' => __( 'Title', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your headline here', LWD_ELEM_DOMAIN ),
				'default' => __( 'This is the title', LWD_ELEM_DOMAIN ),
                'label_block' => true
			]
		);
        
        $this->add_control(
			'type',
			[
				'label' => __( 'Box type', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'text' => __( 'Text or number', LWD_ELEM_DOMAIN ),
					'icon' => __( 'Icon', LWD_ELEM_DOMAIN )
				],
				'default' => 'text'
			]
		);
        
         $this->add_control(
			'icon_text',
			[
				'label' => '',
				'type' => Controls_Manager::TEXT,
				'placeholder' => '',
				'default' => 1,
                'label_block' => true,
                'condition' => [
                    'type' => 'text'
                ],
                'show_label' => false
			]
		);
        
        $this->add_control(
			'icon_icon',
			[
				'label' => '',
				'type' => Controls_Manager::ICON,
                'condition' => [
                    'type' => 'icon'
                ],
                'label_block' => true
			]
		);

		$this->add_control(
			'editor',
			[
				'label' => '',
				'type' => Controls_Manager::WYSIWYG,
				'default' => __( "I'm a block of text. Click on Edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.", LWD_ELEM_DOMAIN ),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					],
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
        wp_enqueue_style( LWD_SLUG . '-font-awesome' );
        
		$settings = $this->get_settings(); ?>

		<div class="numbered numbered--1 numbered--<?php echo ( isset( $settings['align'] ) && $settings['align'] != '' ? $settings['align'] : 'center' ); ?>">
            <div class="numbered__list">
                <div class="numbered__i">
                    <div class="numbered__wrap">
                        
                        <?php
        
                            $number = '<div class="numbered__number"><div class="numbered__number__wrap"><div class="numbered__number__i"><div class="numbered__number__border"><span class="numbered__number__value">' . ( isset( $settings['type'] ) && $settings['type'] == 'icon' && isset( $settings['icon_icon'] ) ? '<i class="' . $settings['icon_icon'] . '"></i>' : ( isset( $settings['icon_text'] ) ? trim( $settings['icon_text'] ) : '' ) ) . '</span></div></div></div></div>';
        
                            echo sprintf( isset( $settings['align'] ) && $settings['align'] == 'right' ? '%1$s%2$s' : '%2$s%1$s', '<div class="numbered__content">' . ( isset( $settings['title'] ) && $settings['title'] != '' ? '<h3 class="numbered__title">' . trim( $settings['title'] ) . '</h3>' : '' ) . ( isset( $settings['editor'] ) && $settings['editor'] != '' ? '<div class="numbered__desc">' . $this->parse_text_editor( $settings['editor'] ) . '</div>' : '' ) . '</div>', $number );
        
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>

		<?php
	}

	public function render_plain_content() {}

	protected function _content_template() {}
}
