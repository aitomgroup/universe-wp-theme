<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Image_Gallery extends Widget_Base {

	public function get_name() {
		return 'image-gallery';
	}

	public function get_title() {
		return __( 'Gallery', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-gallery-grid';
	}
    
    public function is_reload_preview_required() {
		return true;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_gallery',
			[
				'label' => __( 'Gallery', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'wp_gallery',
			[
				'label' => __( 'Add images', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::GALLERY,
			]
		);

		$this->add_control(
			'gallery_link',
			[
				'label' => __( 'Link', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'default' => 'imageVideo',
				'options' => [
                    'imageVideo' => __( 'Image / video', LWD_ELEM_DOMAIN ),
					'file' => __( 'Media file', LWD_ELEM_DOMAIN ),
					'none' => __( 'No link', LWD_ELEM_DOMAIN ),
				],
			]
		);
        
        $this->add_control(
			'gallery_rand',
			[
				'label' => __( 'Random order', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
				'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
				'return_value' => 'rand',
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		if ( ! $settings['wp_gallery'] ) {
			return;
		}

		$ids = wp_list_pluck( $settings['wp_gallery'], 'id' );

		$this->add_render_attribute( 'shortcode', 'ids', implode( ',', $ids ) );

		if ( $settings['gallery_link'] ) {
			$this->add_render_attribute( 'shortcode', 'link', $settings['gallery_link'] );
		}

		if ( ! empty( $settings['gallery_rand'] ) ) {
			$this->add_render_attribute( 'shortcode', 'orderby', $settings['gallery_rand'] );
		}
		
        echo do_shortcode( '[gallery ' . $this->get_render_attribute_string( 'shortcode' ) . ']' );
        
	}

	public function render_plain_content() {
		// In plain mode, render without shortcode
		echo do_shortcode( '[gallery ids="' . implode( ',', wp_list_pluck( $this->get_settings( 'wp_gallery' ) ) ) . '"]' );
	}

	protected function _content_template() {}
}
