<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Testimony extends Widget_Base {

	public function get_name() {
		return 'testimony';
	}

	public function get_title() {
		return __( 'Testimony', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-testimonial';
	}

	public function is_reload_preview_required() {
		return true;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_posts',
			[
				'label' => __( 'Listing of testimonies', LWD_ELEM_DOMAIN ),
			]
		);
        
        $this->add_control(
			'cols',
			[
				'label' => __( 'Number of columns', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
                'max' => 4,
                'step' => 1,
				'default' => 3,
			]
		);
        
        $this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					]
				],
				'default' => 'center',
			]
		);
        
        $this->add_control(
			'cat',
			[
				'label' => __( 'Include categories', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT2,
				'options' => get_terms( [
                    'taxonomy' => 'testimony_cat',
                    'hide_empty' => true,
                    'fields' => 'id=>name'
                ] ),
                'default' => [],
                'multiple' => true,
                'label_block' => true,
                'description' => __( 'Select categories from which display testimonies. To display all, leave the field blank.', LWD_ELEM_DOMAIN )
			]
		);
        
        $this->add_control(
            'filter',
            [
                'label' => __( 'Category filter', LWD_ELEM_DOMAIN ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'off',
                'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
                'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
            ]
        );
        
        $this->add_control(
			'count',
			[
				'label' => __( 'Number of testimonies on one page', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
                'max' => 999,
                'step' => 1,
				'default' => get_option( 'posts_per_page', 12 ),
			]
		);
        
        $this->add_control(
            'paginavi',
            [
                'label' => __( 'Pagination', LWD_ELEM_DOMAIN ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'off',
                'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
                'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
            ]
        );
        
        $this->add_control(
			'sortby',
			[
				'label' => __( 'Sort by', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'date' => __( 'By date', LWD_ELEM_DOMAIN ),
                    'title' => __( 'By title', LWD_ELEM_DOMAIN ),
                    'rand' => __( 'Random', LWD_ELEM_DOMAIN )
				],
				'default' => 'date',
			]
		);
        
         $this->add_control(
			'sort',
			[
				'label' => '',
				'type' => Controls_Manager::SELECT,
				'options' => [
					'DESC' => __( 'Descending', LWD_ELEM_DOMAIN ),
                    'ASC' => __( 'Ascending', LWD_ELEM_DOMAIN )
				],
				'default' => 'DESC',
                'condition' => [
                    'sortby!' => 'rand',
                ],
			]
		);

		$this->end_controls_section();
        
        $this->start_controls_section(
			'section_posts_settings',
			[
				'label' => __( 'Settings', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);
        
        $this->add_control(
			'type',
			[
				'label' => __( 'Listing style', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'g-g2' => 2,
                    'g-g3' => 3,
                    'g-g4' => 4
				],
				'default' => apply_filters( 'lwd_filter_archive_style', 'g-g2' )
			]
		);

		$this->add_control(
			'custom_query',
			[
				'label' => __( 'Query', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => '',
                'label_block' => true
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();
        
        global $wp_query, $archive_style, $archive_cols, $archive_align;
        
        $archive_style_old = $archive_style;
        $archive_cols_old = $archive_cols;
        $archive_align_old = $archive_align;
        
        $archive_style = isset( $settings['type'] ) && $settings['type'] != '' ? $settings['type'] : apply_filters( 'lwd_filter_archive_style', 'g-g2' );
        
        $archive_align = isset( $settings['align'] ) && $settings['align'] != '' ? $settings['align'] : 'center';
        
        $archive_cols = isset( $settings['cols'] ) && $settings['cols'] != '' ? $settings['cols'] : 3;
        
        $args = [
            'post_type' => 'testimony'
        ];

        if ( is_user_logged_in() && defined( 'LWD_MEMBER_ZONE' ) && LWD_MEMBER_ZONE ) $args['post_status'] = [ 'restrict', 'publish' ];
        if ( isset( $settings['count'] ) && $settings['count'] != '' ) $args['posts_per_page'] = $settings['count'];
        if ( isset( $settings['sortby'] ) && $settings['sortby'] != '' ) $args['orderby'] = $settings['sortby'];
        if ( isset( $settings['sort'] ) && $settings['sort'] != '' ) $args['order'] = $settings['sort'] == 'DESC' ? 'DESC' : 'ASC';
        
        if ( isset( $settings['paginavi'] ) && $settings['paginavi'] == 'yes' ) $args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
        
        if ( isset( $settings['cat'] ) && !empty( $settings['cat'] ) ) {
             $args['tax_query'] = [
                [
                    'taxonomy' => 'testimony_cat',
                    'field' => 'term_id',
                    'terms' => $settings['cat']
                ]
            ];
        }
        
        if ( isset( $settings['custom_query'] ) && $settings['custom_query'] != '' ) {
            $new_query = [];
            $custom_query = explode( '&', $settings['custom_query'] );
            
            if ( is_array( $custom_query ) ) {
                foreach ( $custom_query as $arg ) {
                    if ( empty( $arg ) ) continue;
                    
                    $arg = explode( '=', $arg );
                    
                    if ( isset( $arg[0] ) && isset( $arg[1] ) && $arg[1] != '' ) $new_query = $arg;
                }
            }
            
            if ( !empty( $new_query ) ) {
                $args = array_merge( $args, $new_query );
            }
        }
        
        if ( isset( $settings['filter'] ) && $settings['filter'] == 'yes' && isset( $_GET['cat'] ) && $_GET['cat'] != '' ) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'testimony_cat',
                    'field' => 'term_id',
                    'terms' => $_GET['cat']
                ]
            ];
        }
		
        query_posts( $args );

            global $wp_query; ?>

            <?php
        
                if ( isset( $settings['filter'] ) && $settings['filter'] == 'yes' ) {
                    
                    $cat_args = [
                        'taxonomy' => 'testimony_cat',
                        'hide_empty' => true
                    ];
                    
                    if ( isset( $settings['cat'] ) && !empty( $settings['cat'] ) ) {
                        $cat_args['include'] = $settings['cat'];
                    }
                    
                    $cats = get_terms( $cat_args );
                    
                    if ( !empty( $cats ) ) { ?>
                        <div class="filter">
                            <?php

                                echo '<a href="' . add_query_arg( 'cat', '' ) . '" class="filter__btn' . ( !isset( $_GET['cat'] ) || $_GET['cat'] == '' ? ' filter__all--checked' : '' ) . ' js_loadFade">' . __( 'All', LWD_TEXT_DOMAIN ) . '</a>';

                                foreach ( $cats as $cat ) {
                                    echo '<a href="' . add_query_arg( 'cat', $cat->term_id ) . '" class="filter__btn' . ( isset( $_GET['cat'] ) && $_GET['cat'] == $cat->term_id ? ' filter__all--checked' : '' ) .' js_loadFade">' . $cat->name . '</a>';
                                }
                            ?>
                        </div>
                    <?php }
                    
                }
        
            ?>

            <div class="<?php echo $archive_style != '' ? $archive_style . ( $archive_cols != '' ? ' ' . $archive_style . '--' . $archive_cols : '' ) . ( $archive_align != '' ? ' ' . $archive_style . '--' . $archive_align : '' ) : ''; ?> pure-g">

                <?php

                    /* Run the loop to output the post.
                     * If you want to overload this 
                     * in a child theme then include a file
                     * called loop-single.php and that
                     * will be used instead.
                     */

                    get_template_part( 'loop' );

                ?>

            </div>
        
            <?php 

            if ( isset( $settings['paginavi'] ) && $settings['paginavi'] == 'yes' && function_exists( 'ai_paginator' ) ) {
                ai_paginator( $wp_query->max_num_pages );
            }
        
        wp_reset_query();
        
        $archive_style = $archive_style_old;
        $archive_cols = $archive_cols_old;
        $archive_align = $archive_align_old;
        
	}

	public function render_plain_content() {}

	protected function _content_template() {}
}
