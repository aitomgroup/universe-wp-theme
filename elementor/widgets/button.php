<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Button extends Widget_Base {

	public function get_name() {
		return 'button';
	}

	public function get_title() {
		return __( 'Button', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-button';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_button',
			[
				'label' => __( 'Button', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'button_type',
			[
				'label' => __( 'Button style', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
                    '' => __( 'Classic button', LWD_TEXT_DOMAIN ),
                    'highlight' => __( 'Highlight button', LWD_TEXT_DOMAIN ),
                    'border' => __( 'Border button', LWD_TEXT_DOMAIN ),
                    'text-dark' => __( 'Black text button', LWD_TEXT_DOMAIN )
                ],
				//'prefix_class' => 'g-btn--',
			]
		);

		$this->add_control(
			'text',
			[
				'label' => __( 'Buttton text', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Click me', LWD_ELEM_DOMAIN ),
				'placeholder' => __( 'Click me', LWD_ELEM_DOMAIN )
			]
		);
        
        global $elem_link_type;
        
        if ( !empty( $elem_link_type ) ) {
            
            $this->add_control(
                'link_type',
                [
                    'label' => __( 'Type of link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::SELECT,
                    'default' => 'external',
                    'options' => $elem_link_type,
                ]
            );
            
            $this->add_control(
                'link_internal',
                [
                    'label' => __( 'Internal link', LWD_ELEM_DOMAIN ),
                    'type' => 'internal_url',
                    'placeholder' => '',
                    'condition' => [
                        'link_type' => 'internal',
                    ],
                    'show_label' => false,
                ]
            );
            
            $this->add_control(
                'link',
                [
                    'label' => __( 'External link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::URL,
                    'placeholder' => __( 'http://www.example.com', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_type' => 'external',
                    ],
                    'show_label' => false,
                ]
            );
            
            $this->add_control(
                'link_phone',
                [
                    'label'       => '',
                    'description' => __( 'Enter the phone number', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::TEXT,
                    'placeholder' => __( '+44 02 7778 8855', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_type' => 'phone',
                    ],
                    'label_block' => true,
                ]
            );
            
            $this->add_control(
                'link_email',
                [
                    'label'       => '',
                    'description' => __( 'Enter an email address', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::TEXT,
                    'placeholder' => __( 'john.doe@mail.co.uk', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_type' => 'e-mail',
                    ],
                    'label_block' => true,
                ]
            );
            
        } else {
            $this->add_control(
                'link',
                [
                    'label' => __( 'Link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::URL,
                    'placeholder' => __( 'http://www.example.com', LWD_ELEM_DOMAIN ),
                    'default' => [
                        'url' => '',
                    ],
                    'separator' => 'before',
                ]
            );
        }

		

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left'    => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					],
                    'justify' => [
						'title' => __( 'Fullwidth', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'default' => '',
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings();
        
        $link = $this->get_link_url( $settings );

		if ( $link ) {
			$this->add_render_attribute( 'button', 'href', $link['url'] );

			if ( isset( $link['is_external'] ) && $link['is_external'] ) {
				$this->add_render_attribute( 'url', 'target', '_blank' );
			}
		}

		$this->add_render_attribute( 'button', 'class', 'g-btn elementor-btn txt-center' );

        if ( $settings['button_type'] ) {
			$this->add_render_attribute( 'button', 'class', 'g-btn--' . $settings['button_type'] . ( $settings['button_type'] == 'text-dark' ? ' g-btn--border' : '' ) );
		}

        ?>
		<a <?php echo $this->get_render_attribute_string( 'button' ); ?>>
            <?php echo $settings['text']; ?>
        </a>
		<?php
	}

	protected function _content_template() {
		?><a class="g-btn elementor-btn g-btn--{{ settings.button_type }}<# if ( settings.button_type == 'text-dark' ) { #> g-btn--border<# } #> txt-center" href="{{ settings.link.url }}">{{{ settings.text }}}</a><?php
	}
    
    private function get_link_url( $instance ) {
        $link_data = [];
        $is_external = false;

        foreach( $instance as $option => $value ) {
            if ( strpos( $option, 'link' ) === 0 && $value != '' ) {
                if ( $option == 'link_to' ) continue;

                if ( $option == 'link_type' ) {
                    $link_data['link_data'] = $value;
                    continue;
                }

                if ( $option == 'link' ) {
                    if ( isset( $value['url'] ) && $value['url'] != '' ) {
                        $link_data[ 'link_data-external' ] = $value['url'];
                        if ( isset( $value['is_external'] ) && $value['is_external'] ) $is_external = true;
                    } else {
                        continue;
                    }
                } else {
                    $link_data[ str_replace( '_', '_data-', $option ) ] = $value;
                }
            } 
        }

        if ( empty( $link_data ) ) {
            return false;
        }

        $link_data = lwd_get_link( 1, 'link_data', $link_data );

        if ( $is_external ) $link_data['is_external'] = true;

        return $link_data;
	}
}
