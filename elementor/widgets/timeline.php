<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Timeline extends Widget_Base {

	public function get_name() {
		return 'timeline';
	}

	public function get_title() {
		return __( 'Timeline', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-time-line';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_timeline',
			[
				'label' => __( 'Timeline', LWD_ELEM_DOMAIN ),
			]
		);
        
        $this->add_control(
			'points',
			[
				'label' => __( 'Timeline items', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
                        'year' => date( 'Y' ),
						'name' => __( 'Event title #1', LWD_ELEM_DOMAIN ),
						'editor' => __( 'I am the content of this item. Click on Edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', LWD_ELEM_DOMAIN ),
					],
					[
                        'year' => date( 'Y' ) - 1,
						'name' => __( 'Event title #2', LWD_ELEM_DOMAIN ),
						'editor' => __( 'I am the content of this item. Click on Edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', LWD_ELEM_DOMAIN ),
					],
				],
				'fields' => [
					[
						'name' => 'year',
						'label' => __( 'Year', LWD_ELEM_DOMAIN ),
                        'type' => Controls_Manager::TEXT,
                        'label_block' => true
					],
					[
						'name' => 'name',
						'label' => __( 'Title', LWD_ELEM_DOMAIN ),
                        'type' => Controls_Manager::TEXT,
                        'label_block' => true
					],
                    [
						'name' => 'editor',
						'label' => '',
                        'type' => Controls_Manager::WYSIWYG,
					],
                    [
						'name' => 'image',
						'label' => __( 'Select an image', LWD_ELEM_DOMAIN ),
                        'type' => Controls_Manager::MEDIA,
					],
				],
				'title_field' => '{{{ year }}} {{{ name }}}',
			]
		);
        
        $this->add_control(
			'sort',
			[
				'label' => __( 'Years sorting', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'DESC' => __( 'Descending', LWD_ELEM_DOMAIN ),
					'ASC' => __( 'Ascending', LWD_ELEM_DOMAIN )
				],
				'default' => 'DESC',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		//$editor_content = $this->parse_text_editor( $editor_content );
        
        if ( isset( $settings['points'] ) && !empty( $settings['points'] ) ) {
            $years = [];
            foreach ( $settings['points'] as $point ) {
                if ( !isset( $point['year'] ) || !isset( $point['name'] ) ) continue;
                $years[ $point['year'] ][] = $point;
            }
            
            if ( !empty( $years ) ) {
                if ( isset( $settings['sort'] ) && $settings['sort'] == 'ASC' ) {
                    ksort( $years );
                } else {
                    krsort( $years );
                } ?>
                <div class="timeline content">
                    <?php
                        foreach( $years as $year ) {
                            $last_i = count( $year ) - 1;
                            foreach ( $year as $k => $i ) {
                                if ( $k == 0 )
                                    echo '<div class="timeline__items"><span class="timeline__year">' . $i['year'] . '</span>';
                                else
                                    echo '<span class="timeline__point"></span>';
                                
                                echo '<div class="timeline__i  js_timeline">
                                    <div class="timeline__i__content">
                                        <h3 class="timeline__title">' . $i['name'] . '</h3>
                                        ' . ( isset( $i['editor'] ) && $i['editor'] != '' ? '<div class="timeline__text">' . $this->parse_text_editor( $i['editor'] ) .'</div>' : '' );
                                        
                                if ( isset( $i['image'] ) && ( ( isset( $i['image']['url'] ) && $i['image']['url'] != '' ) || ( isset( $i['image']['id'] ) && $i['image']['id'] != '' ) ) ) {

                                    $image = isset( $i['image']['id'] ) && $i['image']['id'] != '' ? wp_get_attachment_image_src( $i['image']['id'], 'large' ) : [ $i['image']['url'] ];
                                    
                                    echo '<div class="timeline__image">
                                        <noscript>
                                            <img src="' . $image[0]. '" alt="' . $i['name'] . '" />
                                        </noscript>
                                        <img class="lazyload" src="' . ( isset( $image[1] ) && isset( $image[2] ) ? generate_attachment_placeholder( $image[1], $image[2] ) : '' ) . '" data-src="' . $image[0] . '" alt="' . $i['name'] . '" />
                                    </div>';
                                }
                                
                                echo '</div>
                                </div>';
                                
                                if ( $k == $last_i ) echo '</div>';
                            }
                        }
                    ?>
                </div>
            <?php }
        }

	}

	public function render_plain_content() {}

	protected function _content_template() { ?>
        <# if ( settings.points ) { #>
            <div class="timeline content">
				<# 
                   var years = [];
                   
                   _.each( settings.points, function( item ) {
                        if ( typeof item.year == 'undefined' || typeof item.name == 'undefined' ) return true;
                        var current_year = years[ item.year ] !== undefined ? years[ item.year ] : [];
                        current_year.push( item );
                        years[ item.year ] = current_year; 
                   } );
                   
                   if ( years.length > 0 ) {
                        function sortYears( obj, dir ){
                            var keys = Object.keys(obj).sort(),
                                sortedObj = [];
                    
                            for( var i in keys ) {
                                sortedObj[keys[i]] = obj[keys[i]];
                            }
                    
                            if ( dir == 'DESC' ) {
                                sortedObj.reverse();
                            }
                    
                            return sortedObj;
                        }
                    
                        years = sortYears( years, settings.sort ); #>
                        <div class="timeline content">
                            <# _.each( years, function( year ) {
                               _.each( year, function( i, k ) { #>
                               
                                     <# if ( k == 0 ) { #>
                                         <div class="timeline__items"><span class="timeline__year">{{ i.year }}</span>
                                     <# } else { #>
                                         <span class="timeline__point"></span>
                                     <# } #>
                                         
                                    <div class="timeline__i  js_timeline">
                                        <div class="timeline__i__content">
                                            <h3 class="timeline__title">{{ i.name }}</h3>
                                            <div class="timeline__text">{{{ i.editor }}}</div>
                                            
                                            <# if ( i.image && i.image.url && typeof i.image.url !== 'undefined' ) { #>
                                                <div class="timeline__image">
                                                    <img src="{{ i.image.url }}" alt="{{  i.name }}"/> 
                                                </div>
                                            <# } #>
                                            
                                        </div>
                                    </div>
                                         
                                     <# if ( k == year.length - 1 ) { #></div><# } #>
                                
                               <# } );
                            } ); #>
                        </div>
                   <# }
                #>
            </div>
        <# } #>
    <?php }
}
