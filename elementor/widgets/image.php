<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Image extends Widget_Base {

	public function get_name() {
		return 'image';
	}

	public function get_title() {
		return __( 'Image', LWD_ELEM_DOMAIN );
	}

	public function get_icon() {
		return 'eicon-insert-image';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_image',
			[
				'label' => __( 'Image', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Select an image', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'image', // Actually its `image_size`
				'label' => __( 'Image size', LWD_ELEM_DOMAIN ),
				'default' => 'large',
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'center',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'caption',
			[
				'label' => __( 'Caption', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'placeholder' => __( 'Enter image caption', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'link_to',
			[
				'label' => __( 'Link', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SELECT,
				'default' => 'none',
				'options' => [
					'none' => __( 'No link', LWD_ELEM_DOMAIN ),
					'file' => __( 'Media file', LWD_ELEM_DOMAIN ),
					'custom' => __( 'Custom URL', LWD_ELEM_DOMAIN ),
				],
			]
		);
        
        global $elem_link_type;
        
        if ( !empty( $elem_link_type ) ) {
            
            $this->add_control(
                'link_type',
                [
                    'label' => __( 'Type of link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::SELECT,
                    'default' => 'external',
                    'options' => $elem_link_type,
                    'condition' => [
                        'link_to' => 'custom',
                    ],
                ]
            );
            
            $this->add_control(
                'link_internal',
                [
                    'label' => __( 'Internal link', LWD_ELEM_DOMAIN ),
                    'type' => 'internal_url',
                    'placeholder' => '',
                    'condition' => [
                        'link_to' => 'custom',
                        'link_type' => 'internal',
                    ],
                    'show_label' => false,
                ]
            );
            
            $this->add_control(
                'link',
                [
                    'label' => __( 'External link', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::URL,
                    'placeholder' => __( 'http://www.example.com', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_to' => 'custom',
                        'link_type' => 'external',
                    ],
                    'show_label' => false,
                ]
            );
            
            $this->add_control(
                'link_phone',
                [
                    'label'       => '',
                    'description' => __( 'Enter the phone number', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::TEXT,
                    'placeholder' => __( '+44 02 7778 8855', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_to' => 'custom',
                        'link_type' => 'phone',
                    ],
                    'label_block' => true,
                ]
            );
            
            $this->add_control(
                'link_email',
                [
                    'label'       => '',
                    'description' => __( 'Enter an email address', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::TEXT,
                    'placeholder' => __( 'john.doe@mail.co.uk', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_to' => 'custom',
                        'link_type' => 'e-mail',
                    ],
                    'label_block' => true,
                ]
            );
            
        } else {
            $this->add_control(
                'link',
                [
                    'label' => __( 'Custom URL', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::URL,
                    'placeholder' => __( 'http://www.example.com', LWD_ELEM_DOMAIN ),
                    'condition' => [
                        'link_to' => 'custom',
                    ],
                    'show_label' => false,
                ]
            );
        }

		$this->add_control(
			'view',
			[
				'label' => __( 'Display', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_image',
			[
				'label' => __( 'Image', LWD_ELEM_DOMAIN ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'space',
			[
				'label' => __( 'Size (%)', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 100,
					'unit' => '%',
				],
				'size_units' => [ '%' ],
				'range' => [
					'%' => [
						'min' => 1,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image img' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'opacity',
			[
				'label' => __( 'Opacity (%)', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 1,
				],
				'range' => [
					'px' => [
						'max' => 1,
						'min' => 0.10,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image img' => 'opacity: {{SIZE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'image_border',
				'label' => __( 'Border type', LWD_ELEM_DOMAIN ),
				'selector' => '{{WRAPPER}} .elementor-image img',
			]
		);

		$this->add_control(
			'image_border_radius',
			[
				'label' => __( 'Border radius', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .elementor-image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_caption',
			[
				'label' => __( 'Caption', LWD_ELEM_DOMAIN ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'caption_align',
			[
				'label' => __( 'Alignment', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justify', LWD_ELEM_DOMAIN ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .widget-image-caption' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Text color', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .widget-image-caption' => 'color: {{VALUE}};',
				],
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		if ( empty( $settings['image']['url'] ) ) {
			return;
		}

		$has_caption = ! empty( $settings['caption'] );

		$this->add_render_attribute( 'wrapper', 'class', 'elementor-image' );

		if ( ! empty( $settings['shape'] ) ) {
			$this->add_render_attribute( 'wrapper', 'class', 'elementor-image-shape-' . $settings['shape'] );
		}

		$link = $this->get_link_url( $settings );

		if ( $link ) {
			$this->add_render_attribute( 'link', 'href', $link['url'] );

			if ( ! empty( $link['is_external'] ) ) {
				$this->add_render_attribute( 'link', 'target', '_blank' );
			}
		} ?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
		<?php
		if ( $has_caption ) : ?>
			<figure class="wp-caption">
		<?php endif;

		if ( $link ) : ?>
				<a <?php echo $this->get_render_attribute_string( 'link' ); ?>>
		<?php endif;

		echo Group_Control_Image_Size::get_attachment_image_html( $settings );

		if ( $link ) : ?>
				</a>
		<?php endif;

		if ( $has_caption ) : ?>
				<figcaption class="widget-image-caption wp-caption-text"><?php echo $settings['caption']; ?></figcaption>
		<?php endif;

		if ( $has_caption ) : ?>
			</figure>
		<?php endif; ?>
		</div>
		<?php
	}

	protected function _content_template() {
		?>
		<# if ( '' !== settings.image.url ) {
			var image = {
				id: settings.image.id,
				url: settings.image.url,
				size: settings.image_size,
				dimension: settings.image_custom_dimension,
				model: editModel
			};

			var image_url = elementor.imagesManager.getImageUrl( image );

			if ( ! image_url ) {
				return;
			}

			var link_url;

			if ( 'custom' === settings.link_to ) {
				link_url = settings.link.url;
			}

			if ( 'file' === settings.link_to ) {
				link_url = settings.image.url;
			}

			#><div class="elementor-image{{ settings.shape ? ' elementor-image-shape-' + settings.shape : '' }}"><#
			var imgClass = '',
				hasCaption = '' !== settings.caption;

			if ( '' !== settings.hover_animation ) {
				imgClass = 'elementor-animation-' + settings.hover_animation;
			}

			if ( hasCaption ) {
				#><figure class="wp-caption"><#
			}

			if ( link_url ) {
					#><a href="{{ link_url }}"><#
			}
						#><img src="{{ image_url }}" class="{{ imgClass }}" /><#

			if ( link_url ) {
					#></a><#
			}

			if ( hasCaption ) {
					#><figcaption class="widget-image-caption wp-caption-text">{{{ settings.caption }}}</figcaption><#
			}

			if ( hasCaption ) {
				#></figure><#
			}

			#></div><#
		} #>
		<?php
	}

	private function get_link_url( $instance ) {

		if ( 'none' === $instance['link_to'] ) {
			return false;
		}

		if ( 'custom' === $instance['link_to'] ) {
            
            $link_data = [];
            $is_external = false;
			
            foreach( $instance as $option => $value ) {
                if ( strpos( $option, 'link' ) === 0 && $value != '' ) {
                    if ( $option == 'link_to' ) continue;
                    
                    if ( $option == 'link_type' ) {
                        $link_data['link_data'] = $value;
                        continue;
                    }
                    
                    if ( $option == 'link' ) {
                        if ( isset( $value['url'] ) && $value['url'] != '' ) {
                            $link_data[ 'link_data-external' ] = $value['url'];
                            if ( isset( $value['is_external'] ) && $value['is_external'] ) $is_external = true;
                        } else {
                            continue;
                        }
                    } else {
                        $link_data[ str_replace( '_', '_data-', $option ) ] = $value;
                    }
                } 
            }

            if ( empty( $link_data ) ) {
				return false;
			}
            
            $link_data = lwd_get_link( 1, 'link_data', $link_data );
            
            if ( $is_external ) $link_data['is_external'] = true;
            
            return $link_data;

		}

        return [
			'url' => $instance['image']['url'],
		];
	}
}
