<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Common extends Widget_Base {

	public function get_name() {
		return 'common';
	}

	public function show_in_panel() {
		return false;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'_section_style',
			[
				'label' => __( 'Module style', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);

		global $elem_animation;

        if ( !empty( $elem_animation ) ) {
            $this->add_control(
                'animation',
                [
                    'label' => __( 'Animation', LWD_ELEM_DOMAIN ),
                    'type' => Controls_Manager::SELECT,
                    'default' => '',
                    'options' => $elem_animation,
                    'prefix_class' => 'js_',
                    'label_block' => true,
                ]
            );
        }

		$this->add_control(
			'_element_id',
			[
				'label' => __( 'CSS ID', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'label_block' => true,
				'title' => __( 'Add your own ID without a hash # at the beginning and no spaces, for example: my-id', LWD_ELEM_DOMAIN ),
			]
		);

		$this->add_control(
			'_css_classes',
			[
				'label' => __( 'CSS classes', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'prefix_class' => '',
				'label_block' => true,
				'title' => __( 'Add your own class without a dot . at the beginning and no spaces, for example: my-class', LWD_ELEM_DOMAIN ),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'_section_responsive',
			[
				'label' => __( 'Responsive', LWD_ELEM_DOMAIN ),
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);

		$this->add_control(
			'hide_desktop',
			[
				'label' => __( 'Hide on computer', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
				'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
				'return_value' => 'hidden-desktop',
			]
		);

		$this->add_control(
			'hide_tablet',
			[
				'label' => __( 'Hide on tablet', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
				'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
				'return_value' => 'hidden-tablet',
			]
		);

		$this->add_control(
			'hide_mobile',
			[
				'label' => __( 'Hide on mobile', LWD_ELEM_DOMAIN ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Yes', LWD_ELEM_DOMAIN ),
				'label_off' => __( 'No', LWD_ELEM_DOMAIN ),
				'return_value' => 'hidden-phone',
			]
		);

		$this->end_controls_section();
	}
}
