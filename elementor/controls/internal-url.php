<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A internal URL input control.
 *
 * @param string $default     A default value
 *                            Default empty
 * @param string $input_type  any valid HTML5 input type: email, tel, etc.
 *                            Default 'text'
 *
 * @since 1.0.0
 */
class Control_Internal_URL extends Control_Base {

	public function get_type() {
		return 'internal_url';
	}

	protected function get_default_settings() {
		return [
			'label_block' => true,
			'show_suggest' => true,
		];
	}

	public function content_template() {
		?>
		<div class="elementor-control-field elementor-control-url-suggest-{{{ data.show_suggest ? 'show' : 'hide' }}}">
			<label class="elementor-control-title">{{{ data.label }}}</label>
			<div class="elementor-control-input-wrapper">
                <input type="text" data-setting="{{ data.name }}" class="jq-suggest-field" placeholder="{{ data.placeholder }}" />
				<button class="elementor-control-url-suggest tooltip-target jq-suggest-link" data-tooltip="<?php _e( 'Suggest URL', LWD_ELEM_DOMAIN ); ?>" title="<?php esc_attr_e( 'Suggest URL', LWD_ELEM_DOMAIN ); ?>">
					<span class="elementor-control-url-suggest-icon" title="<?php esc_attr_e( 'Suggest URL', LWD_ELEM_DOMAIN ); ?>"><i class="fa fa-search"></i></span>
				</button>
			</div>
		</div>
		<# if ( data.description ) { #>
		<div class="elementor-control-description">{{{ data.description }}}</div>
		<# } #>
		<?php
	}
}
