<?php
/**
 * The template for displaying contact page
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

get_header(); 

    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();?>

            <article id="<?php the_ID(); ?>" <?php echo post_class(); ?>>
                
                <?php
                
                    $is_elementor = get_post_meta( $post->ID, '_elementor_edit_mode', true );
                
                    if ( !isset( $is_elementor ) || $is_elementor != 'builder' ) echo '<section class="section"><div class="content">';   
                
                        $contact_title_bg = apply_filters( 'lwd_filter_contact_title_bg', 'blue' );
                        $contact_title_padding = apply_filters( 'lwd_filter_contact_title_padding', 'tight' );
                        $contact_title_color = apply_filters( 'lwd_filter_contact_title_color', 'light' );
                
                        ?><section class="section<?php echo $contact_title_bg != '' ? ' section--' . $contact_title_bg : ''; echo $contact_title_padding != '' ? ' section--padding-' . $contact_title_padding : ''; ?>">
                            <div class="content">
                                <div class="title-perex<?php echo $contact_title_color != '' ? ' title-perex--' . $contact_title_color : ''; ?>"> 	
                                    <h1 class="title-perex__title js_loadFade"><?php the_title(); ?></h1>
                                </div>
                            </div>
                
                            <?php 
                
                                $contacts = get_post_meta( $post->ID, 'contacts', true );
                
                                //$contacts_cols = count( $contacts );
                                $contacts_cols = apply_filters( 'lwd_filter_contacts_cols', 3 );
                                $contacts_color = apply_filters( 'lwd_filter_contacts_color', 'light' );
                                $contacts_align = apply_filters( 'lwd_filter_contacts_align', 'left' );
                                
                                if ( !empty( $contacts ) ) { ?>
                
                                <div class="content">
                                    <div class="pure-g">
                                        <div class="contact contact--<?php echo $contacts_cols >= 4 ? 4 : $contacts_cols; echo $contacts_color != '' ? ' contact--' . $contacts_color : ''; echo $contacts_align != '' ? ' contact--' . $contacts_align : ''; ?>">
                                            
                                            <?php
                                                            
                                                switch ( true ) {
                                                    case ( $contacts_cols >= 4 ) :
                                                        $contact_class = ' pure-u-v1250-6-24 pure-u-v900-8-24 pure-u-v600-12-24';
                                                        break;
                                                    case ( $contacts_cols == 3 ) :
                                                        $contact_class = ' pure-u-v900-8-24 pure-u-v600-12-24';
                                                        break;
                                                    case ( $contacts_cols == 2 ) :
                                                        $contact_class = ' pure-u-v600-12-24';
                                                        break;
                                                    default : 
                                                        $contact_class = '';
                                                        break;
                                                }
                                              
                                                foreach ( $contacts as $contact ) {
                                                    if ( !isset( $contact['title'] ) || $contact['title'] == '' ) continue;
                                                    
                                                    ?><div class="pure-u-1<?php echo $contact_class; ?>">
                                                        <div class="contact__i js_loadFadeDelayed">
                                                            
                                                            <h3 class="contact__title"><?php echo trim( $contact['title'] ); ?></h3>
                                                            
                                                            <?php if ( isset( $contact['address'] ) && $contact['address'] != '' ) { ?><div class="contact__address"><?php echo nl2br( trim( $contact['address'] ) ); ?></div><?php } ?>
                                                            
                                                            <?php if ( isset( $contact['company-number'] ) && $contact['company-number'] != '' ) { ?><span class="contact__ico"><?php _e( 'Company ID', LWD_TEXT_DOMAIN ); ?>: <?php echo trim( $contact['company-number'] ); ?></span><?php } ?>
                                                            
                                                            <?php if ( isset( $contact['tax-number'] ) && $contact['tax-number'] != '' ) { ?><span class="contact__ico"><?php _e( 'VAT ID', LWD_TEXT_DOMAIN ); ?>: <?php echo trim( $contact['tax-number'] ); ?></span><?php } ?>
                                                            
                                                            <?php if ( isset( $contact['phone'] ) && $contact['phone'] != '' ) { ?><a class="contact__phone" href="tel:<?php echo lwd_format_phone( $contact['phone'], false ); ?>"><?php _e( 'Tel.', LWD_TEXT_DOMAIN ); ?>: <?php echo lwd_format_phone( $contact['phone'] ); ?></a><?php } ?>
                                                            
                                                            <?php if ( isset( $contact['fax'] ) && $contact['fax'] != '' ) { ?><span class="contact__fax"><?php _e( 'Fax', LWD_TEXT_DOMAIN ); ?>: <?php echo trim( $contact['fax'] ); ?></span><?php } ?>
                                                            
                                                            <?php if ( isset( $contact['e-mail'] ) && $contact['e-mail'] != '' ) { ?><a class="contact__email" href="mailto:<?php echo trim( $contact['e-mail'] ); ?>"><?php _e( 'E-mail', LWD_TEXT_DOMAIN ); ?>: <?php echo trim( $contact['e-mail'] ); ?></a><?php } ?>
                                                            
                                                            <?php if ( isset( $contact['desc'] ) && $contact['desc'] != '' ) { ?><div class="contact__address"><?php echo nl2br( trim( $contact['desc'] ) ); ?></div><?php } ?>
                                                            
                                                        </div>
                                                    </div><?php
                                                    
                                                }
                                                            
                                            ?>

                                        </div>
                                    </div>
                                    
                                <?php } ?>
                                    
                            </div>
                        </section><?php
                
                        do_action( 'lwd_before_content' );

                        the_content();

                        do_action( 'lwd_after_content' );
                
                    if ( !isset( $is_elementor ) || $is_elementor != 'builder' ) '</div></section>';

                ?>

                
            </article>

        <?php endwhile;
    endif;

get_footer(); ?>