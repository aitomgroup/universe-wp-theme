<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
    
		<h2 class="comments-title txt-center">
			<?php
				$comments_number = get_comments_number();
				if ( '1' === $comments_number ) {
					/* translators: %s: post title */
					printf( _x( 'One response to &ldquo;%s&rdquo;', 'comments title', LWD_TEXT_DOMAIN ), get_the_title() );
				} else {
                    if ( get_locale() == 'cs_CZ' && $comments_number >= 2 && $comments_number <= 4 ) {
                        printf(
                            '%1$s reakce na &ldquo;%2$s&rdquo;',
                            number_format_i18n( $comments_number ),
                            get_the_title()
                        );
                    } else {
                         printf(
                            /* translators: 1: number of comments, 2: post title */
                            _nx(
                                '%1$s response to &ldquo;%2$s&rdquo;',
                                '%1$s responses to &ldquo;%2$s&rdquo;',
                                $comments_number,
                                'comments title',
                                LWD_TEXT_DOMAIN
                            ),
                            number_format_i18n( $comments_number ),
                            get_the_title()
                        );
                    }
				}
			?>
		</h2>

        <div class="timeline content commentlist">
            
			<?php
				wp_list_comments( array(
					'avatar_size'  => 90,
					'style'        => 'div',
					'short_ping'   => false,
					'reply_text'   => __( 'Reply', LWD_TEXT_DOMAIN ),
                    'callback'     => 'ai_comments_function',
                    'end-callback' => 'ai_comments_end_function'
				) );
			?>

        </div>

		<?php 
    
            the_comments_pagination( array(
                'prev_text' => '<span class="screen-reader-text">' . __( 'Předchozí', LWD_TEXT_DOMAIN ) . '</span>',
                'next_text' => '<span class="screen-reader-text">' . __( 'Next', LWD_TEXT_DOMAIN ) . '</span>'
            ) );

	endif; // Check for have_comments().

	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) { ?>

		<!--<p class="no-comments"><?php _e( 'Comments are closed', LWD_TEXT_DOMAIN ); ?></p>-->
	<?php }

    echo '<div id="respond" data-anchor="respond-achor"></div>'; // style="top:-200px;position:relative"
    
    $commenter = wp_get_current_commenter();
    $req = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );
    
	comment_form( array(
        'fields' => array(
            'author' => '<div class="pure-g"><div class="form1__item pure-u-1 pure-u-v700-12-24"><label class="form1__label" for="author">' . __( 'Name', LWD_TEXT_DOMAIN ) . ( $req ? '<em class="form1__required">*</em>' : '' ) .'</label><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" class="form1__input"' . $aria_req . ' /></div>',
            'email' => '<div class="form1__item pure-u-1 pure-u-v700-12-24"><label class="form1__label" for="email">' . __( 'E-mail', LWD_TEXT_DOMAIN ) . ( $req ? '<em class="form1__required">*</em>' : '' ) .'</label><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" class="form1__input"' . $aria_req . ' /></div></div>',
            'url' => '<div class="pure-g"><div class="form1__item pure-u-1"><label class="form1__label" for="url">' . __( 'Website', LWD_TEXT_DOMAIN ) . '</label><input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" class="form1__input" /></div></div>'
        ),
        'comment_field' =>  '<div class="pure-g"><div class="form1__item pure-u-1"><label class="form1__label" for="comment">' . __( 'Comment', LWD_TEXT_DOMAIN ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" class="form1__area"></textarea></div></div>',
        'class_form' => 'comment-form form1 form1--small pure-form js_loadFade',
        'class_submit' => 'form1__btn',
        'submit_button' => '<button type="submit" name="%1$s" id="%2$s" class="%3$s">%4$s</button>',
        'submit_field' => '<div class="form1__item  pure-u-1">%1$s %2$s</div>',
    ) );
	
    ?>

</div><!-- #comments -->
