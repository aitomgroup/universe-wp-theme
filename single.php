<?php
/**
* The template for displaying all single posts, attachments and default template for single custom post type
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

get_header(); 

    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();

            $post_meta = lwd_get_post_metas( $post->ID );

            if ( is_singular('post') ) {
                if ( get_post_format() == 'link' ) {
                    $post_meta['custom-link-internal'] = $post_meta['custom-link'];
                    $post_meta['custom-link'] = 'internal';
                } else {
                    if ( isset( $post_meta['custom-link'] ) ) unset( $post_meta['custom-link'] );
                    if ( isset( $post_meta['custom-link-internal'] ) ) unset( $post_meta['custom-link-internal'] );
                }
                
                if ( get_post_format() != 'video' ) {
                    if ( isset( $post_meta['video'] ) ) unset( $post_meta['video'] );
                }
                
                if ( get_post_format() != 'gallery' ) {
                    if ( isset( $post_meta['gallery'] ) ) unset( $post_meta['gallery'] );
                }
            }

            // Redirect if detail is private
            if ( isset( $post_meta['custom-link'] ) && $post_meta['custom-link'] != '' ) {
                    
                $link = lwd_get_link( $post->ID, 'custom-link', $post_meta );
                    
                if ( isset( $link['url'] ) && $link['url'] != '' ) echo '<script>window.location = "' . $link['url'] . '";</script>';
            }

        
            $detail_style = apply_filters( 'lwd_filter_single_style', 'g-detail1', $post->post_type );
            $detail_meta_style = apply_filters( 'lwd_filter_single_meta_style', 'g-g2', $post->post_type );

            $show_date = apply_filters( 'lwd_filter_single_post_date', $post->post_type == 'post' ? true : false, $post->post_type );
            $show_cats = apply_filters( 'lwd_filter_single_post_cats', $post->post_type == 'post' ? true : false, $post->post_type );
            $show_tags = apply_filters( 'lwd_filter_single_post_tags', $post->post_type == 'post' ? true : false, $post->post_type );

            $ico_calendar = locate_template( 'assets/images/icons/ico-calendar.svg' );
            if ( isset( $ico_calendar ) && $ico_calendar != '' ) $ico_calendar = file_get_contents( $ico_calendar );
            $ico_category = locate_template( 'assets/images/icons/ico-category.svg' );
            if ( isset( $ico_category ) && $ico_category != '' ) $ico_category = file_get_contents( $ico_category );
            $ico_tag = locate_template( 'assets/images/icons/ico-tag.svg' );
            if ( isset( $ico_tag ) && $ico_tag != '' ) $ico_tag = file_get_contents( $ico_tag );

            do_action( 'lwd_before_content' );

            ?>

            <article id="<?php the_ID(); ?>" <?php echo post_class(); ?>>
                
                <section class="section">
                    
                    <div class="content content--smaller">
                        
                        <!-- [ PAGE TITLE ] -->
                        <h1 class="<?php echo $detail_style; ?>__title js_loadFade"><?php the_title(); ?></h1>
                        <!-- [ / PAGE TITLE ] -->
                        
                        <?php 
                        
                            if ( isset( $post_meta['video'] ) && $post_meta['video'] != '' ) {
                                
                                echo '<div class="' . $detail_style . '__video js_loadFade">';
                                
                                if ( get_youtube_id_from_url( $post_meta['video'] ) != '' ) {
                                    echo '<iframe class="' . $detail_style . '__video__iframe" width="560" height="315" src="https://www.youtube.com/embed/' . get_youtube_id_from_url( $post_meta['video'] ) . '" frameborder="0" allowfullscreen></iframe>';
                                } else if ( get_vimeo_id_from_url( $post_meta['video'] ) != '' ) {
                                    echo '<iframe  class="' . $detail_style . '__video__iframe" src="//player.vimeo.com/video/' . get_vimeo_id_from_url( $post_meta['video'] ) . '" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                                }
                                
                                echo '</div>';
                                
                            } else if ( has_post_thumbnail() ) {
                                $image_id = $image = $image_half = $image_full = '';
                                
                                $image_id = get_post_thumbnail_id( $post->ID );
                                
                                if ( $image_id != '' ) $image_data = wp_get_attachment_metadata( $image_id );
                                
                                $image = isset( $image_data['sizes']['thumb-single'] ) ? get_the_post_thumbnail_url( $post, 'thumb-single' ) : get_the_post_thumbnail_url( $post, 'full' );
                                
                                if ( isset( $image_data['sizes']['thumb-default'] ) ) $image_half =  get_the_post_thumbnail_url( $post, 'thumb-default' );
                                
                                if ( isset( $image_data['width'] ) && $image_data['width'] > 760 ) $image_full =  get_the_post_thumbnail_url( $post, 'full' );
                                
                                if ( $image_id != '' ) $image_data = get_post( $image_id );
                                
                                if ( $image != '' ) { ?>
                        
                                <div class="js_loadFade">
                                    <noscript>
                                        <img src="<?php echo ( $image_full != '' ? $image_full : $image ); ?>" class="<?php echo $detail_style . '__image'; ?>"<?php echo $image_half != '' ? ' srcset="' . $image_half . ' 280w, ' . $image . ' 760w' . ( $image_full != '' ? ', ' . $image_full : '' ) . '"' : ''; ?> sizes="auto" alt="<?php echo isset( $image_data->post_excerpt ) && $image_data->post_excerpt != '' ? $image_data->post_excerpt : $image_data->post_title; ?>" />
                                    </noscript>
                                    <img src="<?php echo generate_attachment_placeholder( 760, 506 ); ?>" class="<?php echo $detail_style . '__image lazyload'; ?>" data-src="<?php echo ( $image_full != '' ? $image_full : $image ); ?>"<?php echo $image_half != '' ? ' data-srcset="' . $image_half . ' 280w, ' . $image . ' 760w' . ( $image_full != '' ? ', ' . $image_full : '' ) . '"' : ''; ?> data-sizes="auto" alt="<?php echo isset( $image_data->post_excerpt ) && $image_data->post_excerpt != '' ? $image_data->post_excerpt : $image_data->post_title; ?>" />
                                </div>
                        
                                <?php }
                            }
                        
                            if ( $show_date || ( $show_cats && has_category() ) || ( $show_tags && has_tag() ) ) { ?>
                        
                                <div class="<?php echo $detail_meta_style; ?>__meta <?php echo $detail_style; ?>__meta">
                                    
                                    <?php if ( $show_date ) {
                                        echo sprintf( '<time class="%1$s__meta__i" datetime="%2$s">%3$s <span class="%1$s__meta__text">%4$s</span></time>', $detail_meta_style, get_the_date( DATE_W3C ), $ico_calendar, get_the_date() ); 
                                    } ?>

                                    <?php if ( $show_cats && has_category() ) { ?>
                                        <span class="<?php echo $detail_meta_style; ?>__meta__i"><?php echo $ico_category; ?> <span class="<?php echo $detail_meta_style; ?>__meta__text"><?php the_category( ', ' ); ?></span></span>
                                    <?php } ?>

                                    <?php if ( $show_tags && has_tag() ) { ?>
                                        <span class="<?php echo $detail_meta_style; ?>__meta__i"><?php echo $ico_tag; ?> <span class="<?php echo $detail_meta_style; ?>__meta__text"><?php the_tags( '', ', ' ); ?></span></span>
                                    <?php } ?>

                                </div>
                        
                        <?php } ?>
                        
                        <?php if ( has_excerpt() ) { ?>
                            <div class="<?php echo $detail_style; ?>__perex js_loadFade" n:if="$item->desc"><?php the_excerpt(); ?></div>
                        <?php } ?>
                        
                        <div class="wysiwyg">
                            <?php the_content(); ?>
                        </div>
                        
                        <?php if ( isset( $post_meta['gallery'] ) && $post_meta['gallery'] != '' ) { ?>
                            <h2><?php _e( 'Photogallery', LWD_TEXT_DOMAIN ); ?></h2>
                            
                            <?php echo do_shortcode( $post_meta['gallery'] ); ?>                
                        
                            <?php if ( isset( $post_meta['gallery-desc'] ) && $post_meta['gallery-desc'] != '' ) { ?>
                        
                                <div class="wysiwyg js_loadFade">
                                    <?php echo apply_filters( 'the_content', $post_meta['gallery-desc'] ); ?>
                                </div>
                        
                            <?php }
                        } ?>
                        
                        <?php
                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) {
                                comments_template();
                            }
                        ?>
                        
                    </div>
                                
                </section>
                             
            </article>
                    
            <?php 

            do_action( 'lwd_after_content' );

        endwhile;
    endif;

get_footer(); ?>