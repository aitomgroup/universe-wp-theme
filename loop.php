<?php
/**
 * The loop that displays posts/pages/events/sports/gastro/venues.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

global $archive_style, $archive_cols, $archive_align;

$archive_style = isset( $archive_style ) ? $archive_style : 'g-g2';
$archive_cols = isset( $archive_cols ) ? $archive_cols : 3;
$archive_align = isset( $archive_align ) ? $archive_align : 'left';

/* If there are no posts to display, such as an empty archive page */ 

if ( ! have_posts() ) : ?>

	<div class="pure-u-1 js_loadFade <?php echo $archive_style; ?>__wrap">
 		<div class="<?php echo $archive_style; ?>__i article">
			<h3><?php _e( 'Nothing found', LWD_TEXT_DOMAIN); ?></h3>
            <div class="<?php echo $archive_style; ?>__desc"><?php _e( 'Sorry, no results found.', LWD_TEXT_DOMAIN ); ?></div>
		</div>
	</div><!-- #post-0 -->

<?php endif;

	/* Start the Loop.
	 *
	 * In lightwebdesign we use the same loop in multiple contexts.
	 * It is broken into three main parts: when we're displaying
	 * posts that are in the gallery category, when we're displaying
	 * posts in the asides category, and finally all other posts.
	 *
	 * Additionally, we sometimes check for whether we are on an
	 * archive page, a search page, etc., allowing for small differences
	 * in the loop on each template without actually duplicating
	 * the rest of the loop that is shared.
	 *
	 * Without further ado, the loop:
	 */

    $c = 1;

    $ico_calendar = locate_template( 'assets/images/icons/ico-calendar.svg' );
    if ( isset( $ico_calendar ) && $ico_calendar != '' ) $ico_calendar = file_get_contents( $ico_calendar );
    $ico_category = locate_template( 'assets/images/icons/ico-category.svg' );
    if ( isset( $ico_category ) && $ico_category != '' ) $ico_category = file_get_contents( $ico_category );
    $ico_tag = locate_template( 'assets/images/icons/ico-tag.svg' );
    if ( isset( $ico_tag ) && $ico_tag != '' ) $ico_tag = file_get_contents( $ico_tag );
    $ico_plus = locate_template( 'assets/images/icons/ico-plus.svg' );
    if ( isset( $ico_plus ) && $ico_plus != '' ) $ico_plus = file_get_contents( $ico_plus );
	 
    while ( have_posts() ) : 

        global $post;

        the_post();

        $post_style = $archive_style;

        $post_class = $post_style . '__wrap pure-u-1';

        switch ( $archive_cols ) {
            case 4:
                $post_class .= ' pure-u-v1250-6-24 pure-u-v600-12-24 js_loadFade';
                break;
            case 3:
                $post_class .= ' pure-u-v600-8-24 js_loadFade';
                break;
            case 2:
                $post_class .= ' pure-u-v600-12-24 js_slideFromSide';
                break;
            case 1:
                $post_class .= ' js_loadFade';
                break;
        }

        $permalink = '';

        if ( $post->post_type == 'post' && get_post_format() == 'link' && $custom_post_link = get_post_meta( $post->ID, 'custom-link', true ) ) {
            $permalink = $custom_post_link;
        } else if ( in_array( $post->post_type, [ 'service', 'testimony' ] ) ) {
            $get_link = lwd_get_link( $post->ID, 'custom-link' );
            if ( isset( $get_link['url'] ) && $get_link['url'] != '' && strpos( $get_link['url'], '404' ) == false ) $permalink = $get_link['url'];
        } else {
            $permalink = get_permalink();
        }

        $show_date = apply_filters( 'lwd_filter_single_post_date', $post->post_type == 'post' ? true : false, $post->post_type );
        $show_cats = apply_filters( 'lwd_filter_single_post_cats', $post->post_type == 'post' ? true : false, $post->post_type );
        $show_tags = apply_filters( 'lwd_filter_single_post_tags', $post->post_type == 'post' ? true : false, $post->post_type );

        $video = get_post_meta( $post->ID, 'video', true );

        if ( $post->post_type == 'post' && get_post_format() != 'video' ) $video = '';

        ?>

            <!-- [ POST ] -->
            <article <?php echo post_class( $post_class );?> id="post-<?php the_ID(); ?>">
                
                <div class="<?php echo $post_style; ?>__i article">
                    
                    <?php 
                    
                    $image = '';
                    
                    if ( $post_style == 'g-g4' ) {
                        $icon_id = get_post_meta( $post->ID, 'icon', true );
                        if ( $icon_id != '' ) {
                            $icon = wp_get_attachment_image_src( $icon_id, 'full' );
                            if ( isset( $icon[0] ) ) $image = $icon[0];
                            
                            if ( isset( $permalink ) && $permalink != '' ) { ?>
                                <a href="<?php echo $permalink; ?>">
                            <?php } ?>
                    
                            <div class="g-g4__icon">
                                <noscript>
									<img src="<?php echo $image; ?>" alt="<?php the_title(); ?>" />
								</noscript>	
								<img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $image; ?>" alt="<?php the_title(); ?>" />
                            </div>
                                    
                            <?php if ( isset( $permalink ) && $permalink != '' ) { ?>
                                </a>
                            <?php } ?>
                    
                        <?php }
                    }
                    
                    if ( has_post_thumbnail() && ( $post_style != 'g-g4' || $image == '' ) ) {
    
                        $image = get_the_post_thumbnail_url( $post, 'thumb-single' );
                        $image_half =  get_the_post_thumbnail_url( $post, 'thumb-default' );
                        $image_id = get_post_thumbnail_id( $post->ID );
                        if ( $image_id != '' ) $image_data = get_post( $image_id );
                    
                        if ( $image != '' ) { ?>
                    
                            <div class="<?php echo $post_style; ?>__image-wrap">
                    
                            <?php if ( isset( $permalink ) && $permalink != '' ) { ?>
                                <a href="<?php echo $permalink; ?>" class="<?php echo $post_style; ?>__image <?php echo $post_style; ?>__image--link">
                            <?php } ?>

                            <noscript>
                                <img src="<?php echo $image; ?>" class="<?php echo $post_style . '__img'; ?>"<?php echo $image_half != '' ? ' srcset="' . $image_half . ' 280w, ' . $image . ' 760w"' : ''; ?> sizes="auto" alt="<?php echo isset( $image_data->post_excerpt ) && $image_data->post_excerpt != '' ? $image_data->post_excerpt : $image_data->post_title; ?>" />	
                            </noscript>
                                    
                            <img src="<?php echo generate_attachment_placeholder( 760, 506 ); ?>" class="<?php echo $post_style . '__img lazyload'; ?>" data-src="<?php echo $image; ?>"<?php echo $image_half != '' ? ' data-srcset="' . $image_half . ' 280w, ' . $image . ' 760w"' : ''; ?> data-sizes="auto" alt="<?php echo isset( $image_data->post_excerpt ) && $image_data->post_excerpt != '' ? $image_data->post_excerpt : $image_data->post_title; ?>" />

                            <?php if ( isset( $video ) && $video != '' ) { ?>
                                    <div class="<?php echo $post_style; ?>__play"></div>
                            <?php } ?>

                            <?php if ( isset( $permalink ) && $permalink != '' ) { ?>
                                    <div class="<?php echo $post_style; ?>__hover">
                                        <div class="<?php echo $post_style; ?>__hover__middle"><?php echo $ico_plus; ?><span class="<?php echo $post_style; ?>__hover__text"><?php _e( 'More information', LWD_TEXT_DOMAIN ); ?></span></div>
                                    </div>
                                </a>
                            <?php } ?>
                                      
                            </div>
                    
                        <?php }

                    } ?>
                    
                    <?php if ( isset( $permalink ) && $permalink != '' ) { ?>
                        <h3><a href="<?php echo $permalink; ?>" class="<?php echo $post_style; ?>__title"><?php the_title(); ?></a></h3>
                    <?php } else { ?>
                        <h3 class="<?php echo $post_style; ?>__title"><?php the_title(); ?></h3>
                    <?php } ?>
                    
                    <?php if ( $show_date || ( $show_cats && has_category() ) || ( $show_tags && has_tag() ) ) { ?>
                        
                        <div class="<?php echo $post_style; ?>__meta">

                            <?php if ( $show_date ) {
                                echo sprintf( '<time class="%1$s__meta__i" datetime="%2$s">%3$s <span class="%1$s__meta__text">%4$s</span></time>', $post_style, get_the_date( DATE_W3C ), $ico_calendar, get_the_date() ); 
                            } ?>

                            <?php if ( $show_cats && has_category() ) { ?>
                                <span class="<?php echo $post_style; ?>__meta__i"><?php echo $ico_category; ?> <span class="<?php echo $post_style; ?>__meta__text"><?php the_category( ', ' ); ?></span></span>
                            <?php } ?>

                            <?php if ( $show_tags && has_tag() ) { ?>
                                <span class="<?php echo $post_style; ?>__meta__i"><?php echo $ico_tag; ?> <span class="<?php echo $post_style; ?>__meta__text"><?php the_tags( '', ', ' ); ?></span></span>
                            <?php } ?>

                        </div>

                    <?php } ?>
                    
                    <?php if ( has_excerpt() ) { ?>
                        <div class="<?php echo $post_style; ?>__desc"><?php echo lwd_perex( get_the_excerpt(), apply_filters( 'lwd_filter_loop_perex_length', 150, $post->post_type ) ); ?></div>
                    <?php } ?>
                    
                    <?php if ( isset( $permalink ) && $permalink != '' ) { ?>
                        <a href="<?php echo $permalink; ?>" class="<?php echo $post_style; ?>__link<?php echo ( in_array( $post_style, [ 'g-g3', 'g-g4'] ) ? ' g-btn g-btn--border g-btn--text-dark' : '' ); ?>"><?php _e( 'More information', LWD_TEXT_DOMAIN ); ?></a>
                    <?php } ?>
                </div>
                
            </article>
            <!-- [ / POST ] -->
        
        <?php

        echo $c % $archive_cols == 0 && $wp_query->current_post +1 != $wp_query->post_count ? '</div><div class="' . ( $archive_style != '' ? $archive_style . ( $archive_cols != '' ? ' ' . $archive_style . '--' . $archive_cols : '' ) . ( $archive_align != '' ? ' ' . $archive_style . '--' . $archive_align : '' ) : '' ) . ' pure-g">' : '';

        $c++;

    endwhile; // End the loop. Whew. ?>

