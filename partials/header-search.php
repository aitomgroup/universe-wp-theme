<?php
/**
 * The template partial for displaying header searchform
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

    $search = get_theme_mod( 'lwd_search', '' );
    $ico_close = locate_template( 'assets/images/icons/ico-close.svg' );

    if ( $search != '' ) { ?>
        <input type="checkbox" id="search-status" class="jq_search-status hidden">
        <form action="<?php echo home_url( '/' ); ?>" role="search" method="GET" class="g-header__search-form search-form1 search-is-open">
            <div id="jq_search" class="search-form1__mobil-wrap">
                <input name="s" id="s" autofocus="" class="search-form1__text" placeholder="<?php _e( 'Type what are you looking for...', LWD_TEXT_DOMAIN ); ?>">
                <button type="submit" class="search-form1__btn"><?php _e( 'Search', LWD_TEXT_DOMAIN ); ?></button>
                <label for="search-status" id="jq_search_close" class="search-form1__close"><?php if ( isset( $ico_close ) && $ico_close != '' ) echo file_get_contents( $ico_close ); ?></label>
            </div>
            <label for="search-status"  id="jq_search_call" class="search-form1__call"><?php _e( 'Search', LWD_TEXT_DOMAIN ); ?></label>
        </form>		
    <?php }