<?php
/**
 * The template partial for displaying header content
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

?>

    <!-- [ HEADER ] -->
    <input type="checkbox" id="menu-status" class="hidden">
    <header class="g-header menu-is-open">
        <div class="clearfix">

            <div class="g-header__top">

                <div class="g-header__line1 clearfix">

                    <?php
                        /***** Logo *****/
                        echo lwd_logo( 'g-header__logo' );
                    ?>

                    <div class="g-header__right">

                        <?php 
                            
                            do_action( 'lwd_before_header_right' );
                                
                            /**** Klientská zóna ****/
                            echo get_template_part( 'partials/header', 'login' );

                            /**** Telefonní číslo ****/
                            $phone = get_theme_mod( 'lwd_phone', '' );
                        
                            $ico_call = locate_template( 'assets/images/icons/ico-call.svg' );

                            if ( $phone != '' ) { ?>
                                <div class="g-header__call">
                                    <div class="call">
                                        <a href="tel:<?php echo lwd_format_phone( $phone, false ); ?>" class="call__link">
                                            <?php if ( isset( $ico_call ) && $ico_call != '' ) echo file_get_contents( $ico_call ); ?>
                                            <span class="call__number"><?php echo lwd_format_phone( $phone, true, false ); ?></span>	
                                        </a>
                                    </div>
                                </div>
                            <?php }

                            /***** Jazykové menu *****/
                            echo get_template_part( 'partials/header', 'langs' );

                            /***** Vyhledavaci formular *****/
                            echo get_template_part( 'partials/header', 'search' );

                            /***** Hamburger *****/
                            echo '<label for="menu-status" id="jq_open_menu" class="g-header__hamburger g-hamburger">
                                <span class="g-hamburger__i"></span>
                                <span class="g-hamburger__title">' . __( 'MENU', LWD_TEXT_DOMAIN ) . '</span>
                            </label>';
                        
                            do_action( 'lwd_after_header_right' );
                        
                        ?>

                    </div>
                </div>
            </div>

            <div class="g-header__bottom clearfix">
                
                <?php do_action( 'lwd_before_header_menu' ); ?>
                
                <?php /***** Hlavní menu *****/ ?>
                <div  id="jq_menu" class="g-header__nav">
                    <?php
                        wp_nav_menu( array(
                            'container'       => 'nav',
                            'container_class' => 'nav clearfix',
                            'echo'            => true,
                            'menu_class'      => 'nav__list0',
                            'theme_location'  => 'main',
                            'walker'          => new lwd_main_walker()
                         ) );
                    ?>
                </div>
                
                <?php do_action( 'lwd_after_header_menu' ); ?>
                
            </div>

        </div>			
    </header>
    <!-- [ / HEADER ] -->