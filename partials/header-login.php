<?php
/**
 * The template partial for displaying header login
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

    if ( defined( 'LWD_MEMBER_ZONE' ) && LWD_MEMBER_ZONE ) {

    $ico_gear = locate_template( 'assets/images/icons/ico-gear.svg' );
    $ico_exit = locate_template( 'assets/images/icons/ico-exit.svg' );
    $ico_enter = locate_template( 'assets/images/icons/ico-enter.svg' ); ?>

        <div class="g-header__client-zone">	
            <?php if ( is_user_logged_in() ) { ?>
            <a class="login" href="<?php echo LWD_MZ_HOME_URL; ?>"><?php if ( isset( $ico_gear ) && $ico_gear != '' ) echo file_get_contents( $ico_gear ); ?> <span class="login__text"><?php _e( 'My account', LWD_TEXT_DOMAIN ); ?></span></a>
            </div><div class="g-header__client-zone">
            <a class="login" href="<?php echo wp_logout_url( home_url() ); ?>"><?php if ( isset( $ico_exit ) && $ico_exit != '' ) echo file_get_contents( $ico_exit ); ?> <span class="login__text"><?php _e( 'Log out', LWD_TEXT_DOMAIN ); ?></span></a>
            <?php } else { ?>
                <a class="login" href="<?php echo LWD_MZ_HOME_URL . 'login'; ?>"><?php if ( isset( $ico_enter ) && $ico_enter != '' ) echo file_get_contents( $ico_enter ); ?> <span class="login__text"><?php _e( 'Log in', LWD_TEXT_DOMAIN ); ?></span></a>
            <?php } ?>
        </div>

    <?php }