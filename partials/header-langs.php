<?php
/**
 * The template partial for displaying header languages switcher
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

    $items = array();
    $itemCurrent = '';

    if ( function_exists( 'icl_get_languages' ) ) {
        $languages = icl_get_languages( 'skip_missing=0' );
        if ( 1 < count( $languages ) ) {
            foreach( $languages as $code => $l ) {
                if ( isset( $l['active'] ) && $l['active'] ) {
                    $itemCurrent = $l['language_code'];
                    continue;
                }
                
                $items[ $code ] = array(
                    'url'      => esc_url( $l['url'] ),
                    'http'     => isset( $l['language_code'] ) ? $l['language_code'] : '',
                    'name'     => isset( $l['native_name'] ) ? $l['native_name'] : '',
                    'code'     => isset( $l['language_code'] ) ? $l['language_code'] : ''
                );
            }
        }
    } elseif ( function_exists( 'mlp_show_linked_elements' ) ) {
        $api = apply_filters( 'mlp_language_api', NULL );
        /** @var Mlp_Language_Api_Interface $api */
        if ( ! is_a( $api, 'Mlp_Language_Api_Interface' ) ) {
            return '';
        }

        $translations = $api->get_translations( array(
            'strict'       => false,
            'include_base' => true,
        ) );

        if ( is_multisite() ) $current_blog_id = get_current_blog_id();

        /** @var Mlp_Translation_Interface $translation */
        foreach ( $translations as $site_id => $translation ) {
            $url = $translation->get_remote_url();
            
            if ( empty( $url ) ) continue;
            
            $language = $translation->get_language();
            //$priority = $language->get_priority();
            
            if ( isset( $current_blog_id ) && $current_blog_id === $site_id ) {
                $itemCurrent = $language->get_name( 'language_short' );
                continue;
            }

            $items[ $site_id ] = array(
                'url'      => esc_url( $url ),
                'http'     => $language->get_name( 'http' ),
                'name'     => $language->get_name( 'native' ),
                'code'     => $language->get_name( 'language_short' ),
                //'priority' => $priority
            );
        }

        ksort( $items );
    }

    if ( !empty( $items ) && $itemCurrent != '' ) {

        echo '<input type="checkbox" id="lang-nav-status" class="hidden">';
        echo '<nav class="g-header__lang-nav g-lang-nav"><ul class="g-lang-nav__list">';
        
        echo ' <li class="g-lang-nav__i"><label for="lang-nav-status" class="g-lang-nav__link g-lang-nav__link--active">' . strtoupper( esc_html( $itemCurrent ) ) . '</label></li>';

        foreach ( $items as $item ) {
            echo sprintf( '<li class="g-lang-nav__i"><a rel="alternate" hreflang="%1$s" href="%2$s" title="%3$s" class="%4$s">%5$s</a></li>', esc_attr( $item['http'] ), $item['url'], esc_html( $item[ 'name' ] ), 'g-lang-nav__link', strtoupper( esc_html( $item[ 'code' ] ) ) );
        }

        echo '</ul></nav>';

    }