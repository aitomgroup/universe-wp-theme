<?php
/**
 * The template partial for displaying hero title
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

if ( is_front_page() ) {
    global $post;
    
    $hero_meta = lwd_get_post_metas( $post->ID ); 
    
    if ( isset( $hero_meta['hero-title'] ) ) $front_hero_title = $hero_meta['hero-title'];
    
    $hero_buttons = array();
    
    if ( isset( $hero_meta['hero-buttons'] ) ) {
        foreach( unserialize( $hero_meta['hero-buttons'] ) as $button ) {
            if ( isset( $button['title'] ) && isset( $button['style'] ) && isset( $button['link'] ) ) $hero_buttons[] = $button;
        }
    }
    
}

if ( is_front_page() && isset( $front_hero_title ) && $front_hero_title != '' ) { ?>

    <!-- [ HERO ] -->
    <section class="placeholder<?php echo ( isset( $hero_meta['hero-align'] ) && $hero_meta['hero-align'] != '' ? ' placeholder--' . $hero_meta['hero-align'] : '' ); ?>">
        
        <div class="placeholder__fix"<?php echo isset( $hero_meta['hero-bg'] ) && $hero_meta['hero-bg'] != '' ? ' style="background: none"' : ''; ?>>
            
            <?php if ( isset( $hero_meta['hero-bg'] ) && $hero_meta['hero-bg'] != '' ) {
                global $post;

                $hero_bg = wp_get_attachment_image_src( $hero_meta['hero-bg'], 'hero-full' );
                $hero_bg_half =  wp_get_attachment_image_src( $hero_meta['hero-bg'], 'hero-half' );

                echo '<div class="placeholder__bg fullBgLazy fullBgLazy--cover lazyload lazyloadFadeIn"' . ( isset( $hero_bg[0] ) && $hero_bg[0] != '' ? ' data-bg="' . $hero_bg[0] . '"' . ( isset( $hero_bg_half[0] ) && $hero_bg_half[0] != '' ? ' data-bgset="' . $hero_bg_half[0] . ' 767w, ' . $hero_bg[0] . ' 1920w"' : '' ) : '' ) . '></div>';

            } ?>
            
            <div class="placeholder__wrap">
                
                <div class="placeholder__middle">
                    
                    <div class="placeholder__title">
                        
                        <div class="title-perex<?php echo ( isset( $hero_meta['hero-title-color'] ) && $hero_meta['hero-title-color'] != '' ? ' title-perex--' . $hero_meta['hero-title-color'] : '' ) . ( isset( $hero_meta['hero-align'] ) && $hero_meta['hero-align'] != '' ? ' title-perex--' . $hero_meta['hero-align'] : '' ); ?>">
                            
                            <h1 class="title-perex__title js_loadFade"><?php echo apply_filters( 'lwd_filter_page_title', $front_hero_title ); ?></h1>
                            
                            <?php $perex = apply_filters( 'lwd_filter_page_perex', isset( $hero_meta['hero-perex'] ) ? lwd_perex( $hero_meta['hero-perex'], 200 ) : '' ); ?>
                            
                            <?php if ( $perex != '' ) { ?>
                                <div class="title-perex__perex js_loadFade"><?php echo do_shortcode( apply_filters( 'wpautop', $perex ) ); ?></div>
                            <?php } ?>
                        </div>
                        
                    </div>
                    
                    <?php if ( !empty( $hero_buttons ) ) { ?>
                        <div class="placeholder__divider"></div>
                        <div class="placeholder__btns">	
                            <div class="buttons<?php echo ( isset( $hero_meta['hero-align'] ) && $hero_meta['hero-align'] != '' ? ' buttons--' . $hero_meta['hero-align'] : '' ); ?> content js_loadFade">

                                    <?php foreach ( $hero_buttons as $button ) {
                                        if ( !isset( $button['link'] )  || $button['link'] == '' ) $button['link'] = 'internal';
                                        $link = lwd_get_link( $post->ID, 'link', $button ); ?>
                                        <a href="<?php echo isset( $link['url'] ) ? $link['url'] : ''; ?>" class="buttons__btn g-btn<?php echo ( isset( $button['style'] ) && $button['style'] != '' ? ' g-btn--' . $button['style'] . ( $button['style'] == 'text-dark' ? ' g-btn--border' : '' ) : '' ); ?>"><?php echo $button['title']; ?></a>
                                    <?php } ?>

                            </div>
                        </div>
                    <?php } ?>
                </div>
                
                <a href="#" class="placeholder__arrow js_scroll_under"><?php _e( 'Scroll Down', LWD_TEXT_DOMAIN ); ?></a>	
                
            </div>
            
        </div>
        
    </section>
    <!-- [ / HERO ] -->
    
<?php } elseif ( is_page() || is_singular() ) {

    global $post;
    
    $show_hero = get_post_meta( $post->ID, 'hero', true );
    
    if ( !isset( $show_hero ) || $show_hero != 'off' ) {
        
        $hero_custom_title = get_post_meta( $post->ID, 'hero-custom-title', true ); ?>

        <!-- [ HERO ] -->
        <section class="page-header">

            <?php

                if ( has_post_thumbnail() ) {
                    global $post;

                    $hero_bg = get_the_post_thumbnail_url( $post, 'hero' );
                    $hero_bg_half =  get_the_post_thumbnail_url( $post, 'hero-half' );

                    if ( $hero_bg != '' ) {
                        echo '<div class="page-header__bg lazyload" data-bg="' . $hero_bg . '"' . ( $hero_bg_half != '' ? ' data-bgset="' .$hero_bg_half . ' 767w, ' . $hero_bg . ' 1920w"' : '' ) . '></div>';
                    }

                }

            ?>

            <h1 class="page-header__title js_loadFade"><?php echo apply_filters( 'lwd_filter_page_title', ( $hero_custom_title != '' ? $hero_custom_title : get_the_title() ) ); ?></h1>

            <?php $perex = apply_filters( 'lwd_filter_page_perex', has_excerpt() ? lwd_perex( get_the_excerpt(), 250 ) : '' ); ?>

            <?php if ( $perex != '' ) { ?>
                <div class="page-header__perex js_loadFade"><?php echo $perex; ?></div>
            <?php } ?>

        </section>
        <!-- [ / HERO ] -->

    <?php }

} elseif (  is_home() || is_archive() || is_tag() || is_tax() || is_search() ) { ?>
    

     <!-- [ HERO ] -->
    <section class="page-header">
        
        <?php /* TO-DO: Add global hero background */ ?>
        
        <h1 class="page-header__title js_loadFade"><?php
        
        if ( is_home() ) {
            global $wp_query;
            
            if ( isset( $wp_query->queried_object->post_title ) ) echo $wp_query->queried_object->post_title; else _e( 'News', LWD_TEXT_DOMAIN );
        } elseif ( is_category() || is_tag() || is_tax() ) {
             $term = $wp_query->queried_object;

            if ( is_category() || is_tax() ) _e( 'Category: ', LWD_TEXT_DOMAIN );
            if ( is_tag() ) _e( 'Tag: ', LWD_TEXT_DOMAIN );

            echo $term->name;
        } elseif ( is_day() ) {
            _e( 'Archive of the Day: ', LWD_TEXT_DOMAIN ); the_time( 'j. F, Y' );
        } elseif ( is_month() ) {
            _e( 'Archive of the month: ', LWD_TEXT_DOMAIN ); the_time( 'F, Y' );
        } elseif ( is_year() ) {
            _e( 'Archive of the Year: ', LWD_TEXT_DOMAIN ); the_time( 'Y' );
        } elseif ( is_author() ) {
            _e( 'Author Archive: ', LWD_TEXT_DOMAIN ); wp_title('');
        } elseif ( is_search() ) {
            echo __( 'Search results for: ', LWD_TEXT_DOMAIN ) . get_search_query();
        }    
            
        ?></h1>
   
    </section>
    <!-- [ / HERO ] -->

<?php }