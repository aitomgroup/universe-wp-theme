<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything
 * up till <div class="content__wrap">
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */
?><!DOCTYPE html>
<html class="no-js<?php if(isset($_COOKIE['fontloaded'])) echo ' wf-active'; ?>" <?php language_attributes(); ?>>
	<head>
    	<meta charset="<?php bloginfo( 'charset' ); ?>">
    	<title><?php wp_title(); ?></title>
        
        <!--
                       __    __  __ 
            |    |  | |  \  /  `  / 
            |___ |/\| |__/ .\__, /_ 

        -->
 
   		<!-- Mobile specific meta goodness -->
   		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
 
   		<!-- WP Head -->
		<?php

			/* Always have wp_head() just before the closing </head>
			 * tag of your theme, or you will break many plugins, which
			 * generally use this hook to add elements to <head> such
			 * as styles, scripts, and meta tags.
			*/
			wp_head();
		?>
        
        <?php if(!isset($_COOKIE['fontloaded'])) { ?>	
            <script>
                WebFontConfig = {
                    google: { families: [ 'Raleway:300,400,700:latin,latin-ext' ] },
                    active:function() {document.cookie ='fontloaded=1; expires='+(new Date(new Date().getTime() + 86400000)).toGMTString()+'; path=/'}
                };
            </script>
            <script src="//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js" async defer></script>
            <link rel="prefetch" href="https://fonts.googleapis.com/css?family=Raleway:300,400,700&amp;subset=latin-ext">
        <?php } else { ?>
            <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700&amp;subset=latin-ext" rel="stylesheet" type="text/css">
        <?php } ?>
 
    	<!--[if lte IE 8]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

	</head>
	<body <?php body_class(); ?>>
        <script>document.documentElement.className = document.documentElement.className.replace('no-js', 'js');</script>
        
        <?php do_action( 'body_open' ); ?>
        
        <?php echo get_template_part( 'partials/header', 'content' ); ?>
        
        <!-- [ PAGE CONTENT ] -->
        <main class="main clearfix">
			<?php 
                /*<div class="content">	
                    <!-- breadcrumbs-->
                    <h1 class="main__title"><?php the_title(); ?></h1>
                </div>*/
            ?>
		