<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the .content__wrap div and all content after
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */
?> 		
        </main>
        <!-- [ / PAGE CONTENT ] -->

        <!-- [ FOOTER ] -->
        <footer class="g-footer">
			<div class="clearfix">
				
                <!-- [ FOOTER WIDGETS ] -->
				<div class="g-footer__top pure-g">
					<div class="content">
                        <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar( 'footer' ) ); ?>
					</div>
				</div>
                <!-- [ / FOOTER WIDGETS ] -->
				
                <!-- [ FOOTER COPYRIGHT ] -->
				<div class="g-footer__bottom pure-g js_slideFromTop">
					<div class="content">
						<?php /***** SOCIÁLNÍ IKONY *****/ ?>
						<div class="pure-u-1 pure-push-v1100-8-24 pure-u-v1100-8-24 g-footer__soc">
							<?php
                            
                                $socials = array(
                                    'google-plus' => array(
                                        'text' => __( 'Google+', LWD_TEXT_DOMAIN ),
                                        'url' => get_theme_mod( 'lwd_google' )
                                    ),
                                    'facebook' => array(
                                        'text' => __( 'Facebook', LWD_TEXT_DOMAIN ),
                                        'url' => get_theme_mod( 'lwd_facebook' )
                                    ),
                                    'linked-in' => array(
                                        'text' => __( 'Linked-in', LWD_TEXT_DOMAIN ),
                                        'url' => get_theme_mod( 'lwd_linkedin' )
                                    ),
                                    'twitter' => array(
                                        'text' => __( 'Twitter', LWD_TEXT_DOMAIN ),
                                        'url' => get_theme_mod( 'lwd_twitter' )
                                    ),
                                    'pinterest' => array(
                                        'text' => __( 'Pinterest', LWD_TEXT_DOMAIN ),
                                        'url' => get_theme_mod( 'lwd_pinterest' )
                                    )
                                );
                            
                                $socials_icon = array();
                                foreach ( $socials as $class => $options ) {
                                    if ( isset( $class ) && $class != '' && isset( $options['text'] ) && $options['text'] != '' && isset( $options['url'] ) && $options['url'] != '' ) {
                                        $socials_icon[$class] = array(
                                            'icon' => locate_template( 'assets/images/icons/ico-' . $class . '.svg', false ),
                                            'url' => $options['url'],
                                            'text' => $options['text']
                                        );
                                    }
                                }
                            
                                $socials_icon = apply_filters( 'lwd_filter_footer_socials', $socials_icon );
                                
                                if ( !empty( $socials_icon ) ) {
                                    
                                    echo '<div class="g-footer__social social">';
                                    
                                    foreach ( $socials_icon as $class => $options ) {
                                        echo '<a href="' . $options['url'] . '" class="social__i social__i--' . $class . '" title="' . $options['text'] . '">' . file_get_contents( $options['icon'] ) . '</a>';
                                    }
                                    
                                    echo '</div>';
                                }
                            
                            ?>
						</div>

						<?php /***** COPYRIGHT *****/ ?>
						<div class="pure-u-1 pure-pull-v1100-8-24 pure-u-v1100-8-24 pure-u-v700-16-24 g-footer__copy">
							<span class="g-footer__copy__text"><?php echo apply_filters( 'lwd_filter_copy', do_shortcode( get_theme_mod( 'lwd_copyright', sprintf( __( '&copy; All rights reserved, %s', LWD_TEXT_DOMAIN ), get_option( 'blogname' ) ) ) ) ); ?></span>	
						</div>

						<? /***** AITOM PODPIS *****/ ?>
						<div class="pure-u-1 pure-u-v1100-8-24 pure-u-v700-8-24 g-footer__signature">
                            <?php
                                $signature = apply_filters( 'lwd_filter_signature', array(
                                    'format' => '<a class="g-signature" href="http://www.aitom.cz/">%s</a>',
                                    'icon' => locate_template( 'assets/images/icons/aitom.svg', false )
                                ) );
                            
                                if ( isset( $signature['format'] ) && isset( $signature['icon'] ) ) echo sprintf( $signature['format'], file_get_contents( $signature['icon'] ) );
                            ?>		
						</div>
					</div>
				</div>
                <!-- [ / FOOTER COPYRIGHT ] -->
			</div>
		</footer>
        <!-- [ / FOOTER ] -->

                
 		<!-- [ Functions scripts ] -->
 		<?php
 			/* Always have wp_footer() just before the closing </body>
 			 * tag of your theme, or you will break many plugins, which
 			 * generally use this hook to reference JavaScript files.
 			 */
 			 
 			 wp_footer();
 		?>
 
	</body>
</html>
