<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage AITOM-UNIVERSE
 * @since AITOM-UNIVERSE 0.1
 */

get_header(); 

    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();?>

            <article id="<?php the_ID(); ?>" <?php echo post_class(); ?>>
                
                <?php get_template_part( 'partials/hero' ); ?>
                
                <section class="section">
                    
                    <div class="content">
                        <?php

                            do_action( 'lwd_before_content' );

                            the_content();

                            do_action( 'lwd_after_content' );

                        ?>
                    </div>

                </section>
                
            </article>

        <?php endwhile;
    endif;

get_footer(); ?>